package omeca.mastModel.generator;

import omeca.mastModel.classes.processor;

/*
 * This class generates processor mast model 
 * 
 * */
public class processorMastModel {
	
	public String processor_mast;
	
	void print(processor cpu)
	{
		processor_mast = "Processing_Resource ( \n " +
				"Type => Regular_Processor \n ,   " +
				"Name                   =>" + cpu.nameOfProcessor + ",\n" +
				"Speed_Factor           => " + cpu.speed_Factor + ",\n" + 
				"Worst_ISR_Switch      => " + cpu.Worst_ISR_Switch + ",\n" + 
				"Best_ISR_Switch        => " + cpu.Best_ISR_Switch +",\n" + 
				"Avg_ISR_Switch         => " + cpu.Avg_ISR_Switch +",\n" + 
				"Max_Interrupt_Priority => " + cpu.Max_Interrupt_Priority + ",\n" +
				"Min_Interrupt_Priority => " + cpu.Min_Interrupt_Priority + "\n" +
				");";

	}
	

}

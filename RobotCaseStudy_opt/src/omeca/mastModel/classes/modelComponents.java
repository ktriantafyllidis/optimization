package omeca.mastModel.classes;

import java.util.ArrayList;

public class modelComponents{
	
	public ArrayList<messageOperation> messageOperations = new ArrayList<messageOperation>();
	
	public ArrayList<simpleOperation> simpleOperations = new ArrayList<simpleOperation>();
	
	public ArrayList<compositeOperation> compositeOperations = new ArrayList<compositeOperation>();

}

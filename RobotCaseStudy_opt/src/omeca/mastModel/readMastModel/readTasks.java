package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.IOException;
import omeca.mastModel.classes.task;
import omeca.mastModel.classes.tasks;

public class readTasks {
	
    String line, next;
    public tasks Tasks = new tasks();
    
    
	public void getTasks(BufferedReader bf)
	{
	 try {
			for (line = bf.readLine(); line != null; line = next)
			{
				next = bf.readLine();

			    if (line.contains("Scheduling_Server (")) 
			    {			    		
			    	task tsk = new task();
			    	while(!(next.contains(");")))
			    	{
			    		//scheduler

			    		if (next.contains("Type"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			tsk.type = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Name"))
			    		{
			    			String [] tmp;
			    			tmp = next.split("=> ");
			    			tsk.nameOfTask = tmp[tmp.length-1].trim();
			    		}
			    		else if (next.contains("Server_Sched_Parameters"))
			    		{
			    			if(next.contains("Type"))
			    			{
				    			String tmp[];
				    			tmp = next.split("=> ");
				    			tsk.schedulingType = tmp[tmp.length-1].trim();
			    			}
			    			else
			    			{
						    	//search for the type of the scheduling server
			    				while(!(next.contains("Type")))
			    				{
			    					next = bf.readLine();
			    				}
			    				if (next.contains("Type"))
			    				{
					    			String tmp[];
					    			tmp = next.split("=> ");
					    			tsk.schedulingType = tmp[tmp.length-1].trim();
			    				}
			    			}

			    		}
			    		else if (next.contains("The_Priority"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			tsk.priority = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Scheduler"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			tsk.schedulerName = tmp[tmp.length-1].trim();

			    		}
			    	
			    		next = bf.readLine();

			    	}
			    	Tasks.Tasks.add(tsk);
			    }
			    				    	
			   }  
	  

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	 try 
	 {
		bf.reset();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

}

package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.IOException;
import omeca.mastModel.classes.scheduler;
import omeca.mastModel.classes.schedulers;

public class readSchedulers {
	
    String line, next;
    public schedulers Schedulers = new schedulers();
    
    
	public void getSchedulers(BufferedReader bf)
	{
	 try {
			for (line = bf.readLine(); line != null; line = next)
			{
				next = bf.readLine();

			    if (line.contains("Scheduler (")) 
			    {			    		
			    	scheduler sched = new scheduler();
			    	while(!(next.contains(");")))
			    	{
			    		//scheduler

			    		if (next.contains("Name"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.nameOfScheduler = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Policy"))
			    		{
			    			String [] tmp;
			    			tmp = next.split("=> ");
			    			if (tmp[tmp.length-2].contains("Type"))
			    			{
			    				sched.policyType = tmp[tmp.length-1];
			    			}

			    			else 
			    			{
			    				next = bf.readLine();
			    				if (next.contains("Type"))
			    				{
			    					tmp = next.split("=> ");
					    			sched.policyType = tmp[tmp.length-1];
			    				}
			    			}



			    		}
			    		else if (next.contains("Worst_Context_Switch"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.Worst_Context_Switch = tmp[tmp.length-1].trim();


			    		}
			    		else if (next.contains("Best_Context_Switch"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.Best_Context_Switch = tmp[tmp.length-1].trim();

			    		}
			    		else if (next.contains("Avg_Context_Switch"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.Avg_Context_Switch = tmp[tmp.length-1].trim();


			    		}
			    		else if (next.contains("Max_Priority"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.Max_Priority = tmp[tmp.length-1].trim();

			    		} 		 		
			    		else if (next.contains("Min_Priority"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.Min_Priority = tmp[tmp.length-1].trim();

			    		} 	
			    		else if (next.contains("Host"))
			    		{
			    			String tmp[];
			    			tmp = next.split("=> ");
			    			sched.host = tmp[tmp.length-1].trim();

			    		}
			    		next = bf.readLine();

			    	}
			    	Schedulers.Schedulers.add(sched);
			    }
			    				    	
			   }  
	  

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	 try 
	 {
		bf.reset();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

}

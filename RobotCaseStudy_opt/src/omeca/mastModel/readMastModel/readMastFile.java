package omeca.mastModel.readMastModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class readMastFile {
	
	public BufferedReader bf;
	readProcessors readProc = new readProcessors();
    readSchedulers readSched = new readSchedulers();
	readTasks readTsk = new readTasks();
	readOperations readOper = new readOperations();
	readScenarios readScen = new readScenarios(readOper.Operations, readTsk.Tasks);
	
	
	private void openFile(File filePath)
	{
		try {
			bf  = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//maybe i have to change this
		try {
			bf.mark(1000000);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	
	
	//read the mast file 
	public readMastFile(File filePath)
	{
		
		this.openFile(filePath);
		readProc.getProcessors(bf);
	    System.out.println(readProc.Processors.Processors);
		

		readSched.getSchedulers(bf);
		System.out.println(readSched.Schedulers.Schedulers);
		

		readTsk.getTasks(bf);
		System.out.println(readTsk.Tasks.Tasks);
		

		readOper.getOperations(bf);
		System.out.println(readOper.Operations.Operations);
		
		//read the scenarios and make them abstract scenarios

		readScen.getTransactions(bf);
		System.out.println(readScen.Transactions.Transactions);
		
		
		
	}
	
	public ArrayList<ArrayList<String>>getDeadlines()
	{
		return readScen.deadlines;		
	}
	
	

}

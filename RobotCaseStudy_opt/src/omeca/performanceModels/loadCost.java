package omeca.performanceModels;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import omeca.performanceModels.helperClasses.cpuCost;
import omeca.performanceModels.helperClasses.netCost;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


//this file loads the cost of the HW in lists 

public class loadCost {
	
	org.w3c.dom.Document doc;
	File cost_file =  new File(System.getProperty("user.dir") + "/ComponentModels/HW_components.xml");
	
	public ArrayList<cpuCost> cpuCost_= new ArrayList<cpuCost>(); 
	public ArrayList<netCost> netCost_= new ArrayList<netCost>(); 
	
	public loadCost() throws Exception
	{
		
		open();
		loadCPUcost();
		
	}
	 

		
	private void open() throws Exception
	{

		//read xml promo model
		//File fXmlFile = new File(file_path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(cost_file);	
		doc.getDocumentElement().normalize();
		
	}
		
		
	private void loadCPUcost()
	{
		NodeList nList = doc.getElementsByTagName("cpu");
		
		for (int temp = 0; temp < nList.getLength(); temp++) 
		{ 
			Node nNode = nList.item(temp);
			
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				cpuCost tmpCost = new cpuCost();
				Element eElement = (Element) nNode;
				
				tmpCost.id = eElement.getAttribute("id");
				tmpCost.nameOfCPU = eElement.getElementsByTagName("name").item(0).getTextContent();
				tmpCost.priceOfCPU = eElement.getElementsByTagName("price").item(0).getTextContent();
				
				cpuCost_.add(tmpCost);
				
			}
		}	
	}
	
	
	
	private void loadNetcost()
	{
		NodeList nList = doc.getElementsByTagName("net");
		
		netCost tmpCost = new netCost();
		for (int temp = 0; temp < nList.getLength(); temp++) 
		{ 
			Node nNode = nList.item(temp);
			
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{

				Element eElement = (Element) nNode;
				
				tmpCost.id = eElement.getAttribute("id");
				tmpCost.nameOfNet = eElement.getElementsByTagName("name").item(0).getTextContent();
				tmpCost.priceOfNet = eElement.getElementsByTagName("price").item(0).getTextContent();
				
				netCost_.add(tmpCost);
				
			}
		}	
	}
	
}

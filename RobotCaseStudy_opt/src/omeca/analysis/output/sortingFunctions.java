package omeca.analysis.output;

import java.util.ArrayList;

public class sortingFunctions {
	
	public ArrayList<ArrayList<String>>getCores(String node, ArrayList<ArrayList<String>> cpuUtil)
	{
		ArrayList<ArrayList<String>>coreList = new ArrayList<ArrayList<String>>();
		for (int i=0; i < cpuUtil.size(); i++)
		{
			if(cpuUtil.get(i).get(0).contains(node))
			{
				ArrayList<String>core = new ArrayList<String>();
				core.add(cpuUtil.get(i).get(0));
				core.add(cpuUtil.get(i).get(1));
				coreList.add(core);
			}
			
		}
		return coreList;
		
	}
	
	public CPU sortCores(CPU cpu)
	 {
		 CORE tmp = new CORE();
		 int sort;
		 do{
			 sort=0;
			 for (int i=0; i<cpu.Core.size()-1; i++)
			 {
				 if(cpu.Core.get(i).coreNum > cpu.Core.get(i+1).coreNum )
				 {
					 tmp.coreNum = cpu.Core.get(i+1).coreNum;
					 tmp.util = cpu.Core.get(i+1).util;
					 
					 cpu.Core.get(i+1).coreNum =  cpu.Core.get(i).coreNum;
					 cpu.Core.get(i+1).util = cpu.Core.get(i).util;
					 
					 cpu.Core.get(i).coreNum = tmp.coreNum;
					 cpu.Core.get(i).util = tmp.util;
					 
					 sort = 1;
					 
				 }
			 }
			 
		 }while(sort==1);
		 return cpu;
	 }
	
	public ArrayList<CPU> sortCPU( ArrayList<CPU> arrayCPU)
	 {
		 CPU tmp = new CPU();
		 int sort;
		 do{
			 sort=0;
			 for (int i=0; i< arrayCPU.size()-1; i++)
			 {
				 if(arrayCPU.get(i).node > arrayCPU.get(i+1).node )
				 {
					 tmp = arrayCPU.get(i+1);
					 arrayCPU.set(i+1, arrayCPU.get(i));
					 arrayCPU.set(i, tmp);
					 
					 sort = 1;
					 
				 }
			 }
			 
		 }while(sort==1);
		 return arrayCPU;
	 }
	 
	 
	public ArrayList<CPU> getCPU(ArrayList<ArrayList<String>> cpuUtil)
	 {
		 ArrayList<String>nodesArrayList = new ArrayList<String>();
		 ArrayList<CPU> cpuList = new ArrayList<CPU>();

		 String []node;
		 for (int i=0; i < cpuUtil.size(); i++)
		 {
			 node = cpuUtil.get(i).get(0).split("_");
			 if (!nodesArrayList.contains(node[1]) && node[1].contains("n"))
			 {
				 CPU cpu = new CPU();
				 cpu.name = node[0];
				 String nodeNum = node[1].replace("n", "").trim();
				 cpu.node = Integer.parseInt(nodeNum);
				 for (int j=0; j<getCores(node[1], cpuUtil).size(); j++)
				 {
					 CORE core = new CORE();
					 String coreNumber = (getCores(node[1], cpuUtil).get(j).get(0).split("_"))[2].replace("c", "").trim();
					 core.coreNum =  Integer.parseInt(coreNumber);
					 core.util = getCores(node[1], cpuUtil).get(j).get(1);
					 cpu.Core.add(core);
				 }
				 cpu = sortCores(cpu);
				 cpuList.add(cpu);
				 
			 }
		 }
			 
		 return cpuList;
	 }

	 

}

package omeca.analysis.GUI;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import java.awt.Insets;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.Action;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import java.awt.SystemColor;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import omeca.casestudy.runFromGUI;


public class performanceAnalysisGUI{

	private JFrame frmPromartesPerformanceAnalysis;
	private final Action action = new SwingAction();
	private JTextArea logFile;
	File mastModelFile;
	JTextPane welcometextpane = new JTextPane();
	
	schedulingAnalysis schedAnal = new schedulingAnalysis();
	simulationAnalysis simAnal = new simulationAnalysis();
	PAconfClass conf = new PAconfClass();
	private JTextField finalTime;
	private JTextField minTransactions;
	
	JCheckBox schedAnBox;
	JCheckBox simAnBox;
	JCheckBox resourceCheck;
	JComboBox simProfAnalysis;
	JComboBox simNetAnalysis;
	JCheckBox simLogFile;
	JComboBox schedAnalysisType;
	JComboBox schedNetAnalysis;
	JCheckBox schedLogFile;
	JButton startExec;
	JCheckBox diagramCheck;
	JCheckBox end2endCheck;
	private JScrollPane scrollPane;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					performanceAnalysisGUI window = new performanceAnalysisGUI();
					window.frmPromartesPerformanceAnalysis.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public performanceAnalysisGUI() throws IOException {
		initialize();
		
		loadOptions();
		
		
		
	}

	
	public void loadOptions() throws IOException
	{
		
		startExec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//initialize the configuration 
				logFile.append("Starting analysis... \n");
				conf.SchedulingAnalysis = schedAnBox.isSelected();
				conf.SimulationAnalysis = simAnBox.isSelected();	    
				conf.model_name = mastModelFile.getName();
				conf.end2endDiagram = end2endCheck.isSelected();
				conf.procUtilDiagram = diagramCheck.isSelected();
				conf.pieChart = resourceCheck.isSelected();
				logFile.append("With the following configuration: \n");
				if(schedAnBox.isSelected())
				{
					logFile.append("Schedulability Analysis -- [YES] \n");
					logFile.append("Type of analysis " + schedAnalysisType.getSelectedItem().toString() + " \n");
					logFile.append("Type of network analysis " + schedNetAnalysis.getSelectedItem().toString() + " \n");
					if(schedLogFile.isSelected())
					{
						logFile.append("Schedulability Logfile -- [YES] \n");
					}
					else if(!schedLogFile.isSelected())
					{
						logFile.append("Schedulability Logfile -- [NO] \n");
					}
				}
				else if(!schedAnBox.isSelected())
				{
					logFile.append("Schedulability Analysis -- [NO] \n");
					
				}
				
				if(simAnBox.isSelected())
				{
					logFile.append("Simulation Analysis -- [YES] \n");
					logFile.append("Type of analysis " + simProfAnalysis.getSelectedItem().toString() + " \n");
					logFile.append("Type of network analysis " + simNetAnalysis.getSelectedItem().toString() + " \n");
					logFile.append("Execution time " + Long.parseLong(finalTime.getText()) + " \n");
					if(simLogFile.isSelected())
					{
						logFile.append("Simulation Logfile -- [YES] \n");
					}
					else if(!simLogFile.isSelected())
					{
						logFile.append("Simulation Logfile -- [NO] \n");
					}
				}
				else if(!simAnBox.isSelected())
				{
					logFile.append("Simulation Analysis -- [NO] \n");
					
				}
				
				
				schedAnal.typeOfAnalysis = schedAnalysisType.getSelectedItem().toString();
				schedAnal.net = schedNetAnalysis.getSelectedItem().toString();
				schedAnal.logfile = schedLogFile.isSelected();
		
				
				
				simAnal.profileOfAnalysis = simProfAnalysis.getSelectedItem().toString();
				simAnal.net = (String) simNetAnalysis.getSelectedItem();
				simAnal.logfile = simLogFile.isSelected();
				simAnal.finalTime =  Long.parseLong(finalTime.getText());
				simAnal.minNumTransactions = Long.parseLong(minTransactions.getText());
				
				try {
					runFromGUI run = new runFromGUI(conf, schedAnal, simAnal);
					if (run.schecAnalSuccess == false)		
					{
						logFile.append("Attention! The schedulability analysis failed!\n");
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});		
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPromartesPerformanceAnalysis = new JFrame();
		frmPromartesPerformanceAnalysis.setTitle("ProMARTES Performance Analysis Toolkit");
		frmPromartesPerformanceAnalysis.getContentPane().setEnabled(false);
		frmPromartesPerformanceAnalysis.getContentPane().setForeground(Color.WHITE);
		frmPromartesPerformanceAnalysis.getContentPane().setFont(new Font("Dialog", Font.BOLD, 14));
		frmPromartesPerformanceAnalysis.setBounds(100, 100, 900, 600);
		frmPromartesPerformanceAnalysis.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("File");
		menuBar.setMargin(new Insets(10, 10, 10, 10));
		frmPromartesPerformanceAnalysis.setJMenuBar(menuBar);
		
		JMenu FileMenu = new JMenu("File");
		menuBar.add(FileMenu);
		
		JMenuItem Load = new JMenuItem("Load");
		Load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadFile lfile = new loadFile();
				mastModelFile = lfile.loadFile();
				logFile.append("Opening file...  " + mastModelFile.getName() + "\n");
				welcometextpane.setForeground(Color.BLUE);
				welcometextpane.setText("Successfully loaded the " + mastModelFile.getName() + " system model!");
				
				startExec.setEnabled(true);
			}
		});
		Load.setAction(action);
		FileMenu.add(Load);
		
		JMenuItem Exit = new JMenuItem("Exit");
		FileMenu.add(Exit);
		
		JMenu systemAnalysis = new JMenu("Analysis");
		menuBar.add(systemAnalysis);

		welcometextpane.setForeground(Color.RED);
		welcometextpane.setFont(new Font("Dialog", Font.BOLD, 14));
		welcometextpane.setEditable(false);
		
		welcometextpane.setText("Please Load a Performance Model");
		
		schedAnBox = new JCheckBox("Scheduling Analysis");
		schedAnBox.setSelected(true);
		
		
		simAnBox = new JCheckBox("Simulation Analysis");
		simAnBox.setSelected(true);
		
		JLabel lblPerformanceAnalysisMethod = new JLabel("Performance Analysis method");
		lblPerformanceAnalysisMethod.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JSeparator separator = new JSeparator();
		
	

		
		startExec = new JButton("Start Analysis");
		startExec.setEnabled(false);
		startExec.setFont(new Font("Tahoma", Font.BOLD, 18));
	
		
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Scheduling Analylsis", null, panel_1, null);
		
		schedAnalysisType = new JComboBox();
		schedAnalysisType.setModel(new DefaultComboBoxModel(new String[] {"Offset-Based Optimized", "Offset-Based", "Holistic", "Varying Priorities", "Classic RM", "EDF"}));
		schedAnalysisType.setSelectedIndex(2);
		
		schedLogFile = new JCheckBox("Logfile");
		schedLogFile.setSelected(true);
		
		schedNetAnalysis = new JComboBox();
		schedNetAnalysis.setModel(new DefaultComboBoxModel(new String[] {"WNDP", "NS3"}));
		schedNetAnalysis.setSelectedIndex(0);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(schedLogFile)
							.addContainerGap())
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(schedNetAnalysis, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(schedAnalysisType, Alignment.LEADING, 0, 196, Short.MAX_VALUE))
							.addContainerGap())))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(46)
					.addComponent(schedAnalysisType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(37)
					.addComponent(schedNetAnalysis, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(47)
					.addComponent(schedLogFile)
					.addContainerGap(128, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Simulation Analysis", null, panel_2, null);
		
		simProfAnalysis = new JComboBox();
		simProfAnalysis.setModel(new DefaultComboBoxModel(new String[] {"SCHEDULABILITY", "PERFORMANCE", "TRACES", "VERBOSE"}));
		simProfAnalysis.setSelectedIndex(1);
		
		simNetAnalysis = new JComboBox();
		simNetAnalysis.setMaximumRowCount(2);
		simNetAnalysis.setModel(new DefaultComboBoxModel(new String[] {"WNDP", "NS3"}));
		simNetAnalysis.setSelectedIndex(0);
		
		finalTime = new JTextField();
		finalTime.setText("1000");
		finalTime.setColumns(10);
		
		minTransactions = new JTextField();
		minTransactions.setText("1000");
		minTransactions.setColumns(10);
		
		simLogFile = new JCheckBox("Logfile");
		simLogFile.setSelected(true);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(simLogFile, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
						.addComponent(finalTime, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(minTransactions, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(simNetAnalysis, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
						.addComponent(simProfAnalysis, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(221, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(42)
					.addComponent(simProfAnalysis, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(31)
					.addComponent(simNetAnalysis, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addComponent(finalTime, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addComponent(minTransactions, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(simLogFile)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Output", null, panel, null);
		tabbedPane.setEnabledAt(2, true);
		
		resourceCheck = new JCheckBox("Resource utilization text files");
		resourceCheck.setSelected(true);
		resourceCheck.setBackground(SystemColor.menu);
		resourceCheck.setHorizontalAlignment(SwingConstants.LEFT);
		
		diagramCheck = new JCheckBox("Resource Utilization diagram");
		diagramCheck.setSelected(true);
		diagramCheck.setBackground(SystemColor.menu);
		diagramCheck.setHorizontalAlignment(SwingConstants.LEFT);
		
		end2endCheck = new JCheckBox("End to end delay diagram");
		end2endCheck.setSelected(true);
		end2endCheck.setHorizontalAlignment(SwingConstants.LEFT);
		end2endCheck.setBackground(SystemColor.menu);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(16)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(diagramCheck, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(end2endCheck, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(resourceCheck, Alignment.LEADING))
					.addContainerGap(249, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(30)
					.addComponent(resourceCheck)
					.addGap(46)
					.addComponent(end2endCheck)
					.addGap(50)
					.addComponent(diagramCheck)
					.addContainerGap(126, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JLabel lblOptions = new JLabel("Options");
		lblOptions.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(frmPromartesPerformanceAnalysis.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addComponent(welcometextpane, GroupLayout.PREFERRED_SIZE, 900, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 861, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(separator, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(schedAnBox)
									.addGap(53)
									.addComponent(simAnBox))
								.addComponent(startExec, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblPerformanceAnalysisMethod))
							.addGap(79)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblOptions, GroupLayout.PREFERRED_SIZE, 243, GroupLayout.PREFERRED_SIZE)
								.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(welcometextpane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPerformanceAnalysisMethod)
						.addComponent(lblOptions, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(schedAnBox)
								.addComponent(simAnBox))
							.addGap(18)
							.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(68)
							.addComponent(startExec, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE))
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 316, GroupLayout.PREFERRED_SIZE))
					.addGap(40)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
					.addGap(156))
		);
		
		logFile = new JTextArea();
		logFile.setFont(new Font("Dialog", Font.BOLD, 12));
		scrollPane.setViewportView(logFile);
		logFile.setRows(10);
		logFile.setEditable(false);
		logFile.setText("Welcome to Performance Analysis toolkit! \n\n");
		logFile.setColumns(30);
		frmPromartesPerformanceAnalysis.getContentPane().setLayout(groupLayout);
		
		
		
		

			

	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Load model");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}

package omeca.analysis.GUI;
import javax.accessibility.Accessible;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;


import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.*;
 
/*
 * FileChooserDemo.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class loadFile extends JPanel{
    static private final String newline = "\n";
    JTextArea log;
    JFileChooser fc;
    File file;
 
    public File loadFile() 
    {
        //super(new BorderLayout());
 
    	lastOpenedFile lof = new lastOpenedFile();
    	String path = lof.getLastOpenedFilePath();
    	
    	if (path == null)
    	{
    		path =  System.getProperty("user.dir") + "/models";
    		fc = new JFileChooser(new File(path));
    		
    	}
    
    	else
    	{
    		fc = new JFileChooser(path);
    		
    	}
 
        
 
        //Uncomment one of the following lines to try a different
        //file selection mode.  The first allows just directories
        //to be selected (and, at least in the Java look and feel,
        //shown).  The second allows both files and directories
        //to be selected.  If you leave these lines commented out,
        //then the default mode (FILES_ONLY) will be used.
        //
        //fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        fc.showOpenDialog(null);
        file = fc.getSelectedFile();
        
        //pass the path to the conf file
        lof.wtiteToFile(System.getProperty("user.dir") + "/configuration/lastOpenedFilePath", file.getParent());
            
        return file;
    }
 
 
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = loadFile.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
 

}

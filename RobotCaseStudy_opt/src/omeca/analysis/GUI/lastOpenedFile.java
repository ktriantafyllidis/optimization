package omeca.analysis.GUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class lastOpenedFile {
	
		
		private File file = null;
		
		PrintWriter pw;
		public BufferedReader bf;
	    String line, next;
	    String path = null;
		
		private void openFile(String filePath)
		{
			try {
				bf  = new BufferedReader(new FileReader(filePath));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//maybe i have to change this
			try {
				bf.mark(1000000);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
		
		public String getLastOpenedFilePath()
		{		
		  	this.openFile(System.getProperty("user.dir") + "/configuration/lastOpenedFilePath");
		  	path = "";
			try 
			{
				for (line = bf.readLine(); line != null; line = next)
				{
					next = bf.readLine();
					
					path += line;
				}
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try 
			{
				bf.close();
			} catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return path;

		}

		
		
		public void wtiteToFile(String filePath, String path)
		{
			
			
			file = new File(filePath);
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
			
			try {
				pw = new PrintWriter(new FileOutputStream(file));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			pw.println(path);
			pw.close();
			
		}
		


}

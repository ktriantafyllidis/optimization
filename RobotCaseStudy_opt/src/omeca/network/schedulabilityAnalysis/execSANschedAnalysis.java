package omeca.network.schedulabilityAnalysis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class execSANschedAnalysis {
	
	
	/*
	 * This method performs the network simulation based on the tool of Waqar
	 * */
	public void executeMathematica()
	{
		//necessitates the existence of mathematica
		//for Linux machines => 
		String fire_simulation;
		StringBuffer output = new StringBuffer();

		//if (System.getProperty("os.name").equals("Linux"))
		//{predictnwdelay-tcp-v3.m
			fire_simulation = "math -noprompt -script NetworkSimulator/predictnwdelay-tcp-v3-31dec13.m";
			//System.out.println ("OS= " + System.getProperty("os.name"));
		//}
		
		try 
		{
            Process lChldProc = Runtime.getRuntime().exec(fire_simulation);
            try 
            {
                lChldProc.waitFor();
                //container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));    
            } 
            catch (InterruptedException ex) 
            {
                	lChldProc.destroy();    
            }
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(lChldProc.getInputStream()));
            String line = "";			
            while ((line = reader.readLine())!= null) 
            {
            	output.append(line + "\n");
            }
        } 
		catch (IOException ex) 
		{
        }    	
		
		System.out.println(output);
		
	}
	
	
	/*
	 * This method reads the delays from Waqar file and puts them into the MAST file-model
	 * */
	public void MastNetDelays()
	{
	}	

}

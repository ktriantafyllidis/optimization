package omeca.casestudy.convsersion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/*This is a class that changes the messages values form message size to 
 * message delays in order to support the network simulators implemented 
 * by VCA
 * *
 */
public class MsgSize2MsgDelay {
	
	private Document doc;
	private String modelPath;
	
	
	/**This method opens the file 
	 * 
	 * @param path: This is the path of the file to be opened
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * */
	public void openMast2Model(String modelPath)
	{
		this.modelPath = modelPath;
		
		File file = new File(modelPath);
		 
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		try {
			this.doc = dBuilder.parse(file);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	 private void saveToXML(File file, Document doc) {
         try {
              TransformerFactory factory = TransformerFactory.newInstance();
              Transformer transformer = factory.newTransformer();
              transformer.setOutputProperty(OutputKeys.INDENT, "yes");

              StringWriter writer = new StringWriter();
              StreamResult result = new StreamResult(writer);
              DOMSource source = new DOMSource(doc);

              transformer.transform(source, result);

              String strTemp = writer.toString();

              FileWriter fileWriter = new FileWriter(file);
              BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

              bufferedWriter.write(strTemp);
              bufferedWriter.flush();
              bufferedWriter.close();
         }
         catch(Exception ex) {
         }
    }
	
	
	
	 /** This method converts the network message size to message delays as the 
	  * VCA network analysis tools have computed.
	  * 
	  * @param ArrayList<ArrayList<String>> delays: Is the arrayList with the network delays
	  * */
	public void convertMsgSize2MsgDelay(ArrayList<ArrayList<String>> delays)
	{
		
	    NodeList nodeList = doc.getDocumentElement().getChildNodes();

	    for (int i = 0; i < nodeList.getLength(); i++) 
	    {
	    	 Node node = nodeList.item(i);

	    	 if (node instanceof Element)
	    	 {
	    		 
	    		 if(node.getNodeName().equals("mast_mdl:Message"))
	    		 {
	    			 
	    			 //open the file with the delays and load the data by name of the message transaction
	    			 for (int j=0; j<delays.size(); j++)
	    			 {
	    				 if(delays.get(j).get(0).toLowerCase().equals(((Element) node).getAttribute("Name")))
	    				 {
	    					 //remove the old attributes connected to the message size
	    					 ((Element) node).removeAttribute("Max_Message_Size");
	    					 ((Element) node).removeAttribute("Min_Message_Size");
	    					 ((Element) node).removeAttribute("Avg_Message_Size");
	    					 
	    					 //add the new attributes containing the message delays 
    						 ((Element) node).setAttribute("Best_Case_Execution_Time", delays.get(j).get(1)+"E-3");
    						 ((Element) node).setAttribute("Avg_Case_Execution_Time", delays.get(j).get(2)+"E-3");
    						 ((Element) node).setAttribute("Worst_Case_Execution_Time", delays.get(j).get(3)+"E-3");
	    						 
	    					 
	    				 }
	    			 }
	    			
	    			 
	    		 }
	  
	    	 }
	    	 
	    	
	    }
	
	    File file = new File(modelPath);
	    saveToXML(file, doc); 

		
	}
	
	
	
	public void convertMsgSize2MsgDelayOnlyAverage(ArrayList<ArrayList<String>> delays)
	{
		
	    NodeList nodeList = doc.getDocumentElement().getChildNodes();
	    int size = delays.get(0).size();

	    for (int i = 0; i < nodeList.getLength(); i++) 
	    {
	    	 Node node = nodeList.item(i);

	    	 if (node instanceof Element)
	    	 {
	    		 
	    		 if(node.getNodeName().equals("mast_mdl:Message"))
	    		 {
	    			 
	    			 //open the file with the delays and load the data by name of the message transaction
	    			 for (int j=0; j<delays.size(); j++)
	    			 {
	    				 if(delays.get(j).get(0).toLowerCase().equals(((Element) node).getAttribute("Name")))
	    				 {
	    					 //remove the old attributes connected to the message size
	    					 ((Element) node).removeAttribute("Max_Message_Size");
	    					 ((Element) node).removeAttribute("Min_Message_Size");
	    					 ((Element) node).removeAttribute("Avg_Message_Size");
	    					 
	    					 //add the new attributes containing the message delays 
	    					 if (size > 2)
	    					 {
	    						 ((Element) node).setAttribute("Best_Case_Execution_Time", delays.get(j).get(2)+"E-3");
	    						 ((Element) node).setAttribute("Avg_Case_Execution_Time", delays.get(j).get(2)+"E-3");
	    						 ((Element) node).setAttribute("Worst_Case_Execution_Time", delays.get(j).get(2)+"E-3");
	    					 } 
	    					 if (size == 2)
	    					 {
	    						 ((Element) node).setAttribute("Best_Case_Execution_Time", delays.get(j).get(1)+"E-3");
	    						 ((Element) node).setAttribute("Avg_Case_Execution_Time", delays.get(j).get(1)+"E-3");
	    						 ((Element) node).setAttribute("Worst_Case_Execution_Time", delays.get(j).get(1)+"E-3");
	    					 }
	    					 
	    				 }
	    			 }
	    			
	    			 
	    		 }
	  
	    	 }
	    	 
	    	
	    }
	
	    File file = new File(modelPath);
	    saveToXML(file, doc); 

		
	}
	
	
	
	

}

package omeca.casestudy.evaluator;

import java.util.ArrayList;

public class schedAnalysisType {
	
	/*
	 * analysisType options:
	 *  - offset_based_optimized
	 *  - offset_based
	 *  - hollistic
	 *  - edf_within_priorities
	 *  - edf_monoprocessor
	 *  - varying_priorities
	 *  - classic_rm
	 * * */
	
	String typeOfAnalysis;
	
	public void setTypeOfAnalysis(String typeOfAnalysis)
	{
		this.typeOfAnalysis = typeOfAnalysis;
	}
	
	public String getTypeOfAnalysis()
	{
		return this.typeOfAnalysis;
	}
	

}

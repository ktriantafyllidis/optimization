package omeca.casestudy.evaluator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import omeca.casestudy.objectives.Objective;



/**
 * MAST analysis                          
 *<p>
 * Schedulability analysis of models compatible with MAST model format    
 * 
 * <p>
 * For now we support a standard analysis, passing the path
 * of the model input and output.
 *
 * @param  String Input path of the model
 * @param  String Output path/file name of the analysis results. 
 */

public class Evaluator_Schedulability_Analysis {

	protected final String model, output, AnalysisType;
	protected Collection<Objective> objectives = new ArrayList<Objective>();
	
	public Evaluator_Schedulability_Analysis(String analType, String model_path, String outputFile) {
		this.model = model_path;
		this.output = outputFile;
		this.AnalysisType = analType;
		new DummyAnalyzer(this.AnalysisType, this.model, this.output);
	}
	
	
}

class DummyAnalyzer 
{
	
	String outputFile;
	
	public DummyAnalyzer(String AnalysisType, String MASTmodel, String outputFile) {
		this.outputFile = outputFile;

		String mast_path;
		StringBuffer output = new StringBuffer();
		
		//get the name of operating system

		
		if (System.getProperty("os.name").equals("Linux"))
		{
			mast_path = "./mast_linux/mast_analysis";
			//System.out.println ("OS= " + System.getProperty("os.name"));
		}
		
		else
		{
			mast_path = "cmd /c start mast/mast_analysis.exe";
			//System.out.println ("OS = " + System.getProperty("os.name"));
		}
		
		/*        Choose the tool for the analysis
		 * tool_name:
				  parse
		          classic_rm
		          varying_priorities
		          edf_monoprocessor
		          edf_within_priorities
		          holistic
		          offset_based_optimized
		          offset_based
		 */
		
		String path = mast_path +" " + AnalysisType + " -v -l -c -p "+ MASTmodel + " " + outputFile;
	
        try 
        {

            Process lChldProc = Runtime.getRuntime().exec(path);
            try 
            {
                lChldProc.waitFor();
                //container.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            } 
            catch (InterruptedException ex) 
            {
                	lChldProc.destroy();    
            }
			
            BufferedReader reader = new BufferedReader(new InputStreamReader(lChldProc.getInputStream()));
            String line = "";			
            while ((line = reader.readLine())!= null) 
            {
            	output.append(line + "\n");
            }
            
        } 
        catch (IOException ex) 
        {
        }
        
		System.out.println(output);
	}
	
}
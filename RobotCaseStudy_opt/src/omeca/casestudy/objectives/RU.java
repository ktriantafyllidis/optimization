package omeca.casestudy.objectives;

public class RU {
	
	private float util_core;
	private float util_network;
	private RU_objectives ru = new RU_objectives();
	private String nameOfResource;
	private String utilValueString;
	
	private int core_cnt = 0;
	private int net_cnt = 0;
	
	
	public RU(ResourcesUtilization util)
	{
	
		float utilVal = 0.0f;
		
		//read the array and distinguish network from CPU cores 
		for (int i = 0; i <  util.resourcesUtilization.size(); i++)
		{		
			nameOfResource = util.resourcesUtilization.get(i).get(0);
			utilValueString = util.resourcesUtilization.get(i).get(1);
			utilVal = Float.parseFloat(utilValueString);
			
			if (nameOfResource.contains("_pc"))
			{
				//network metrics
				update_RU_net_w(utilVal);
				update_RU_net_a(utilVal);
			}
			else 
			{
				//core metrics
				update_RU_core_w(utilVal);
				update_RU_core_a(utilVal);
			}
						
		}
		
		ru.coreUtil_a = ru.coreUtil_a / core_cnt;
		ru.netUtil_a = ru.netUtil_a / net_cnt;
	}
	
	
	private void update_RU_core_w(float utilVal)
	{
		if (Float.parseFloat(ru.coreUtil_w.get(1)) < utilVal)
		{
			ru.coreUtil_w.set(0, nameOfResource);
			ru.coreUtil_w.set(1, Float.toString(utilVal));
		}
		
	}
	
	
	private void update_RU_core_a(float utilVal)
	{		
		ru.coreUtil_a += utilVal ;
		core_cnt++;	
	}
	
	
	private void update_RU_net_w(float utilVal)
	{
		if (Float.parseFloat(ru.netUtil_w.get(1)) < utilVal)
		{
			ru.netUtil_w.set(0, nameOfResource);
			ru.netUtil_w.set(1, Float.toString(utilVal));
		}
		
	}
	
	private void update_RU_net_a(float utilVal)
	{		
		ru.netUtil_a += utilVal ;
		net_cnt++;	
	}
	
	
	
	public float getCoreUtil_a()
	{
		return ru.coreUtil_a;
	}
	
	public float getCoreUtil_w()
	{
		return  Float.parseFloat(ru.coreUtil_w.get(1));
	}
	
	
	public float getNetUtil_a()
	{
		return ru.netUtil_a;
	}
	
	public float getNetUtil_w()
	{
		return  Float.parseFloat(ru.netUtil_w.get(1));
	}
	

}

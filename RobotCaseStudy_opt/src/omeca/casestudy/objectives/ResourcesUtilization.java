package omeca.casestudy.objectives;

import java.util.ArrayList;

public class ResourcesUtilization implements Objective {

	protected final ArrayList<ArrayList<String>> resourcesUtilization;
	
	public ResourcesUtilization(ArrayList<ArrayList<String>> ResourcesUtilization) {
		this.resourcesUtilization = ResourcesUtilization;
	}
	
	@Override
	public ArrayList<ArrayList<String>> value() {
		return resourcesUtilization;
	}
	
	public void printResourcesUtilization()
	{
		System.out.println(this.resourcesUtilization);
	}

}

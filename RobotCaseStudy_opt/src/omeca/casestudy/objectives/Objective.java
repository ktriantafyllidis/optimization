package omeca.casestudy.objectives;

import java.util.ArrayList;

public interface Objective {

	public ArrayList<ArrayList<String>> value();
	
}

package omeca.casestudy.objectives;

import java.util.ArrayList;

public class NetworkUtilization implements Objective{
	
	protected final ArrayList<ArrayList<String>> networkUtilization;
	
	public NetworkUtilization(ArrayList<ArrayList<String>> networkUtilization) 
	{
		this.networkUtilization = networkUtilization;
	}
	
	@Override
	public ArrayList<ArrayList<String>> value() 
	{
		return networkUtilization;
	}
	
	public void printNetworkUtilization()
	{
		System.out.println(this.networkUtilization);
	}

}

package omeca.casestudy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jfree.ui.RefineryUtilities;

import omeca.analysis.GUI.PAconfClass;
import omeca.analysis.GUI.schedulingAnalysis;
import omeca.analysis.GUI.simulationAnalysis;
import omeca.analysis.classes.utilization;
import omeca.analysis.output.outputLog;
import omeca.analysis.output.readScheAnalysisResults;
import omeca.analysis.output.readSimulationResults;
import omeca.analysis.output.charts.end2endLineChart;
import omeca.analysis.output.charts.pieChart;
import omeca.analysis.output.charts.schedProcUtilChart;
import omeca.analysis.output.charts.simProcUtilChart;
import omeca.casestudy.convsersion.MsgSize2MsgDelay;
import omeca.casestudy.convsersion.mast_to_mast2;
import omeca.casestudy.evaluator.Evaluator_Schedulability_Analysis;
import omeca.casestudy.evaluator.Evaluator_Simulation;
import omeca.casestudy.evaluator.schedAnalysisType;
import omeca.casestudy.objectives.NetworkUtilization;
import omeca.casestudy.objectives.ResourcesUtilization;
import omeca.casestudy.objectives.ResponseTime;
import omeca.mastModel.readMastModel.readMastFile;
import omeca.network.inputfiles.NetworkTransmissions;
import omeca.network.schedulabilityAnalysis.convertModelToDelays;
import omeca.network.schedulabilityAnalysis.execSANschedAnalysis;
import omeca.network.simulationAnalysis.execNS3simAnalysis;

public class runFromGUI {
	
	
	public boolean schecAnalSuccess = true;
	

	
	public runFromGUI(PAconfClass conf, schedulingAnalysis schedAnal, simulationAnalysis simAnal) throws IOException
	{
		String currentDir = System.getProperty("user.dir");
		String outputFile = currentDir + File.separator + "output_files" + File.separator + conf.model_name+ ".out";
		readScheAnalysisResults schedRes = null;
		readSimulationResults simRes = null;
		ArrayList<ArrayList<String>>  Sched_getCPUUtilization = null;
		ArrayList<ArrayList<String>>  getResponseDelays = null;
		ArrayList<ArrayList<String>>  Sim_getCPUUtilization = null;
		
		//1st i apply the scheduling analysis
		//check the network configuration -- fix the path
		NetworkTransmissions Net = new NetworkTransmissions();		
		Net.getNetworkTransfers(new File("models" + File.separator + conf.model_name));
		convertModelToDelays delays = new convertModelToDelays();
		if(schedAnal.net == "NS3")
		{
			//test
			new execNS3simAnalysis();
			delays.passToMastModel(currentDir + File.separator + "NetworkSimulatorNS3" + File.separator + "delays.txt", currentDir + 
					 File.separator + "models" + File.separator + conf.model_name);
		}
		else
		{
			execSANschedAnalysis net_sim = new execSANschedAnalysis();
			net_sim.executeMathematica();
		
			// read the delays from the delays.txt file and put them into the mast file
			delays.passToMastModel(currentDir + File.separator + "NetworkSimulator" + File.separator +"delays.txt", currentDir + File.separator +
					"models" + File.separator + conf.model_name);
		}
		
		if(simAnal.net == "NS3")
		{
			//test
			new execNS3simAnalysis();
			delays.passToMastModel(currentDir + File.separator + "NetworkSimulatorNS3" + File.separator + "delays.txt", currentDir + 
					File.separator + "models" + File.separator + conf.model_name);
		}
		else
		{
			execSANschedAnalysis net_sim = new execSANschedAnalysis();
			net_sim.executeMathematica();
		
			// read the delays from the delays.txt file and put them into the mast file
			delays.passToMastModel(currentDir + File.separator + "NetworkSimulator" + File.separator + "delays.txt", currentDir + 
					File.separator + "models" + File.separator + conf.model_name);
		}
		
		
		if(conf.SchedulingAnalysis)
		{
			schedAnalysisType SchedType = new schedAnalysisType();
			SchedType.setTypeOfAnalysis(schedAnal.typeOfAnalysis);
			Evaluator_Schedulability_Analysis evaluator = new Evaluator_Schedulability_Analysis(SchedType.getTypeOfAnalysis(), "NetworkSimulator" 
			+ File.separator + "robot1", outputFile);
			
			schedRes = new readScheAnalysisResults();
			schedRes.openOutputFile(currentDir + File.separator + "output_files" + File.separator + conf.model_name + ".out");
			Sched_getCPUUtilization = schedRes.getCPUUtilization();
			ResourcesUtilization SchedulabilityResources_Util = new ResourcesUtilization(Sched_getCPUUtilization);
			SchedulabilityResources_Util.printResourcesUtilization();
	 		schedRes.close();
			schedRes.openOutputFile(currentDir + File.separator + "output_files" + File.separator + conf.model_name + ".out");
			getResponseDelays = schedRes.getResponseDelays();
			ResponseTime SchedulabilityRes_Time = new ResponseTime(getResponseDelays);
			SchedulabilityRes_Time.printResponseTime();
			schedRes.close();
			
			
			if(Sched_getCPUUtilization.isEmpty())
			{
				schecAnalSuccess = false; 
				
			}
		

			//print the logfiles
	 		if (schedAnal.logfile)
	 		{
			
				
				//create logfile for delays of scheduling analysis
				outputLog fileSched = new outputLog();
				fileSched.newFile(currentDir + File.separator + "output_files" + File.separator + conf.model_name + ".schedulabilityResults");
				fileSched.writeToFile(SchedulabilityResources_Util.value(), "Processor Utilization");
				fileSched.writeToFile(SchedulabilityRes_Time.value(), "Response time of events");
				fileSched.close();
	 		}
		}
		
		if (conf.SimulationAnalysis)
		{
			String model2name = conf.model_name.replace(".mast", "");
			mast_to_mast2 mast2 = new mast_to_mast2(currentDir + File.separator + "models", conf.model_name, currentDir + 
					File.separator + "models_simulation", model2name + "2");
			mast2.packBasedNet_to_packBasedNetVCA(currentDir + File.separator + "models_simulation" + File.separator + model2name+"2.mdl.xml");	
			//open and read the mast2 xml file 
			MsgSize2MsgDelay model = new MsgSize2MsgDelay();
			model.openMast2Model(currentDir + File.separator + "models_simulation" + File.separator + model2name+"2.mdl.xml");
			model.convertMsgSize2MsgDelayOnlyAverage(delays.getNetworkDelays());
		
			String modelName = model2name + "2.mdl.xml";
			String resultsName = model2name + "2.res.xml";
			String tracesName = model2name + "2.trc.xml";
			
			String profile_combo = simAnal.profileOfAnalysis;
			
			long finalTime = simAnal.finalTime;
			long minNumTransactions = simAnal.minNumTransactions;
			
			Evaluator_Simulation sim_eval = new Evaluator_Simulation(modelName, resultsName, tracesName, profile_combo, finalTime, minNumTransactions);

			
			//show the results
			simRes = new readSimulationResults();
			//this is enabled for test
			simRes.getSimulationResults(currentDir, resultsName); //this is the version with the classes UNTESTED
			simRes.openOutputFile(currentDir + File.separator + "models_simulation" + File.separator + resultsName);
			Sim_getCPUUtilization = simRes.getCPUUtilization();
			ResourcesUtilization SimulationCPU_Util = new ResourcesUtilization(Sim_getCPUUtilization);
			SimulationCPU_Util.printResourcesUtilization();

			NetworkUtilization SimulationNet_Util = new NetworkUtilization(simRes.getNetworkUtilization());
			SimulationNet_Util.printNetworkUtilization();
			
			ResponseTime SimulationRes_Time = new ResponseTime(simRes.getResponseDelays());
			SimulationRes_Time.printResponseTime();
			
			if(simAnal.logfile)
			{
				//print to file
				outputLog fileSim = new outputLog();
				fileSim.newFile(currentDir + File.separator + "output_files" + File.separator + conf.model_name + ".simulationResults");
				fileSim.writeToFile(SimulationCPU_Util.value(), "Processor Utilization");
				fileSim.writeToFile(SimulationNet_Util.value(), "Network Utilization");
				fileSim.writeToFile(SimulationRes_Time.value(), "Response time of events");
				fileSim.close();
						
			}
				
		}
		
		if (conf.SchedulingAnalysis && conf.SimulationAnalysis)
		{
			/*
			if(conf.pieChart)
			{
		        pieChart demo = new pieChart("Comparison", "Which operating system are you using?");
		        demo.pack();
		        demo.setVisible(true);
				
			}*/
			
			if (conf.end2endDiagram)
			{
				readMastFile mastFile = new readMastFile(new File("models" + File.separator + conf.model_name));
				ArrayList<ArrayList<String>>deadlines = mastFile.getDeadlines();
		        end2endLineChart end2endDiagram = new end2endLineChart(simRes.getResponseDelays(),getResponseDelays,deadlines);
		        end2endDiagram.pack();
		        RefineryUtilities.centerFrameOnScreen(end2endDiagram);
		        //RefineryUtilities.positionFrameOnScreen(end2endDiagram, 0, 50);
		        end2endDiagram.setVisible(true);
				
			}
			
			if (conf.procUtilDiagram)
			{
				if (schecAnalSuccess == true)
				{
			        schedProcUtilChart schedprocUtilChart = new schedProcUtilChart(Sched_getCPUUtilization);
			        schedprocUtilChart.pack();
			        //RefineryUtilities.centerFrameOnScreen(schedprocUtilChart);
			        //RefineryUtilities.positionFrameOnScreen(schedprocUtilChart, 50, 50);
			        schedprocUtilChart.setVisible(true);
				}
		        simProcUtilChart simprocUtilChart = new simProcUtilChart(Sim_getCPUUtilization);
		        simprocUtilChart.pack();
		       // RefineryUtilities.centerFrameOnScreen(simprocUtilChart);
		        simprocUtilChart.setVisible(true);
				
			}

			
		}
		

		
	
	}
	
	
	
	

}

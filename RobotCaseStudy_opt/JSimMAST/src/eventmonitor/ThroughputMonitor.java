/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SuspendTimeMonitor
 * Description: Monitor register the suspend Time statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import core.Clock;
import flow.FlowEndPoint;
import flow.FlowEvent;

public class ThroughputMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private long lastEventOccurrence;
	private long acumIntearrivingTime;
	private long numEvents; 
	
	
	/* METHODS */

	// Constructor
	public ThroughputMonitor(FlowEndPoint fep){
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		if (numEvents > 0) {
			acumIntearrivingTime += Clock.currentTime() - lastEventOccurrence;
		}
		numEvents++;
		lastEventOccurrence = Clock.currentTime();
		// 1/th = acum / numEvents
	}

	public String simTimingResultsAttr(String offset){
		return "";
	}
	
	public String simTimingResultsElem(String offset){return "";}
}
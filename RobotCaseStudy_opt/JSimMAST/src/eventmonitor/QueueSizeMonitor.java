package eventmonitor;

import flow.FlowEndPoint;
import flow.FlowEvent;
import flow.Segment;

public class QueueSizeMonitor extends Statistical_EM{
	
	/* ATTRIBUTES */
	
	private long numOfQueuedActivations = 0;
	private long currentQueueSize=0;
	
	/* METHODS */
	
	// Constructor
	public QueueSizeMonitor(FlowEndPoint fep){
		super(fep);
	}

	public void event(FlowEvent fev) {
		Segment precedingSegment = (Segment)hostFEP.source();
		FlowEndPoint previousFEP = precedingSegment.getInputFEP(0);
		//numOfQueuedActivations = previousFEP.numPendingFlowEvent();
		currentQueueSize =previousFEP.numPendingFlowEvent();
		if (currentQueueSize>numOfQueuedActivations)    //JMD
			numOfQueuedActivations=currentQueueSize;    //JMD
	}
	
	public String simTimingResultsAttr(String offset){
		return "\n" + offset + "Num_Of_Queued_Activations=\"" + numOfQueuedActivations + "\"";
	}
	
	public String simTimingResultsElem(String offset){return "";}
}

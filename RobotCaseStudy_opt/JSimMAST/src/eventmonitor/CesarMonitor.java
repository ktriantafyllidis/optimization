/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: CesarMonitor
 * Description: 
 * Author: C.C.C.
 * Date: 24-01-2012
 ********************************************************************/

package eventmonitor;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import flow.FlowEndPoint;
import flow.FlowEvent;
import flow.WorkloadEvent;

public class CesarMonitor extends EventMonitor {

	/* ATTRIBUTES */
	
	private List<GlobalResponseTimeData> dataList;
		
	
    /* METHODS */
	
    // Constructor
	public CesarMonitor(FlowEndPoint fep, WorkloadEvent workloadEvent){
		super(fep);
		dataList = new ArrayList<GlobalResponseTimeData>();
		for (WorkloadEvent wEvent : hostFEP.ownerTrans().getWorkloadEvents()) {
			dataList.add(new GlobalResponseTimeData(wEvent));
		}
	}
	
	public void event(FlowEvent fev) {
		for (GlobalResponseTimeData data : dataList) {
			data.event(fev);
		}
	}
	

	/* INNER CLASSES */
	
	private class GlobalResponseTimeData{
		
		/* CONSTANTS */
		
		private final int GLOBAL_TIME_QUEUE_MAX_SIZE = 1001;
		
		
		/* ATTRIBUTES */
		
		private long numEvents = 0;
		private WorkloadEvent referenced;
		
		private PriorityQueue<Long> globalTimeQueue = new PriorityQueue<Long>();
		
		
		/* METHODS */
		
		// Constructor
		public GlobalResponseTimeData(WorkloadEvent wEvent){
			referenced = wEvent;
		}
		
		public void event(FlowEvent fev){
			long globalTime = fev.globalTime(referenced);
			if (globalTime >= 0) {
				//
				if (globalTimeQueue.size() < GLOBAL_TIME_QUEUE_MAX_SIZE)
					globalTimeQueue.add(globalTime);
				else {
					if(globalTime > globalTimeQueue.peek()) {
						globalTimeQueue.poll();
						globalTimeQueue.add(globalTime);
					}
				}
				numEvents++;
				System.out.println(globalTime);
			}
		}
		
	}
	
}

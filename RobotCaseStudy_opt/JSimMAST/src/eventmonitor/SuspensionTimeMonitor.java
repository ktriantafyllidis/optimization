/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SuspensionTimeMonitor
 * Description: Monitor register the suspension Time statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import core.Clock;
import flow.*;

public class SuspensionTimeMonitor extends Statistical_EM {

	/* ATTRIBUTES */
	
	private long maxSuspensionTime = 0; // ns
	private long acumSuspensionTime = 0;  // ns
	private long numEvents = 0;
	
	/* METHODS */

	// Constructor
	public SuspensionTimeMonitor(FlowEndPoint fep){
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		acumSuspensionTime += fev.suspensionTime();
		numEvents++;
		if (numEvents == 1)
			maxSuspensionTime = fev.suspensionTime();
		else if (fev.suspensionTime()> maxSuspensionTime)
			maxSuspensionTime = fev.suspensionTime(); 
	}

	public String simTimingResultsAttr(String offset){
		if (numEvents != 0)
			return "\n" + offset + "Max_Suspension_Time=\"" + Clock.timeSeconds(maxSuspensionTime) + 
		           "\" Avg_Suspension_Time=\"" + Clock.timeSeconds(acumSuspensionTime / numEvents) + "\"";
		else
			return "";
    }
	
	public String simTimingResultsElem(String offset){return "";}
}
/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: LogOnEvent
 * Description: Generates a trace for each registered event
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import flow.*;
import core.*;

public class LogOnEvent extends LoggingMonitor {
	
	/* METHODS */
	
	// Constructor
	public LogOnEvent(FlowEndPoint fep){
		//super(fep, Logger.SrcType.WorkloadEvent);
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		Logger.registerNewTrace(hostFEP.sID(), Logger.WORKLOAD_EVENT_GENERATED);
	}
	
}

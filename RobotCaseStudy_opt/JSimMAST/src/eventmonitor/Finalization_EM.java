package eventmonitor;

import core.ConditionalStop;
import flow.FlowEndPoint;

public abstract class Finalization_EM extends EventMonitor implements ConditionalStop{

	/* FIELDS */
	
	protected boolean stopCondition = false;
	
    
    /* METHODS*/
    
    // Constructor
	protected Finalization_EM(FlowEndPoint fep){
		super(fep);
	}
	
	public boolean stop() {
		return stopCondition;
	}
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: GlobalTimeMonitor
 * Description: Mmonitor register the GlobalTime statistical data
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

import utilityClasses.FilesManager;

import core.Clock;
import core.Repository;

import exceptions.JSimMastException;
import flow.FlowEndPoint;
import flow.FlowEvent;
import flow.WorkloadEvent;

public class GlobalTimeMonitor extends Statistical_EM {

	/* CONSTANTS */
	
	private String EVENT_NAME = "End_Main";
	
	
	/* ATTRIBUTES */
	
	private List<GlobalResponseTimeData> dataList;
	private String nominalDeadline;
	private int workloadEventIndex;
	private String fullFepName;
	
	
    /* METHODS */
	
    // Constructor
	public GlobalTimeMonitor(FlowEndPoint fep, String nominalDeadline, WorkloadEvent workloadEvent){
		super(fep);
		this.nominalDeadline = nominalDeadline;
		fullFepName = hostFEP.ownerTrans().name() + "/" + hostFEP.name();
		dataList = new ArrayList<GlobalResponseTimeData>();
		for (WorkloadEvent wEvent : hostFEP.ownerTrans().getWorkloadEvents()) {
			dataList.add(new GlobalResponseTimeData(wEvent));
			if (wEvent == workloadEvent) {
				workloadEventIndex = hostFEP.ownerTrans().getWorkloadEvents().indexOf(wEvent);
			}
		}
	}
	
	public void event(FlowEvent fev) {
		for (GlobalResponseTimeData data : dataList) {
			data.event(fev);
		}
	}
	
	public String simTimingResultsAttr(String offset){return "";}
	
	public String simTimingResultsElem(String offset){
		// I write results relative to every referenced workload event
		String string = "";
		String text = "";
		String targetPath = "";
		for (GlobalResponseTimeData data : dataList) {
			if (data.numEvents != 0)
				string += offset + "<mast_res:Worst_Global_Response_Time Referenced_Event=\"" + data.referenced.name() + 
				                   "\" Value=\"" + Clock.timeSeconds(data.worst) + "\"/>\n" +
					      offset + "<mast_res:Avg_Global_Response_Time Referenced_Event=\"" + data.referenced.name() + 
					               "\" Value=\"" + Clock.timeSeconds(data.acum/data.numEvents) + "\"/>\n" +
					      offset + "<mast_res:Best_Global_Response_Time Referenced_Event=\"" + data.referenced.name() + 
					               "\" Value=\"" + Clock.timeSeconds(data.best) + "\"/>\n";
			/* 
			 * I create a file with the list of global response times relative to the current ref event
			 * I filter to avoid the creation of multiple files.
			 */
			if (hostFEP.name().equals(EVENT_NAME)) { // 
				targetPath = "./outputFiles/" + Repository.modelName() + "_" +  
				                                hostFEP.ownerTrans().name() + "_" +
				                                hostFEP.name() + "_" +
				                                data.referenced.name() + ".txt";
				// Content of the file: header
				text += "E2EF: " + hostFEP.ownerTrans().name() + "\n";
				text += "Event: " + hostFEP.name() + "\n";
				text += "Workload event: " + data.referenced.name() + "\n";
				text += "Mostrando los ultimos #" + data.globalTimeQueue.size() + "# tiempos de respuesta de un total de #" + data.numEvents + "#\n";
				
				// Content of the file: list
				Object[] globalTimeArray = data.globalTimeQueue.toArray();
				Arrays.sort(globalTimeArray);
				for (Object globalTime : globalTimeArray) {
					text += "\n" + globalTime.toString();
				}
				
				try {
					FilesManager.createFile(targetPath);
					FilesManager.fillTextFile(text, targetPath);				
				} catch (JSimMastException e) {
					e.printStackTrace();
				}
			}
		}
		return string;
	}
	

	@Override
	public String eventNum(){
		return "" + dataList.get(workloadEventIndex).numEvents;
	}
	
	@Override
	public String worstResponseTime(){
		return "" + dataList.get(workloadEventIndex).worst;
	}
	
	@Override
	public String nominalDeadline(){
		return nominalDeadline;
	}
	
	@Override
	public String confidenceLevel(){
		return "";
	}
	
	@Override
	public String fullFepName(){
		return fullFepName;
	}
	
	
	/* INNER CLASSES */
	
	private class GlobalResponseTimeData{
		
		/* CONSTANTS */
		
		private final int GLOBAL_TIME_QUEUE_MAX_SIZE = 1001;
		
		
		/* ATTRIBUTES */
		
		private long worst; // ns
		private long best; // ns
		private long acum; // ns
		private long numEvents = 0;
		private WorkloadEvent referenced;
		
		private PriorityQueue<Long> globalTimeQueue = new PriorityQueue<Long>();
		
		
		/* METHODS */
		
		// Constructor
		public GlobalResponseTimeData(WorkloadEvent wEvent){
			referenced = wEvent;
		}
		
		public void event(FlowEvent fev){
			long globalTime = fev.globalTime(referenced);
			if (globalTime >= 0) {
				//
				//globalTimeList.add(globalTime);
				if (globalTimeQueue.size() < GLOBAL_TIME_QUEUE_MAX_SIZE)
					globalTimeQueue.add(globalTime);
				else {
					if(globalTime > globalTimeQueue.peek()) {
						globalTimeQueue.poll();
						globalTimeQueue.add(globalTime);
					}
				}
				//
				acum += globalTime;
				numEvents++;
				if (numEvents == 1) {
					worst = globalTime;
					best = globalTime;
				} else {
					if (globalTime > worst)
						worst = globalTime;
					if (globalTime < best)
						best = globalTime;
				}
			} // else => globalTime = -1, debido a que el evento no ha sido generado por referenced
			// si no se hace esta comprobación, numEvents nunca seria 0
		}
		
	}
	
}

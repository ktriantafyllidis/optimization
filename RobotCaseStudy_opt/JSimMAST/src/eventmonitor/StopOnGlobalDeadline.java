/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: StopOnGlobalDeadline
 * Description: The simulation is stopped when a global deadline 
 *        is not satified
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package eventmonitor;

import flow.*;

public class StopOnGlobalDeadline extends Finalization_EM {

	/* METHODS */
	
	// Constructor
	public StopOnGlobalDeadline(FlowEndPoint fep){
		super(fep);
	}
	
	public void event(FlowEvent fev) {
		// TODO implement event()
	}

}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: MessageDispatcher
 * Description: It is an activity which represents the transmition of
 *      a message. 
 * 
 * Author: J.M. Drake
 * Date: 10-4-11
 ********************************************************************/

package flow;

import module.ETG;
import operation.Message;
import resource.PacketBasedNetwork;
import core.Controller;
import core.Logger;

public class MessageDispatcher extends Segment {

	
	/* FIELDS */
	
	private long maxPacketTime;
	private long minPacketTime;
		public long maxPacketTime() {return maxPacketTime;}	
		public long minPacketTime() {return minPacketTime;}

	// Flow end points to which a flow event has to be sent before/after a packet is transmited.
	private FlowEndPoint preEventTarget;
	private FlowEndPoint postEventTarget;
		public void setPreEventTarget(FlowEndPoint preEventTarget) {this.preEventTarget = preEventTarget;}	
		public void setPostEventTarget(FlowEndPoint postEventTarget) {this.postEventTarget = postEventTarget;}
  
	//
	private Step assignedStep;
	
	// Generator of time needed to send a message through the net.
	private ETG messageTimeGenerator;
		public ETG messageTimeGenerator() {return messageTimeGenerator;}
  
	
	/* METHODS */
	
	// Constructor based on message transmission activity
	public MessageDispatcher(Step step){
		
		/* ************************ SAME AS FOR SEGMENT ************************ */
		
		// name = step.name() + "-mssgDispatcher";
		
		/* I get the server, the trans. and the inputFEP of the step and 
		 * I set them as server, trans. and inputFEP for the message dispatcher */
		ownerTrans = step.ownerTrans();
		executor = step.server();
		inputFepList.add(step.getInputFEP(0)); //inputFepList = step.inputFepList;
		inputFepList.get(0).setTarget(this);
		
		/* ********************************************************************* */
		
		assignedStep = step;
		
		/* ************************ SAME AS FOR SEGMENT ************************ */
		
		/* I get the outputFEP of the step and 
		 * I set it as outputFEP for the message dispatcher */
		outputFepList.add(step.getOutputFEP(0)); //outputFepList = step.outputFepList;
		outputFepList.get(0).setSource(this);
		
		name = outputFepList.get(0).name() + "-messageDispatcher";
		
		// I register a source in the Logger
		if (Controller.profile() == Controller.ProfileType.TRACES)
			sid = Logger.registerNewSource(ownerTrans.name() + "/" + name, Logger.SrcType.Segment);
		
		/*if (Controller.profile() == Controller.ProfileType.TRACES){
			
			if (outputFepList.get(0).sID() == -1)
				outputFepList.get(0).setSID(sid);
			
			new LogOnEvent(outputFepList.get(0), midFlowEvent);
			
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.FREE));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.PROCESSING));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.BLOCKED));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.SUSPENDED));
			
		}*/
	}
	 
	@Override
	public void complete(){
		PacketBasedNetwork net = (PacketBasedNetwork)executor.getProcessingResource(); //().scheduler().host());
		maxPacketTime = net.maxPacketTransmissionTime();
		minPacketTime = net.minPacketTransmissionTime();  
		messageTimeGenerator = ((Message)(assignedStep.operation())).etg().clone();
	}
	
	@Override
	public void denormalize() {
		
		//super.denormalize();
	
			// The throughtput is already considered into the speedFactor
			messageTimeGenerator.denormalize(executor.speedFactor() * executor.throughput() / 1.0E9);
		
	
	}
	
	@Override
	public void finish() {
		if (isLast()){
			outputFepList.get(0).addFlowEvent(currentEvent());
			setCurrentStep(null);
			if (inputFepList.get(0).numPendingFlowEvent() > 0){
				//Se inicia la transmision del proximo mensaje
				setCurrentStep(null);
				arriveNewEvent();
			} else {
				// No existen transmisiones pendientes
				setStepIndex(0);
			}
		} else {
			//A�n quedan del mensaje paquetes pendientes para enviar 
			setStepIndex(stepIndex()+1);
			setCurrentStep(stepList.get(stepIndex()));
			currentEvent().setExecutionTim(currentStep().etg().next());
			executor().require(this);
		}
	}
	
	/* 
	 * Evaluate the current packet size of context switch
	 */	
	private long contextSwitchPacket(){
		if (this.executor.scheduler().contextSwitch()!=null){
			return this.executor.scheduler().contextSwitch().next();
		}else return 0;
	}
	/* 
	 * This method refills the activity list once is empty and then calls Segment.arriveNewEvent()
	 */
	@Override
	public void arriveNewEvent(){ //int inputIndex){
			
		if (currentStep() == null) {

			stepList.clear();
			
			// I evaluate the message time
			long currentMessageTime = messageTimeGenerator.next();
			
			while(currentMessageTime>0){
				// Add contextSwitchPacket
				currentMessageTime+=contextSwitchPacket();
				
/*				//testline gadem
				if (minPacketTime == 0)
				{
					System.out.println("Hello");
				}
				*/
				if (currentMessageTime <= minPacketTime){
					stepList.add(new Step(minPacketTime));
					break;
				}else if (currentMessageTime > maxPacketTime){
					stepList.add(new Step(maxPacketTime));
					currentMessageTime-=maxPacketTime;
				}else{
					stepList.add(new Step(currentMessageTime));
					break;
				}
			}
			super.arriveNewEvent();
		}
	}

	
	
	@Override
	public String toString(){
		String string = "\nName: " + name +
			            "\nInput fep: " + inputFepList.get(0).name() +
                        "\nOutput fep: " + outputFepList.get(0).name() + 
                        "\nExecutor" + executor.name() + 
                        "\nMax packet time = " + maxPacketTime + 
		                "\nMin packet time = " + minPacketTime + 
		                "\nTransmission time = " + messageTimeGenerator.toString();
		return "\nMessage dispatcher:" + string.replaceAll("\n", "\n\t");
	}
}

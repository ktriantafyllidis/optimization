/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Segment
 * Description: It represent a segment inside of a Transaction. A 
 *      segment is a flow sequence of activities, all them executed
 *      in the same scheduling server, and between them there is not
 *      context switch process.  
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import operation.SimpleOperation;

import org.w3c.dom.Element;

import resource.SimThread;
import core.Controller;
import core.Logger;
import core.Repository;
import eventmonitor.BlockingTimeMonitor;
import eventmonitor.DisplayOnMissingGlobalDeadline;
import eventmonitor.DisplayOnMissingLocalDeadline;
import eventmonitor.GlobalMissingRatioMonitor;
import eventmonitor.GlobalTimeMonitor;
import eventmonitor.JitterMonitor;
import eventmonitor.LocalMissingRatioMonitor;
import eventmonitor.LocalTimeMonitor;
import eventmonitor.LogOnLocalDeadline;
import eventmonitor.QueueSizeMonitor;
import eventmonitor.StopOnNumEvent;
import eventmonitor.SuspensionTimeMonitor;
import eventmonitor.ThroughputMonitor;
import eventmonitor.WaitingTimeMonitor;

public class Segment extends EventHandler {
	
	/* ATTRIBUTES */

	// Ordered list of the segment activities.
	protected List<Step> stepList = new ArrayList<Step>();
	
	// Reference to the activity which is currently processing in the segment
	private Step currentStep;
		public Step currentStep(){return currentStep;}
		public void setCurrentStep(Step step){currentStep = step;}
		
	// Reference to the flow event which is currently being attended in the segment
	private FlowEvent currentEvent;
		public FlowEvent currentEvent(){return currentEvent;}
		public void setCurrentEvent(FlowEvent ev){currentEvent = ev;}
  
	//
	private int stepIndex = 0;
		public int stepIndex(){return stepIndex;}
		public void setStepIndex(int index){stepIndex = index;}
  
	// Reference to the server in which the segment is executed.
	protected SimThread executor;
		public SimThread executor(){return executor;}
		public void setExecutor(SimThread server){executor = server;}
	
	//
	protected int sid;
		public int sid(){return sid;}
	
	
	/* METHODS */
	
	// Constructors
	public Segment(){}
	public Segment(Step firstStep){
	  
		//name = firstStep.name() + "-segment";
		
		/* I get the server, the trans. and the inputFEP of the 1st step and
		 * I set them as server, trans. and inputFEP for the segment */
		ownerTrans = firstStep.ownerTrans();
		executor = firstStep.server();
		inputFepList.add(firstStep.getInputFEP(0));
		inputFepList.get(0).setTarget(this);
		
						/*isSystemTimed = firstActivity.isSystemTimed();
					  		if (isSystemTimed){
						  	ProcessingSupplier server=executor;
						  	while (SchedulableResource.class==server.getClass()){
								server=((SchedulableResource)server).scheduler().host();
						  	}
						  	inputFEP.setTimer(((RegularProcessor)server).systemTimer());
					  	}*/
	  
		// I fill the step list
		Step step = firstStep;
		stepList.add(step);
		while(step.getOutputFEP(0).target() != null){
			if (step.getOutputFEP(0).target().getClass() != Step.class)
				break;
			else if (((Step)step.getOutputFEP(0).target()).server() != executor)
				break;
			step = (Step)step.getOutputFEP(0).target();
			stepList.add(step);
		}
						/*EventHandler stepTarget = step.getOutputFEP(0).target(); 
						while(stepTarget != null){
							if (stepTarget.getClass() != Step.class || ((Step)stepTarget).server() != executor)
								break;
							else { 
								step = (Step)stepTarget;
								stepList.add(step);
							}
							stepTarget = step.getOutputFEP(0).target();
						}*/
		
		/* I get the outputFEP of the last step and 
		 * I set it as outputFEP for the segment */
		Step lastStep = stepList.get(stepList.size() - 1);
		outputFepList.add(lastStep.getOutputFEP(0)); //outputFepList = lastStep.outputFepList;
		outputFepList.get(0).setSource(this);
		
		name = outputFepList.get(0).name() + "-segment";
		
		// I register a source in the Logger
		if (Controller.profile() == Controller.ProfileType.TRACES)
			if (ownerTrans != null)
				sid = Logger.registerNewSource(ownerTrans.name() + "/" + name, Logger.SrcType.Segment);
			else
				sid = Logger.registerNewSource(name, Logger.SrcType.Segment);
		
		// I deploy each step in the step list, so the step list will grow up
		ArrayList<Step> tempActList = new ArrayList<Step>();
		Iterator<Step> iter = stepList.iterator();
	 	while(iter.hasNext()){
	 		tempActList.addAll(iter.next().deploy());
	 	}
	 	stepList = tempActList;
		
		/*if (Controller.profile() == Controller.ProfileType.TRACES){
			
			if (outputFepList.get(0).sID() == -1)
				outputFepList.get(0).setSID(sid);
			
			new LogOnEvent(outputFepList.get(0), midFlowEvent);
		*/	
			/*executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.FREE));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.PROCESSING));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.BLOCKED));
			executor.addStateMonitor(new LogOnStateMod(executor, BasicSchedulableResource.SUSPENDED));*/			
	}

	public void complete(){}
	
	public void postComplete(){}
	
	public void denormalize() {
		for (Step step : stepList) {
			if (step.operation() instanceof SimpleOperation)
				step.etg().denormalize(executor.speedFactor());
		}
	}
	
	public void arriveNewEvent(){
		if (currentStep == null){
			
			// I set the scenario to execute the 1st step of the segment
			stepIndex = 0;
			currentStep = stepList.get(stepIndex);
			
			// I set the current flow event the one polled by the input FEP and I configure it
			currentEvent = inputFepList.get(0).pollPendingFlowEvent();
			//currentEvent.setActivationTime(Clock.currentTime());
			currentEvent.setExecutionTim(currentStep.etg().next());
			
			// I require the execution of that 1st step
			executor.require(this);
		}
	}

	/**
	 * This method is conceived to allow a server to report when 
	 * the execution of a step of the current segment has been completed.
	 */
	public void finish() {
		
		if (isLast()){ // the executed step is the last one of the current segment
			
			// I transfer the flow event to the outputFEP of the segment 
			outputFepList.get(0).addFlowEvent(currentEvent);
			
			if (inputFepList.get(0).numPendingFlowEvent() > 0){ // there are pending flow events				
				// same code as for arriveNewEvent()
				stepIndex = 0;
				currentStep = stepList.get(stepIndex);
				currentEvent = inputFepList.get(0).pollPendingFlowEvent();
				currentEvent.setExecutionTim(currentStep.etg().next());
				executor.require(this);
			} else {
			  currentStep = null;
			  stepIndex = 0;
			}
		} else {
			stepIndex++;
		  	currentStep = stepList.get(stepIndex);
		  	currentEvent.setExecutionTim(currentStep.etg().next());
		  	executor.require(this);
	  	}
	}
	
	public boolean isLast() {
		return stepIndex == (stepList.size() - 1);
	}

	public void addEventMonitors(){
		
		// I add event monitors to the output FEP, depending on the simulation profile
		
		FlowEndPoint outputFEP = outputFepList.get(0);
		
		switch (Controller.profile()) {
		case SCHEDULABILITY:
		case PERFORMANCE:
			
			/* Finalization event monitors */
			
			//if (Controller.transExecution() > 0)
				outputFEP.addEventMonitor(new StopOnNumEvent(outputFEP));
	
			
			/* Statistical event monitors */
			
			// I add the corresponding event monitors to the outputFEP if it has any kind of requirement.
			if (outputFEP.hasReqs()) {
				outputFEP.addEventMonitor(new SuspensionTimeMonitor(outputFEP));
				outputFEP.addEventMonitor(new BlockingTimeMonitor(outputFEP));
				outputFEP.addEventMonitor(new WaitingTimeMonitor(outputFEP));
				outputFEP.addEventMonitor(new QueueSizeMonitor(outputFEP));
				if (outputFEP.hasHardLocalDeadlines()) {
					Iterator<Element> iter = outputFEP.getHardLocalDeadlineIter();
					LocalTimeMonitor localTimeMonitor = new LocalTimeMonitor(outputFEP, iter.next().getAttribute("Deadline"));
					outputFEP.addEventMonitor(localTimeMonitor);
					Repository.addTimeMonitor(localTimeMonitor);
				} else
					outputFEP.addEventMonitor(new LocalTimeMonitor(outputFEP, ""));
				
				if (outputFEP.target() != null) { // not sink
					outputFEP.addEventMonitor(new JitterMonitor(outputFEP));
					outputFEP.addEventMonitor(new ThroughputMonitor(outputFEP));
					if (outputFEP.hasHardGlobalDeadlines()) {
						Iterator<Element> iter = outputFEP.getHardGlobalDeadlineIter();
						Element firstHardGlobalDeadlineElem = iter.next();
						String refEvent = firstHardGlobalDeadlineElem.getAttribute("Referenced_Event");
						String deadline = firstHardGlobalDeadlineElem.getAttribute("Deadline");
						WorkloadEvent wEvent = (WorkloadEvent)ownerTrans.getEventHandler(refEvent);
						GlobalTimeMonitor globalTimeMonitor = new GlobalTimeMonitor(outputFEP, deadline, wEvent);
						outputFEP.addEventMonitor(globalTimeMonitor);
						Repository.addTimeMonitor(globalTimeMonitor);
					} else
						outputFEP.addEventMonitor(new GlobalTimeMonitor(outputFEP, "", null));
				}
			}
			
			// For each outputFEP's local max miss ratio requirement, I add a LocalMissingRatioMonitor
			if (outputFEP.hasLocalMaxMissRatios()) {
				Iterator<Element> iter = outputFEP.getLocalMaxMissRatioIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new LocalMissingRatioMonitor(outputFEP, iter.next()));				
				}
			}
			// For each outputFEP's global max miss ratio requirement, I add a GlobalMissingRatioMonitor
			if (outputFEP.hasGlobalMaxMissRatios()) {
				Iterator<Element> iter = outputFEP.getGlobalMaxMissRatioIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new GlobalMissingRatioMonitor(outputFEP, iter.next()));			
				}
			}
			
			/* Notification event monitors */
			
			// For each outputFEP's hard local deadline, I add a DisplayOnMissingLocalDeadline
			if (outputFEP.hasHardLocalDeadlines()) {
				Iterator<Element> iter = outputFEP.getHardLocalDeadlineIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new DisplayOnMissingLocalDeadline(outputFEP, iter.next()));			
				}
			}
			// For each outputFEP's hard global deadline, I add a DisplayOnMissingGlobalDeadline
			if (outputFEP.hasHardGlobalDeadlines()) {
				Iterator<Element> iter = outputFEP.getHardGlobalDeadlineIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new DisplayOnMissingGlobalDeadline(outputFEP, iter.next()));				
				}
			}
			break;
		case VERBOSE:
			// For each outputFEP's hard local deadline, I add a DisplayOnMissingLocalDeadline
			if (outputFEP.hasHardLocalDeadlines()) {
				Iterator<Element> iter = outputFEP.getHardLocalDeadlineIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new DisplayOnMissingLocalDeadline(outputFEP, iter.next()));					
				}
			}
			// For each outputFEP's hard global deadline, I add a DisplayOnMissingGlobalDeadline
			if (outputFEP.hasHardGlobalDeadlines()) {
				Iterator<Element> iter = outputFEP.getHardGlobalDeadlineIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new DisplayOnMissingGlobalDeadline(outputFEP, iter.next()));					
				}
			}
			break;
		default: // TRACES
			// For each outputFEP's hard local deadline, I add a LogOnLocalDeadline
			if (outputFEP.hasHardLocalDeadlines()) {
				Iterator<Element> iter = outputFEP.getHardLocalDeadlineIter();
				while (iter.hasNext()) {
					outputFEP.addEventMonitor(new LogOnLocalDeadline(outputFEP, iter.next()));					
				}
			}
			break;
		}
		// I add other event monitors regarding the current segment
		// ... 
	}

	
	@Override
	public String toString(){
		String string = "\nInput fep: " + inputFepList.get(0).name() +
                        "\nOutput fep: " + outputFepList.get(0).name() + 
                        "\nExecutor: " + executor.name() + 
		                "\nList of simple operations:";
		for (Step step : stepList) {
			if (step.operation() instanceof SimpleOperation)
				string += "\n\t" + step.operation().name();
		}
		return "\nSegment:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}
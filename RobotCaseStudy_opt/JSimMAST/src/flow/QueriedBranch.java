/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: private List<String> operRefList = new ArrayList<String>();
 * Description: It is an event handler that generates one event in only
 *     one of its outputs each time an input event arrives. The output
 *     path is chosen at the time of the event consumption by one of
 *     the activities connected to an output event. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package flow;

import module.Tools;

import org.w3c.dom.*;

import core.*;
import java.util.*;

public class QueriedBranch extends EventHandler implements Decisor {
	
	/* ENUMS */
	private enum RequestPolicyType {SCAN, 
		                            FIFO, 
		                            LIFO, 
		                            PRIORITY};
	
	
	/* ATTRIBUTES */
	
	// The policy set in the query server.
	private RequestPolicyType requestPolicy = RequestPolicyType.SCAN;

    
	
	/* METHODS */
	
	// Constructor
	public QueriedBranch(Node node, RegularEndToEndFlow trans) {
		
		super(node, trans);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Request_Policy"))
			requestPolicy = RequestPolicyType.valueOf(elem.getAttribute("Request_Policy"));
	}
	
	public void complete(){
		
		Element elem = (Element)node;
		
		// I get the inputFEP & outputFEPs from the owner transaction
		
		FlowEndPoint fep = ownerTrans.getFEP(elem.getAttribute("Input_Event"));
		fep.setTarget(this);
		inputFepList.add(fep);
		
		List<String> outputEventList = Tools.parses(elem.getAttribute("Output_Events_List"));
      	for(int i = 0; i < outputEventList.size(); i++){ 
      		fep = ownerTrans.getFEP(outputEventList.get(i));
      		fep.setSource(this);
      		outputFepList.add(fep);
      		//
      		fep = new FlowEndPoint(fep.name() + "_controlInput", ownerTrans);
      		fep.setTarget(this);
      		inputFepList.add(i + 1, fep); //inputFepList.set(i + 1, fep);
      		ownerTrans.addFEP(fep);
      	}
	}
	
	public void postComplete(){
		FlowEndPoint fep;
		for(int i = 0; i < outputFepList.size(); i++){ 
			fep = outputFepList.get(i);
			Fork.setQueryFork((Step)fep.target(), inputFepList.get(index + 1));
		}
	}
  
	@Override
	public void arriveNewEvent() {
		// TODO implement arriveNewEvent()
	}

	public void decide() {
		// TODO implement decide()
	}

	// The input index (0<=index<number_of_output) is connected
	/*public void setRequestInput(int index, Fork fork){
		FlowEndPoint fep = inputFepList.get(index + 1);
		fep.setSource(fork);
		fork.outputFepList.set(1, fep);
	}*/
	
	

	@Override
	public String toString(){	
		String string = "\nInput fep: " + inputFepList.get(0).name() +
		                "\nOutput feps:";
		for (FlowEndPoint fep : outputFepList) {
			string += "\n\t" + fep.name();
		}
		string += "\nControl input feps:";
		for (int i = 1; i < inputFepList.size(); i++) {
			string += "\n\t" + inputFepList.get(i).name();
		}
		string += "\nRequest policy: " + requestPolicy;
		return "\nQuery server:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Offset
 * Description: It is similar to the Delay event handler, except that
 *      the time interval is counted relative to the arrival of some
 *      (previous) event. If the time interval has already passed when
 *      the input event arrives, the output event is generated
 *      immediately. 
 * 
 * Author: J.M. Drake y F. Aguilar
 * Date: 1-1-09
 ********************************************************************/

package flow;

import org.w3c.dom.*;

import core.Clock;

public class Offset extends Delay {
	
	/* ATTRIBUTES */
	
	// External event which is referenced 
	private FlowEndPoint refFEP;
	
	//
	private int elapsedDeadline = 0;
	
	
	/* METHODS */
	
	// Constructor
	public Offset(Node node, RegularEndToEndFlow trans) {
		super(node, trans);
	}
    
	@Override
	public void complete(){
		
		super.complete();
		
		Element elem = (Element)node;
		
		refFEP = new FlowEndPoint(name + "_ref", ownerTrans);
		refFEP.setTarget(this);
		inputFepList.add(refFEP);
		ownerTrans.addFEP(refFEP);
		
		String refName = elem.getAttribute("Referenced_Event");
	  	Fork auxExternalEvent = (Fork)ownerTrans.getEventHandler(refName + "_auxFork");
	  	refFEP.setSource(auxExternalEvent);
	  	auxExternalEvent.addOutputFEP(refFEP);
	}
	
	
    @Override
	public void arriveNewEvent() {
    	if ((inputFepList.get(0).numPendingFlowEvent() > 0) && (elapsedDeadline > 0)){
    		elapsedDeadline = elapsedDeadline - 1;
			outputFepList.get(0).addFlowEvent(inputFepList.get(0).pollPendingFlowEvent());
		}
		if (inputFepList.get(1).numPendingFlowEvent() > 0){
			FlowEvent ev = inputFepList.get(1).pollPendingFlowEvent();
			ev.setActivationTime(Clock.currentTime() + delay().next());
			if (delayedEvents.isEmpty()) {
				delayedEvents.add(ev);
				Clock.addTimedEvent(ev.activationTime(), this);		  
			} else if (delayedEvents.peek().activationTime() <= ev.activationTime())
				delayedEvents.add(ev);
			else {
				Clock.cancelTimedEvent(this);
				Clock.addTimedEvent(ev.activationTime(), this);
				delayedEvents.add(ev);
			}
		}
    }
	  
	@Override
	public void update() {
		  delayedEvents.poll().freeFlowEvent();
		  if(inputFepList.get(0).numPendingFlowEvent() > 0)
			  outputFepList.get(0).addFlowEvent(inputFepList.get(0).pollPendingFlowEvent());
		  else 
			  elapsedDeadline++;
		  
		  //while (delayedEvents().peek().activationTime()<=Clock.currentTime()){
		  while (!delayedEvents.isEmpty() && delayedEvents.peek().activationTime() <= Clock.currentTime()){
			  delayedEvents.poll().freeFlowEvent();
			  if(inputFepList.get(0).numPendingFlowEvent() > 0)
				  outputFepList.get(0).addFlowEvent(inputFepList.get(0).pollPendingFlowEvent());
			  else 
				  elapsedDeadline++;
		  }
	  }


	
	@Override
	public String toString(){
		String string = "\nReferenced fep: " + refFEP.name();
		return "\nOffset:" + (super.toString() + string).replaceAll("\n", "\n\t"); 
		                     
	}
}

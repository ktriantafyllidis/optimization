package operation;

import org.w3c.dom.Node;

public abstract class CodeOperation extends Operation {
	
	/* METHODS */
	
	// Constructor
	protected CodeOperation(Node node) {
		super(node);
	}
}

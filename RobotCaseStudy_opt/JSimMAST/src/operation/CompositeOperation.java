/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: EnclosingOperation
 * Description: Represents an operation composed of an ordered sequence
 *    of other operations, simple or composite. The execution time
 *    attribute of this class cannot be set, because it is the sum of
 *    the execution times of the comprised operations.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package operation;

import core.*;
import org.w3c.dom.*;

import utilityClasses.XMLparser;

import java.util.*;

public class CompositeOperation extends CodeOperation {

	/* FIELDS */
	
	// List of operations
	private List<String> operRefList = new ArrayList<String>();
	private List<CodeOperation> operList = new ArrayList<CodeOperation>();
		public Iterator<CodeOperation> getOperListIter(){return operList.iterator();}
	
		
	/* METHODS */
	
	// Constructor
	public CompositeOperation(Node node){
		super(node);
		List<Element> compOperChildElemList = XMLparser.getChildElem(node);
		for (Element compOperChildElem : compOperChildElemList) {
			if(compOperChildElem.getLocalName().equals("Operation"))
				operRefList.add(compOperChildElem.getAttribute("Name"));
		}
	}
	
	@Override
	public void complete(){
		for (String operRef : operRefList) {
			operList.add(((CodeOperation)Repository.getModelElem(operRef, "Operation")));
		}
	}
	

	
	@Override
	public String toString(){
		String string = "\nList of operations:";
		for (Operation oper : operList) {
			string += "\n\t" + oper.name();
		}
		return "\n\nComposite operation:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
	
}
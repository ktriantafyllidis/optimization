/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: EnclosingOperation
 * Description: Represents an operation that contains other operations
 *      as part of its execution. The execution time is not the sum of
 *      execution times of the comprised operations, because other
 *      pieces of code may be executed in addition. The enclosed
 *      operations need to be considered for the purpose of calculating
 *      the blocking times associated with their shared resource usage.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package operation;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class EnclosingOperation extends CompositeOperation {

	/* FIELDS */
	
	// For messages, this represents the transmission time.
	private ETG etg;
		public ETG etg() {return etg;}
		
	
	/* METHODS */
	
	// Constructor
	public EnclosingOperation(Node node) {
		
		super(node);

		Element elem = (Element)node;
		
		long worst = 0;
		if (elem.hasAttribute("Worst_Case_Execution_Time"))
			worst = Tools.readLong(elem.getAttribute("Worst_Case_Execution_Time"));
		
		long avg = 0;
		if (elem.hasAttribute("Avg_Case_Execution_Time"))
			avg = Tools.readLong(elem.getAttribute("Avg_Case_Execution_Time"));
		
		long best = 0;
		if (elem.hasAttribute("Best_Case_Execution_Time"))
			best = Tools.readLong(elem.getAttribute("Best_Case_Execution_Time"));
		
		etg = new WABETG(worst, avg, best);
	}
	
	
	
	@Override
	public String toString(){
		return "\n\nEnclosing operation" + (super.toString() + etg.toString()).replaceAll("\n", "\n\t");
	}
	
}

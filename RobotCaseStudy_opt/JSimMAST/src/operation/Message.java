/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Abstract class: Message
 * Description: Represents a message to be transmitted through a
 *      network. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package operation;

import module.ConstantETG;
import module.ETG;
import module.Tools;
import module.WABETG;
import module.WABETG_VCA_Simulator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Message extends Operation{

	/* FIELDS */
	
	// For messages, this represents the transmission time.
	private ETG etg;
		public ETG etg(){return etg;}
	
		
	/* METHODS */
		
	// Constructors
	public Message(Node node) {
		
		super(node);
		
		int execTime = 0;
		
		Element elem = (Element)node;
		
		long max = 0;
		if (elem.hasAttribute("Max_Message_Size"))
			max = Long.parseLong(elem.getAttribute("Max_Message_Size"));
		
		long avg = 0;
		if (elem.hasAttribute("Avg_Message_Size"))
			avg = Long.parseLong(elem.getAttribute("Avg_Message_Size"));
		
		long min = 0;
		if (elem.hasAttribute("Min_Message_Size"))			
			min = Long.parseLong(elem.getAttribute("Min_Message_Size"));
		
		//gadem for the WCET
		long wcet = 0;
		if (elem.hasAttribute("Worst_Case_Execution_Time"))
		{
			wcet = Tools.readLong(elem.getAttribute("Worst_Case_Execution_Time"));
			execTime++;
		}
		long acet = 0;
		if (elem.hasAttribute("Avg_Case_Execution_Time"))
		{
			acet = Tools.readLong(elem.getAttribute("Avg_Case_Execution_Time"));
			execTime++;
		}
		
		long bcet = 0;
		if (elem.hasAttribute("Best_Case_Execution_Time"))			
		{
			bcet = Tools.readLong(elem.getAttribute("Best_Case_Execution_Time"));
			execTime++;
		}
		
		
		
		// TODO consider the case of Normalized_Execution_Times attributes
		
		
		
		
		//check if we have execTime or messageSize
		if (execTime == 0)
		{
			etg = new WABETG(max, avg, min);  // the ETG is number of bits * 1E9
		}
		else 
		{
			etg = new WABETG_VCA_Simulator(wcet, acet, bcet);
			
		}
	}
	
	public Message(long messageTime) {
		etg = new ConstantETG(messageTime);
	}
	
	
	
	@Override
	public String toString(){
		String string = "\nTransmission time = " + etg.toString();
		return "\n\nMessage transmission:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: TimingObject
 * Description: It models the processor overhead introduce by timer
 * 		service managing and the granularity of the time 
 * 
 * Author: J.M. Drake y D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package timers;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


import core.ModelElement;

public abstract class TimingObject extends ModelElement{
	
	/* FIELDS */
	
	protected long precision = 0;
		public long precision() {return precision;}
	
	/* METHODS */
		
	// Constructor
	protected TimingObject(Node node) {
		super(node);
		Element elem = (Element)node;
		if (elem.hasAttribute("Precision"))
			precision = Tools.readLong(elem.getAttribute("Precision"));
	}
	
	public static TimingObject newTimer(Node node){
		if (node.getLocalName().equals("Ticker"))
	  		return new Ticker(node);
		else if (node.getLocalName().equals("Alarm_Clock"))
		  	return new AlarmClock(node);
		else // node.getLocalName().equals("Synchronization_Timer")
			return new ClockSynchronizationObject(node);
	}
	
	public void complete() {}

}
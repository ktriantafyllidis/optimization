/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Alarm Clock
 * Description: This represents systems in which timed events are
 *     activated by a hardware timer interrupt. The timer is programmed
 *     always to generate the interrupt at the time of the closest
 *     timed event. Consequently, each one can have its own interrupt.
 *     This represents an overhead.
 * 
 * Author: J.M. Drake y D.Garc�a
 * Date: 1-1-09
 ********************************************************************/

package timers;

import org.w3c.dom.Node;


public class AlarmClock extends Timer {
	
	/* METHODS */

	// Constructor
	public AlarmClock(Node node) {
		super(node);
		
	}
	
	// Is invoked for the elements that require its sevice
	public void attend() {
		owner.timerRequire(this);
	}



	@Override
	public String toString(){
		return "\n\nAlarm clock:" + super.toString().replaceAll("\n", "\n\t");
	}
}

/*******************************************************************************
 * Grupo Computadores y Tiempo Real (CTR) UNIVERSIDAD DE CANTABRIA
 * 
 * @author C�sar Cuevas Cuesta
 * @version 1.0  Last review: 2009/September/25 
 * Class to manage files.
 ******************************************************************************/

package utilityClasses;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import exceptions.JSimMastException;

public class FilesManager {

	/* METHODS */

	/* General */

	public static boolean existFile(String targetPath) {
		
		/* THIS CHUNK OF CODE IS DIFFERENT IN THE PLUGIN VERSION ??????? */
		
		File targetFile = new File(targetPath);
		if (targetFile.exists())
			return true;
		else
			return false;
		/* ***************************************************************** */
	}
	
	/**
	 * This method creates and opens a file at the path specified by the
	 * argument. If the location does not exist, the appropriate folder
	 * structure is created.
	 * 
	 * @param targetPath
	 *            The path where the file is desired to be created.
	 * @throws JSimMastException
	 */
	public static boolean createFile(String targetPath) throws JSimMastException {

		/* THIS CHUNK OF CODE IS DIFFERENT IN THE PLUGIN VERSION */

		// I defensively create the target location before trying to create the
		// file.
		int n = targetPath.lastIndexOf("/");
		if (n == -1) {
			n = targetPath.lastIndexOf("\\");
		}
		String folderPath = targetPath.substring(0, n);
		File targetFolder = new File(folderPath);
		if (!targetFolder.exists()) {
			targetFolder.mkdirs();
		}

		File targetFile = new File(targetPath);
		try {
			if (!targetFile.exists()) {
				targetFile.createNewFile();
				return true;
			} else
				return false;
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems creating the target file "
					+ StringsHandler.getFileNameFromPath(targetPath));
		}

		/* ***************************************************************** */
	}

	/**
	 * This method copies the text file specified by the sourcePath argument at
	 * the path specified by the targetPath argument.
	 * 
	 * @param sourcePath
	 * @param targetPath
	 * @param move
	 *            A flag indicating if the original file has to be deleted.
	 * @throws JSimMastException
	 */
	public static void copyTextFile(String sourcePath, String targetPath, boolean move) throws JSimMastException {

		// I create the new file
		createFile(targetPath);

		// Extra code to avoid problems

		// I read the source and I fill the copy
		String content = readTextFile(sourcePath);
		fillTextFile(content, targetPath);

		if (move) {
			File oldFile = new File(sourcePath);
			oldFile.delete();
		}
	}

	/* ** Extra code to avoid problems ** /
	File inputFile = new File(inputFileName);
	File copyFile = new File(copyFilePath);

	// I take precautions about the input file
    if (!inputFile.exists()){
    	throw new IOException("No such source file: " + inputFileName);
    } else if (!inputFile.isFile()){
    	throw new IOException("Can't copy source directory: " + inputFileName);
    } else if (!inputFile.canRead()){
    	throw new IOException("Source file " + inputFileName + " is unreadable");
    }
    
    // I take precautions about the copy file
    if (copyFile.exists()) {
    	if (copyFile.isDirectory()){
    		copyFile = new File(copyFile, inputFile.getName());
    	}else if (!copyFile.canWrite()){
    		throw new IOException("Destination file " + copyFile.getName() + " is unwriteable");
    		System.out.print("Overwrite existing file " + copyFile.getName() + "? (Y/N): ");
    		System.out.flush();
    		InputStreamReader iSReader = new InputStreamReader(System.in);
    		BufferedReader bReader = new BufferedReader(iSReader);
    		String response = bReader.readLine();
		    if (!response.equals("Y") && !response.equals("y")){
		        throw new IOException("Existing file was not overwritten.");
		    } else {
			    String parent = copyFile.getParent();
			    if (parent == null){
			      parent = System.getProperty("user.dir");
			    }
			    File dir = new File(parent);
			    if (!dir.exists()){
			    	throw new IOException("Destination directory " + parent + " doesn't exist");
			    } else if (!dir.isDirectory()){
			    	throw new IOException("Destination " + parent + " is not a directory");
			    } else if (!dir.canWrite()){
			    	throw new IOException("Destination directory " + parent + " is unwriteable");
		    	}
		    }
		}

	    FileInputStream from = null;
	    FileOutputStream to = null;
	    try {
	      from = new FileInputStream(inputFile);
	      to = new FileOutputStream(copyFile);
	      byte[] buffer = new byte[4096];
	      int bytesRead;

	      while ((bytesRead = from.read(buffer)) != -1)
	        to.write(buffer, 0, bytesRead); // write
	    } catch(IOException e){
	    	
	    } finally {
	      if (from != null)
	        try {
	          from.close();
	        } catch (IOException e) {}
	      if (to != null)
	        try {
	          to.close();
	        } catch (IOException e) {}
	    }
  	}*/

	
	/* Text files */

	/**
	 * This method gets the content of a text file stored at the path specified
	 * by the argument.
	 * 
	 * @param filePath
	 *            The path where the text file is stored.
	 * @return The content of the read text file.
	 * @throws JSimMastException
	 */
	public static String readTextFile(String filePath) throws JSimMastException {

		String text = "";
		BufferedReader bReader = createBR(filePath);

		// I read the text file line by line
		try {
			String line = "";
			do {
				line = bReader.readLine();
				if (line == null)
					break;
				text += line + '\n';
			} while (line != null);
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems with the readable text file [["
					+ StringsHandler.getFileNameFromPath(filePath)
					+ "]] while accessing the BufferedReader");
		} finally {
			closeBR(bReader);
		}
		return text;
	}
	
	/**
	 * This method gets the content of a text file stored at the path specified
	 * by the argument.
	 * 
	 * @param filePath
	 *            The path where the text file is stored.
	 * @return The content of the read text file.
	 * @throws JSimMastException
	 */
	public static List<String> readTextFile2Array(String filePath) throws JSimMastException {

		List<String> lines = new LinkedList<String>();
		BufferedReader bReader = createBR(filePath);

		// I read the text file line by line
		try {
			String line = "";
			do {
				line = bReader.readLine();
				if (line == null)
					break;
				lines.add(line);
			} while (line != null);
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems with the readable text file [["
					+ StringsHandler.getFileNameFromPath(filePath)
					+ "]] while accessing the BufferedReader");
		} finally {
			closeBR(bReader);
		}
		return lines;
	}

	/**
	 * This method...
	 * 
	 * @param filePath
	 * @throws JSimMastException
	 */
	public static void scanTextFile(String filePath) throws JSimMastException {

		// I create the necessary Scanner to read the text file.
		Scanner scanner = createScanner(filePath);

		// I read the input file word by word
		while (scanner.hasNext()) {
		}

		// I close the Scanner
		if (scanner != null)
			scanner.close();

		/*
		 * // I create the necessary Scanner to read the text file. Scanner
		 * scanner;
		 * 
		 * try {
		 * 
		 * scanner = new Scanner(new FileReader(filePath));
		 * scanner.useLocale(Locale.ENGLISH); // I configure the nunber format
		 * 
		 * // I read the input file word by word while (scanner.hasNext()) {}
		 * 
		 * } catch (FileNotFoundException e) {
		 * 
		 * }finally { // I close the Scanner if (scanner != null)
		 * scanner.close(); }
		 */
	}

	/**
	 * This method fills a text file stored at the path specified by the 2nd
	 * argument with the 1st argument.
	 * 
	 * @param text
	 *            The text which will be written in the target text file.
	 * @param targetPath
	 *            The path where the target text file is stored.
	 * @throws JSimMastException
	 */
	public static void fillTextFile(String text, String targetPath)
			throws JSimMastException {

		// I create the necessary PrintWriter to write in the target file.
		PrintWriter pWriter = createPW(targetPath);

		// I write the text in the target file
		pWriter.print(text);

		// I close the PrintWriter
		closePW(pWriter);
	}

	/**
	 * This method...
	 * 
	 * @param filePath1
	 * @param filePath2
	 * @return
	 * @throws JSimMastException
	 */
	public static void compareTextFiles(String filePath1, String filePath2)
			throws JSimMastException {

		String content1 = StringsHandler.flatText(readTextFile(filePath1));
		String content2 = StringsHandler.flatText(readTextFile(filePath2));

		int l1 = content1.length();
		int l2 = content2.length();

		System.out.println("chars Laura = " + l1 + "\nchars Tools = " + l2);

		if ((l1 == l2) && (content1.equals(content2))) {
			System.out.println("coinciden");
		} else {
			System.out.println("NO coinciden");

			int min = Math.min(l1, l2);

			for (int i = 0; i < min; i++) {
				if (content1.charAt(i) != content2.charAt(i)) {
					System.out.println(content1.substring(i - 60, i));
					break;
				}
			}
		}
	}

	
	/* Streams for text files */

	/**
	 * This method creates a <code>BufferedReader</code> over the text file 
	 * stored at the path argument.
	 * 
	 * @param filePath
	 * @return
	 * @throws JSimMastException
	 */
	public static BufferedReader createBR(String filePath)
			throws JSimMastException {
		try {
			return new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the readable text file [["
							+ StringsHandler.getFileNameFromPath(filePath)
							+ "]] while creation of the FileReader for the BufferedReader (the file is not found)");
		}
	}
	
	/**
	 * This method creates a <code>BufferedReader</code> over the text file argument.
	 * 
	 * @param file
	 * @return
	 * @throws JSimMastException
	 */
	public static BufferedReader createBR(File file)
			throws JSimMastException {
		try {
			return new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the readable text file [[ ... ]] while creation of the FileReader for the BufferedReader (the file is not found)");
		}
	}

	/**
	 * This method creates a BufferedReader over the ...
	 * 
	 * @param fileIS
	 * @return
	 */
	public static BufferedReader createBR(InputStream fileIS) {
		return new BufferedReader(new InputStreamReader(fileIS));
	}

	/**
	 * This method closes the BufferedReader argument.
	 * 
	 * @param bReader
	 * @throws JSimMastException
	 */
	public static void closeBR(BufferedReader bReader) throws JSimMastException {
		try {
			if (bReader != null)
				bReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the readable text file while closing the BufferedReader");
		}
	}

	/**
	 * This method...
	 * 
	 * @param filePath
	 * @return
	 * @throws JSimMastException
	 */
	public static Scanner createScanner(String filePath) throws JSimMastException {
		Scanner scanner;
		try {
			scanner = new Scanner(new FileReader(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the readable text file [["
							+ StringsHandler.getFileNameFromPath(filePath)
							+ "]] while creation of the FileReader for the Scanner (the file is not found) ");
		}
		scanner.useLocale(Locale.ENGLISH); // I configure number format
		return scanner;
	}

	/**
	 * This method closes the Scanner argument.
	 * 
	 * @param scanner
	 */
	public static void closeScanner(Scanner scanner) {
		if (scanner != null)
			scanner.close();
	}

	/**
	 * This method creates a <code>PrintWriter</code> over the text file stored 
	 * at the path argument.
	 * 
	 * @param filePath
	 * @return
	 * @throws JSimMastException
	 */
	public static PrintWriter createPW(String filePath) throws JSimMastException {
		try {
			return new PrintWriter(new FileWriter(filePath));
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems with the writable text file [["
					+ StringsHandler.getFileNameFromPath(filePath)
					+ "]] while accessing the PrintWriter.");
		}
	}
	
	/**
	 * This method creates a <code>PrintWriter</code> over the text file 
	 * argument.
	 * 
	 * @param file
	 * @return
	 * @throws JSimMastException
	 */
	public static PrintWriter createPW(File file) throws JSimMastException {
		try {
			return new PrintWriter(new FileWriter(file));
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems with the writable text file [[...]] while accessing the PrintWriter.");
		}
	}
	
	/**
	 * This method creates a <code>PrintWriter</code> over the ...
	 * 
	 * @param fileOS
	 * @return
	 */
	public static PrintWriter createPW(OutputStream fileOS) throws JSimMastException {
		return new PrintWriter(new OutputStreamWriter(fileOS));
	}
	
	/**
	 * This method closes the PrintWriter argument.
	 * 
	 * @param pWriter
	 */
	public static void closePW(PrintWriter pWriter) {
		if (pWriter != null)
			pWriter.close();
	}

	
	/* Binary files */

	/**
	 * This method...
	 * 
	 * @param inputPath
	 * @return
	 */
	public static Object readBinfile(String filePath) throws JSimMastException {

		// I create the necessary ObjecInputStream to read from a bin file.
		ObjectInputStream oiStream = createOIS(filePath);

		// I read and return the contents of the input file
		try {
			return oiStream.readObject();
		} catch (IOException e) {
			throw new JSimMastException(
					"Problems accessing the InputStream from file "
							+ StringsHandler.getFileNameFromPath(filePath));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException("Problems reading the object from file "
					+ StringsHandler.getFileNameFromPath(filePath));
		} finally {
			// I close the ObjectOutputStream
			closeOIS(oiStream);
		}

		/*
		 * // I instantiate the couple of streams to read a bin file.
		 * ObjectInputStream oiStream = null; try { oiStream = new
		 * ObjectInputStream(new FileInputStream(inputPath)); return
		 * oiStream.readObject(); } catch (FileNotFoundException e) {
		 * System.out.println("The file has not been founded"); } catch
		 * (IOException e) {
		 * System.out.println("Problems accessing the InputStream"); } catch
		 * (ClassNotFoundException e) {
		 * System.out.println("Problems reading the object"); } finally { if
		 * (oiStream != null) { try { oiStream.close(); } catch (IOException e)
		 * { System.out.println("Problems closing the InputStream"); } } }
		 * return null;
		 */
	}

	/**
	 * This method...
	 * 
	 * @param targetPath
	 */
	public static void storeInBinFile(String targetPath, Object object)
			throws JSimMastException {

		// I create the necessary ObjectOutputStream to record in a bin file.
		ObjectOutputStream ooStream = createOOS(targetPath);

		// I record the Object in the target file
		try {
			ooStream.writeObject(object);
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems accessing the ObjectOutputStream to file "
							+ StringsHandler.getFileNameFromPath(targetPath));
		} finally {
			// I close the ObjectOutputStream
			closeOOS(ooStream);
		}

		/*
		 * // I instantiate the couple of streams to write in a bin file.
		 * ObjectOutputStream ooStream = null; try { ooStream = new
		 * ObjectOutputStream(new FileOutputStream(targetPath));
		 * ooStream.writeObject(object); } catch (FileNotFoundException e1) {
		 * System.out.println("The file couldn't be created."); } catch
		 * (IOException e1) {
		 * System.out.println("Problems accessing the Output Stream"); } finally
		 * { if (ooStream != null) { try { ooStream.close(); } catch
		 * (IOException e) {
		 * System.out.println("Problems closing the Output Stream"); } } }
		 */
	}

	
	/* Streams for bin files */

	/**
	 * This method ...
	 * 
	 * @param path
	 * @return
	 * @throws JSimMastException
	 */
	public static ObjectInputStream createOIS(String filePath)
			throws JSimMastException {
		try {
			return new ObjectInputStream(new FileInputStream(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the bin file [["
							+ StringsHandler.getFileNameFromPath(filePath)
							+ "]] while creation of the FileInputStream for the ObjectInputstream (the file is not found)");
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems accessing the ObjectInputStream from file "
							+ StringsHandler.getFileNameFromPath(filePath));
		}
	}

	/**
	 * This method closes the ObjectInputStream argument.
	 * 
	 * @param oiStream
	 * @throws JSimMastException
	 */
	public static void closeOIS(ObjectInputStream oiStream)
			throws JSimMastException {
		if (oiStream != null)
			try {
				oiStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new JSimMastException(
						"Problems closing the ObjectInputStream");
			}
	}

	/**
	 * This method...
	 * 
	 * @param targetPath
	 * @return
	 * @throws JSimMastException
	 */
	public static ObjectOutputStream createOOS(String targetPath)
			throws JSimMastException {
		try {
			return new ObjectOutputStream(new FileOutputStream(targetPath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems with the bin file [["
							+ StringsHandler.getFileNameFromPath(targetPath)
							+ "]] while creation of the FileOutputStream for the ObjectOutputStream (the file could not be created)");
		} catch (IOException e) {
			e.printStackTrace();
			throw new JSimMastException(
					"Problems accessing the ObjectOutputStream to file "
							+ StringsHandler.getFileNameFromPath(targetPath));
		}
	}

	/**
	 * This method closes the ObjectOutputStream argument.
	 * 
	 * @param ooStream
	 * @throws JSimMastException
	 */
	public static void closeOOS(ObjectOutputStream ooStream)
			throws JSimMastException {
		if (ooStream != null)
			try {
				ooStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new JSimMastException(
						"Problems closing the ObjectOutputStream");
			}
	}

	
	/* */
	
	/**
	 * This method reads a Java file a returns the content between the braces.
	 * 
	 * @param javaFilePath
	 * @return
	 * @throws JSimMastException
	 */
	public static String loadJavaFile(String javaFilePath)
			throws JSimMastException {

		// I read the java file
		String content = readTextFile(javaFilePath);
		content = StringsHandler.flatText(content);

		// I return the content between the braces
		int n1 = content.indexOf("{");
		int n2 = content.lastIndexOf("}");
		return content.substring(n1 + 1, n2);
	}

	/** 
	 * This method accepts the name of a resource in the file system and the 
	 * name of the folder where it is stored, folder which is supposed to be 
	 * within a JAR file, and returns an <code>InputStream</code> from it. 
	 * 
	 * @param resourceName 
	 * 		The name of the resource.
	 * @param folderName 
	 * 		The name of the folder where the resource is stored + '/'.
	 * @param jarPath 
	 * 		The path of the JAR file which contains the resources folder.
	 * @return An <code>InputStream</code> representing the resource.
	 */
	public static InputStream getResourceIS (String resourceName, String folderName, String jarPath){
		
		//InputStream in = ClassLoader.getSystemResourceAsStream(filename);
		//Note : assumes that myfile.txt is in root directory of jar file
	    //InputStream is = getClass().getResourceAsStream ("/myfile.txt");
		
		InputStream is = null;
		
		try {
			JarFile jar = new JarFile(jarPath);					
			JarEntry entry = jar.getJarEntry(folderName + resourceName);
			is = jar.getInputStream(entry);						
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return is;
	}

}

/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Proporciona recursos para la generaci�n de n�meros 
 *       aleatorios
 *
 * Autors: Patricia L�pez Martinez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1-1-09
 ********************************************************************/
package module;

public class RandomGenerator {

	/**
	 * Retorna un valor aleatorio double entre 0.0 y 1.0
	 * @return
	 */
	public static double normalizedUniformValue() {
		return Math.random();
	}

	/**
	 * Genera un n�mero aleatorio double en el rango min .. max
	 * @param min
	 * @param max
	 * @return
	 */
	public static double uniformValue(double min, double max){
		return min + normalizedUniformValue()*(max - min);
	}
	
	/**
	 * Retorna un valor aleatorio double con una distribuci�n de tipo
	 * exponencial de valor medio "mean".
	 * @param mean
	 * @return
	 */ 
	public static double exponentialValue(double mean){
		return -mean * Math.log(normalizedUniformValue() + 0.5E-9);
	}
	
	/**
	 * Retorna valores con una distribuci�n hiperexponencial 
	 * cuyos coeficiente y pesos son establecidos en la lista 
	 * que se pasa con el par�metro "coefList".
	 * @param coefList
	 * @return
	 */
	public static double hyperExponentialValue(HyperExpCoefList coefList){
	    // Evalua un valor aleatorio entre 0.0 y 1.0
		double v = normalizedUniformValue();
		double coef = 0;
		Coef hec;
		int ord = coefList.order();
		for (int n = 0; n < ord; n++){
			hec = coefList.get(n);
			if ((v > hec.limInf) &&(v < hec.limSup)){
				coef = hec.value;
				break;
			}
		}
		return exponentialValue(1.0/coef);
	}
	
	/**
	 * Retorna valores con una distribuci�n discreta cuyos valores y 
	 * pesos son establecidos en la lista que se pasa con el par�metro "valueList".
	 * @param valueList
	 * @return
	 */
	public static double discreteValue(DiscreteValueList valueList){
	    // Evalua un valor aleatorio entre 0.0 y 1.0
		double v = normalizedUniformValue();
		Coef datum;
		int ord = valueList.order();
		double returnedValue=0;
		for (int n = 0; n < ord; n++){
			datum = valueList.get(n);
			if ((v > datum.limInf) &&(v < datum.limSup)){
				returnedValue = datum.value;
				break;
			}
		}
		return returnedValue;
	}	
	
	/**
	 * Retorna un tiem
	 * @param param
	 * @return
	 */
	public static long interarrivalTime(DistributionParameter param){
		if (param.getClass() == UniformDistributionParameter.class)
			return (long)uniformValue(0,2.0*(double)(((UniformDistributionParameter)param).mean));
	    else if (param.getClass() == ExponentialDistributionParameter.class)
	    	return (long)exponentialValue((double)((ExponentialDistributionParameter)param).mean);
	    else if(param.getClass() == HyperExponentialDistributionParameter.class)
	    	return (long)hyperExponentialValue(((HyperExponentialDistributionParameter)param).data);
	    else
	    	return (long)discreteValue(((DiscreteDistributionParameter)param).data);
	}
}

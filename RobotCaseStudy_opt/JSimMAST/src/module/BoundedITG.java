/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera tiempos de evento siguiendo un patr�n 
 *    acotado aperi�dico
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/

package module;

public class BoundedITG extends AperiodicITG {
	
	/* FIELDS*/
	
    // M�nimo tiempo entre eventos
	long minTime;
	
	// Temporal variable
	long itg;
	
	
	/* METHODS */
	
	// Constructor
	public BoundedITG(DistributionParameter param, long minTime, long firstTime){
		super(param, firstTime);
		this.minTime = minTime;
	}
	
	public long next(){
		if (firstIsGenerated){
			firstIsGenerated=true;
			return firstTime;
		}else{
			while ((itg=RandomGenerator.interarrivalTime(data))<minTime){};
			return itg;
		}
	}
	
	
	
	@Override
	public String toString(){
		String string = "\nMin time = " + minTime;
		return "\n\tPattern: bounded" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

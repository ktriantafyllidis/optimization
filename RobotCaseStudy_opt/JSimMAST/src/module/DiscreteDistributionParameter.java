/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Parámetros de una distribución discreta (Todos los
 *   parámetros de la distribución se expresan en nanosegundos) 
 *
 * Autors: Patricia López Martínez [lopezpa@unican.es]
 *         José M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public class DiscreteDistributionParameter extends DistributionParameter {
	 public DiscreteValueList data;
}

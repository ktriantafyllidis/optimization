/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera un �nico evento en el offsetTime
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public class SingularITG extends ITG {
	
	/* METHODS */
	
	// Constructor
	public SingularITG(long firstTime){
		super(firstTime);
	}

	public long next() {
		if (firstIsGenerated){
			firstIsGenerated = true;
			return firstTime;
		} else
			return Long.MAX_VALUE;
	}
	
	
	
	@Override
	public String toString(){
		return "\n\tPattern: singular" + super.toString().replaceAll("\n", "\n\t");
	}
}

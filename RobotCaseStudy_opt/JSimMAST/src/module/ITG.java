/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Clase abstracta raiz de las clases generadores de 
 *   tiempos de incicio con intervalor entre eventos especificado
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1-1-09
 ********************************************************************/
package module;

public abstract class ITG {

	// Tiempo de generaci�n del primer tiempo
	protected long firstTime;
	
	// False si aun no se ha generado el primer evento
	boolean firstIsGenerated = false;
	
	
	
	// Constructors
	public ITG() {
		firstTime = 0;
	}
	public ITG(long firstTime){
		this.firstTime = firstTime;
	}
	
	/**
	 * @return El pr�ximo tiempo de inicio
	 */
	public abstract long next();


	
	@Override
	public String toString(){
		return "\nFirst time = " + firstTime;
	}
}

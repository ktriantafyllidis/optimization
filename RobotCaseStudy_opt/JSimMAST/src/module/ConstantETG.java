/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Genera un tiempo de ejecuci�n constante
 *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/

package module;

public class ConstantETG extends ETG {

	/* FIELDS */
	
	// Tiempo de ejecuci�n.
	long et = 1;
	
	
	/* METHODS */
	
	// Constructor
	public ConstantETG(long et){
		if (et != 0)
			this.et = et;
	}

	public long next(){
		return et;		
	}
	public void denormalize(double factor){
		et = (long)((double)et/factor);
	}
	
	@Override
	public ETG clone() {
		return new ConstantETG(et);
	}
	
	@Override
	public String toString(){
		return "\nExecution time = " + et;
	}


}

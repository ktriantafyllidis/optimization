/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: clase abstracta raiz de los generadores de tiempo de
 *   ejecuci�n
 *  *
 * Autors: Patricia L�pez Mart�nez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1/1/09
 ********************************************************************/
package module;

public abstract class ETG {
	
	// Retorna el proximo tiempo de ejecuci�n.
	public abstract long next();
	
	public abstract void denormalize(double factor);
	
	public abstract ETG clone();
}

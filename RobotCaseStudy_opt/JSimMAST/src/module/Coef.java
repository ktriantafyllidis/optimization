/*********************************************************************
 *                        Simulador JSimMast
 *                      Compatible con MAST 3.0
 * Computadores y Tiempo Real group. Universidad de Cantabria (Spain)
 *
 * Description: Coeficiente utilizado en la generaci�n de n�meros 
 *      aleatorios basados en listas de valores: Hyperexponential y 
 *      Discrete.
 *
 * Autors: Patricia L�pez Martinez [lopezpa@unican.es]
 *         Jos� M. Drake Moyano [drakej@unican.es]
 * Version: 1-1-09
 ********************************************************************/
package module;

public class Coef {
 public double weight;
 public double value;
 public double limInf;
 public double limSup;
 
 public Coef(double w,double v,double li,double ls){
	 weight=w;
	 value=v;
	 limInf=li;
	 limSup=ls;
 }
}

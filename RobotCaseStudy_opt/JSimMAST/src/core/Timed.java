/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: Timed
 * Description: Interface offered for the Element wich requires a timed signal.
 *      The clock invokes the update() method when the specific time is reached
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;
public interface Timed {
  // Is executed when the time deadline is reched.
  void update();
}

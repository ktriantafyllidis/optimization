/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Repository
 * Description: Repository of the element. Is used to build of the
 *      simulation modelo, and to generate the results. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package core;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.io.*;
import resource.*;
import timers.TimingObject;
import utilityClasses.FilesManager;
import utilityClasses.XMLparser;
import eventmonitor.Statistical_EM;
import exceptions.JSimMastException;
import flow.*;
import operation.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class Repository {
	
	/* ENUMS */
	
	private enum ModelElemType {Regular_Processor, 
								Packet_Based_Network_VCA_Simulator, //gadem enabled
		                           Packet_Based_Network, 
		                           //RTEP_Network, 
		                           //AFDX_Link, 
		                           Regular_Switch,
		                           // AFDX_Switch
		                           Regular_Router, 
		                           Primary_Scheduler, 
		                           //Secondary_Scheduler, 
		                           Thread, 
		                           Communication_Channel, 
		                           //Virtual_Schedulable_Resource, 
		                           //Virtual_Communication_Channel, 
		                           Immediate_Ceiling_Mutex, 
		                           Priority_Inheritance_Mutex, 
		                           //SRP_Mutex, 
		                           Simple_Operation, 
		                           Message, 
		                           Composite_Operation, 
		                           Enclosing_Operation, 
		                           Composite_Message, //gadem enabled
		                           Regular_End_To_End_Flow, 
		                           Clock_Synchronization_Object, 
		                           Ticker, 
		                           Alarm_Clock}
		
	
	/* ATTRIBUTES */
	
	// List of elements. It is organized by type of elements.
	private static Map<String, Map<String, ModelElement>> modelElemMap;
		public static ModelElement getModelElem(String name, String type){
			return modelElemMap.get(type).get(name);
		}
		public static void addModelElem(ModelElement modelElem, String type, String name){
			modelElemMap.get(type).put(name, modelElem);
		}
		public static Map<String, ModelElement> getSubMap(String key){
			for (Entry<String, Map<String, ModelElement>> entry : modelElemMap.entrySet()) {
				if (entry.getKey().equals(key)) {
					return entry.getValue();
				}
			}
			return null;
		}
		
	private static String modelName;
		public static String modelName(){return modelName;}

	private static String modelDate;
		public static String modelDate(){return modelDate;}

	private static String checkingText;
	
	private static List<Statistical_EM> timeMonitorList;
		public static Iterator<Statistical_EM> timeMonitorListIter(){return timeMonitorList.iterator();}
		public static void addTimeMonitor(Statistical_EM timeMonitor){timeMonitorList.add(timeMonitor);}
	

	/* METHODS */
	
	/**
	 * This method parses the model file, 
	 * loads the elements in the model, 
	 * builds the simulation model (completed and denormalized) and 
	 * prints the checking file.
	 * 
	 * @param pathModel
	 * @throws AbortedException
	 */
	public static void init(String pathModel)throws AbortedException{
		
		timeMonitorList = new ArrayList<Statistical_EM>();
		
		// I prepare the map to store the different model elements 
		modelElemMap = new HashMap<String, Map<String, ModelElement>>();
			modelElemMap.put("ProcessingResource", new HashMap<String, ModelElement>());
			modelElemMap.put("Scheduler", new HashMap<String, ModelElement>());
			modelElemMap.put("SchedulableResource", new HashMap<String, ModelElement>());
			modelElemMap.put("MutualExclusionResource", new HashMap<String,ModelElement>());
			modelElemMap.put("Operation", new HashMap<String, ModelElement>());
			modelElemMap.put("EndToEndFlow", new HashMap<String, ModelElement>());
			modelElemMap.put("Timer", new HashMap<String, ModelElement>());
	  
		// Reads and parses the model file
		DocumentBuilderFactory theFactory = DocumentBuilderFactory.newInstance();
		theFactory.setNamespaceAware(true);
		DocumentBuilder theBuilder = null;
		Document modelDocument = null;
		try{
			theBuilder = theFactory.newDocumentBuilder();
			modelDocument = theBuilder.parse(new File(pathModel));
		} catch(ParserConfigurationException e){
			throw new AbortedException("The document builder has not been built");
		}catch (SAXException e){
			throw new AbortedException("The document has not been parsered");
		}catch (IOException e){
			System.out.println("The model file has not been found");
		}
		Element documentElement = modelDocument.getDocumentElement();
      
		if (documentElement.hasAttribute("Model_Name"))
			modelName = documentElement.getAttribute("Model_Name");
		if (documentElement.hasAttribute("Model_Date"))
			modelDate = documentElement.getAttribute("Model_Date");
	  
		
		/* I track the root's children to build the elements of the simulation model, 
		 * storing them in the map */
		
		List<Element> rootChildren = XMLparser.getChildElem(documentElement);
		ModelElement newElem;
		ModelElemType type;
		for (Element rootChild : rootChildren) {
			type = ModelElemType.valueOf(rootChild.getLocalName());
			switch (type) {
			// Processing resources
			case Regular_Processor:
			case Packet_Based_Network_VCA_Simulator:
			case Packet_Based_Network:
//			case RTEP_Network:
//			case AFDX_Link:
			case Regular_Switch:
//			case AFDX_Switch:
			case Regular_Router:
				newElem = ProcessingResource.newProcessingResource(rootChild);
				modelElemMap.get("ProcessingResource").put(newElem.name(), newElem);
				break;
		 	// Schedulers
			case Primary_Scheduler:
//			case Secondary_Scheduler:
				newElem = Scheduler.newScheduler(rootChild);
				modelElemMap.get("Scheduler").put(newElem.name(), newElem);
				break;
			// Schedulable resources
			case Thread:
			case Communication_Channel:
//			case Virtual_Schedulable_Resource:
//		  	case Virtual_Communication_Channel:
		  		newElem = SimThread.newSchedulableResource(rootChild);
		  		modelElemMap.get("SchedulableResource").put(newElem.name(), newElem);
			  	break;
			// Mutual exclusion resources
		  	case Immediate_Ceiling_Mutex:
		  	case Priority_Inheritance_Mutex:
//		  	case SRP_Mutex:
		  		newElem = MutualExclusionResource.newMutualExclusionResource(rootChild);
			  	modelElemMap.get("MutualExclusionResource").put(newElem.name(), newElem);
			  	break;
			// Operations
		  	case Simple_Operation:
		  	case Message:
		  	case Composite_Operation:
		  	case Enclosing_Operation:
		  	case Composite_Message:
		  		newElem = Operation.newOperation(rootChild);
		  		modelElemMap.get("Operation").put(newElem.name(), newElem);
		  		break;				  
		  	// Timers
		  	case Clock_Synchronization_Object:
		  	case Ticker:
		  	case Alarm_Clock:
		  		newElem = TimingObject.newTimer(rootChild);
		  		modelElemMap.get("Timer").put(newElem.name(), newElem);
		  		break;
		  	default: // case Regular_End_To_End_Flow:
		  		newElem = new RegularEndToEndFlow(rootChild);
		  	  	modelElemMap.get("EndToEndFlow").put(newElem.name(), newElem);
		  	  	break;
			}
		}
	  
		
		/* I complete the model elements in a well defined order */
		
		// Operations
		for (ModelElement oper : modelElemMap.get("Operation").values()) {
			oper.complete();
		}
		
		// Schedulers
		Iterator<ModelElement> iter = modelElemMap.get("Scheduler").values().iterator();
		while(iter.hasNext()){
			iter.next().complete();
		}
  
		// Servers
		for (ModelElement server : modelElemMap.get("SchedulableResource").values()) {
			server.complete();
		}
  
		// Processing resources
		for (ModelElement procResource : modelElemMap.get("ProcessingResource").values()) {
			procResource.complete();
		}
  
		// Timers
		for (ModelElement timer : modelElemMap.get("Timer").values()) {
			timer.complete();
		}
		
		// Transactions
		for (ModelElement trans : modelElemMap.get("EndToEndFlow").values()) {
			trans.complete();
		}
		
		// Mutexes
		for (ModelElement mutex : modelElemMap.get("MutualExclusionResource").values()) {
			mutex.complete();
		}
  
		
		/* I denormalize the model elements in a well defined order */
		
		// Schedulers
		for (ModelElement scheduler : modelElemMap.get("Scheduler").values()) {
			scheduler.denormalize();
		}
  
		// Servers
		for (ModelElement server : modelElemMap.get("SchedulableResource").values()) {
			server.denormalize();
		}	  
  
		// Transactions
		for (ModelElement trans : modelElemMap.get("EndToEndFlow").values()) {
			trans.denormalize();
		}

  		
  		/* I build the checking file */
		
  		printCheckingFile();
	}
  
	/**
	 * Genera un string con los resultados de la simulaci�n.
	 * @return
	 */
	public static String result(){
		
		String profile = Double.toString(Clock.timeSeconds(Clock.currentTime()));
		switch (Controller.profile()) {
     	case VERBOSE: 
     		profile = profile + " VERBOSE"; 
     		break;
     	case SCHEDULABILITY: 
     		profile = profile + " SCHEDULABILITY";
     		break;
     	case TRACES: 
     		profile = profile + " TRACES"; 
     		break;
     	case PERFORMANCE: 
     		profile = profile + " PERFORMANCE"; 
     		break;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String generationDate = df.format(new Date(System.currentTimeMillis()));
//		String generationDate=new Date().toString();
		
		// I write the header of the *.res.xml file (the XML declaration and the MAST declaration)
		String res = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
		             "<?mast fileType=\"XML-Result-File\" version=\"1.0\" ?>\n\n";
		
		// I write the root element with its attributes
		res += "<mast_res:REAL_TIME_SITUATION\n" + 
	           "\txmlns:mast_res=\"http://mast.unican.es/xmlmast/result\"\n" + 
	           "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
	           "\txsi:schemaLocation=\"http://mast.unican.es/xmlmast/result ../JSimMast_V_Schemas/Mast2_Result_V.xsd\"\n" + 
	           "\tName=\"" + modelName + "\"\n" +
	           "\tModel_Date=\"" + modelDate + "\"\n" +
	           "\tGeneration_Tool=\"SimMAST_2\"\n" + 
	           "\tGeneration_Profile=\"" + profile + "\"\n" + 
	           "\tGeneration_Date=\"" + generationDate + "\">\n\n";
		
		// I write the transactions results (a <mast_res:End_To_End_Flow_Results> for each trans.)
	  	for (ModelElement modelElem : getSubMap("EndToEndFlow").values()) {
			res += ((RegularEndToEndFlow)modelElem).results("\t");
		}
	  	
	  	/*
	  	 * I write the processing resources results
	  	 * 		<mast_res:Computing_Resource_Results>
	  	 *   	<mast_res:Network_Results>
	  	 */
	  	for (ModelElement modelElem : getSubMap("ProcessingResource").values()) {
	  		res += ((ProcessingResource)modelElem).results("\t");
		}
	  	
	  	/*
	  	 *  I write the mutexes results
	  	 *  	<mast_res:Priority_Inheritance_Mutex_Results>
	  	 *  	<mast_res:Immediate_Ceiling_Mutex_Results>
	  	 *  	<mast_res:SRP_Mutex_Results>
	  	 */
	  	for (ModelElement modelElem : getSubMap("MutualExclusionResource").values()) {
	  		res += ((MutualExclusionResource)modelElem).results("\t");
		}
		
	  	return res + "\n</mast_res:REAL_TIME_SITUATION>";
	}

	
	
	private static void printCheckingFile(){
		
		// I compose the content of the checking file
		for (ModelElement modelElem : modelElemMap.get("ProcessingResource").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("Scheduler").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("SchedulableResource").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("MutualExclusionResource").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("Timer").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("Operation").values()) {
			checkingText += modelElem.toString();
		}
		for (ModelElement modelElem : modelElemMap.get("EndToEndFlow").values()) {
			checkingText += modelElem.toString();
		}
		
		try {
			// I create and open, fill and close the checking file
			String checkingFilePath = "./outputFiles/" + modelName + ".dbg.txt";
			FilesManager.createFile(checkingFilePath);
		  	FilesManager.fillTextFile(checkingText, checkingFilePath);
		} catch (JSimMastException e) {
			e.printStackTrace();
		}
	}
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Interface: ResultMonitor
 * Description: Interface offered for the event and state monitors which
 * stores results.
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;
public interface ResultMonitor {
	
  // Return the formated stored results	
  String result();
}
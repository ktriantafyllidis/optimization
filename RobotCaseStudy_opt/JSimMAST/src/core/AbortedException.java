/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: AbortedException
 * Description: It is thrown if the model building is aborted. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package core;

public class AbortedException extends Exception {
	public AbortedException(String mssg){
		super(mssg);
	}
}

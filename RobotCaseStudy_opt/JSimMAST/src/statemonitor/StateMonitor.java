/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: StateMonitor
 * Description: Monitor the chage of the resource state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/
package statemonitor;

import resource.*;

public abstract class StateMonitor {
	
	/* FIELDS */

	// Resource for which this state monitor is created
	protected Resource hostRsrc;
    
    	
    /* METHODS */
    
   	// Constructor 
    protected StateMonitor(Resource resource) {
    	hostRsrc = resource;
    }
    
	/**
	 * This method is meant to be executed each time that the state of 
	 * the monitored resource changes, performing an appropriate action.
	 * @param newState
	 */
	public abstract void state(int newState);
	
	
}
/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: DisplayChangeStateMonitor
 * Description: Monitor the chage of the resource state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import core.*;
import resource.*;
import module.*;

public class DisplayChangeStateMonitor extends StateMonitor {

	// Constructor
	public DisplayChangeStateMonitor(Resource resource){
		super(resource);
	}
	
	public void state(int newState){
		// <Clock time>:<resource>/<state>
		Controller.mssgOutput(Tools.delimit(Clock.currentTime())+":" + 
			                  hostRsrc.name() + "/" + 
			                  hostRsrc.stateName(newState));
	}
	
}

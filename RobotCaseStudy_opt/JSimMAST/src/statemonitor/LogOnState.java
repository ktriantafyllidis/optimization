/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: DisplayChangeStateMonitor
 * Description: Log the change of the resource state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import core.*;
import resource.*;

public class LogOnState extends Logging_SM {
	
	/* FIELDS */
	
	private int monitoredState;
	
	
	/* METHODS */
	
	// Constructor
	public LogOnState(Resource resource, int state, int mID){
		super(resource, mID);
		monitoredState = state;
	}
	
	public void state(int newState){
		if (newState == monitoredState) {
			Logger.registerNewTrace(hostRsrc.sID(), mID);
		}
	}
}

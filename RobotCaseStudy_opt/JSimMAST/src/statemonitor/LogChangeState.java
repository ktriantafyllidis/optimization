/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: DisplayChangeStateMonitor
 * Description: Log the change of the resource state. 
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package statemonitor;

import core.*;
import resource.*;

public class LogChangeState extends Logging_SM {
	
	
	/* METHODS */
	
	// Constructor
	public LogChangeState(Resource resource, int mID){
		super(resource, mID);
	}
	
	public void state(int newState){
		Logger.registerNewTrace(hostRsrc.sID(), mID);
	}
}

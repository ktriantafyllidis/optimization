package preProcessor;

import exceptions.JSimMastException;
import guis.JSimMastGUI;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import module.Tools;

import org.w3c.dom.Element;

import utilityClasses.XMLparser;

public class MASTpreProcessor {

	/* ENUMS */
	
	private enum ModelElementType {Regular_Processor, 
									Packet_Based_Network_VCA_Simulator,
							        Packet_Based_Network, 
							        RTEP_Network, 
							        AFDX_Link, 
							        Regular_Switch,
							        Regular_Router, 
							        Primary_Scheduler, 
							        Secondary_Scheduler, 
							        Thread, 
							        Communication_Channel, 
							        Virtual_Schedulable_Resource, 
							        Virtual_Communication_Channel, 
							        Immediate_Ceiling_Mutex, 
							        Priority_Inheritance_Mutex, 
							        SRP_Mutex, 
							        Simple_Operation, 
							        Message, 
							        Composite_Operation, 
							        Enclosing_Operation, 
							        Enclosing_Message,
							        Regular_End_To_End_Flow, 
							        Clock_Synchronization_Object, 
							        Ticker, 
							        Alarm_Clock}

	private enum E2eFlowChildType {Periodic_Event, 
								 Sporadic_Event, 
								 Unbounded_Event,
							     Bursty_Event,
							     Singular_Event, 
							     Internal_Event, 
							     Step, 
							     Merge, 
							     Join, 
							     Branch, 
							     Query_Server, 
							     Fork, 
							     Rate_Divisor, 
							     Delay, 
							     Offset, 
							     Message_Fork, 
							     Message_Delivery, 
							     Message_Branch}

	private enum RegularEventChildType{Max_Output_Jitter_Req, 
										Hard_Global_Deadline, 
										Soft_Global_Deadline, 
										Global_Max_Miss_Ratio, 
										Hard_Local_Deadline, 
										Soft_Local_Deadline, 
										Local_Max_Miss_Ratio, 
										Queue_Size_Req, 
										Composite_Observer}
	

	/* FIELDS */
	
	private static JSimMastGUI gui;
	private static Map<String, List<String>> referenciableMap;
	private static List<String> incidenceList;
	
	
	/* METHODS */

	public static void init(JSimMastGUI gui){
		MASTpreProcessor.gui = gui;
		incidenceList = new ArrayList<String>();
		referenciableMap = new TreeMap<String, List<String>>();
			referenciableMap.put("Synchronization_Timer", null);
			referenciableMap.put("Simple_Operation", null);
			referenciableMap.put("Schedulable_Resource", null);
			referenciableMap.put("Processing_Resource", null);
			referenciableMap.put("Scheduler", null);
			referenciableMap.put("Mutual_Exclusion_Resource", null);
			referenciableMap.put("Operation", null);
			referenciableMap.put("System_Timer", null);
			referenciableMap.put("Workload_Event", null);
			referenciableMap.put("Event", null);
			referenciableMap.put("Network_Switch", null);
	}
		
	public static void performProcess(String inputFileLocation) throws JSimMastException {
				
		/*
		 * precisamente hacia esto para no parsear mas de una vez un mismo fichero, pero
		 * en el caso de la gui del simulador, quiero que si he arreglado inconsistencias
		 * en un fichero, que lo vuelva a parsear
		 * 
		 * // I split the inputFileLocation argument into repository location and input file locator. 
		int n = inputFileLocation.indexOf(REPOSITORY_NAME) + REPOSITORY_NAME.length() + 1;
		String repositoryLocation = inputFileLocation.substring(0, n);
		String inputFileLocator = inputFileLocation.substring(n);
		
		// I set the repository location
		Repository.getRepository().setRepositoryLocation(repositoryLocation);*/
		
		// I get the root of the XML input file and its children
		Element rootElem = XMLparser.parseXMLfile(inputFileLocation).getDocumentElement(); // rootElem = Repository.getRepository().getElemByLocator(inputFileLocator, "");
		List<Element> rootChildren = XMLparser.getChildElem(rootElem);
		
		// 1st round
		for (Element rootChild : rootChildren) {
    		String rootChildName = rootChild.getAttribute("Name");
    		ModelElementType modelElementType = ModelElementType.valueOf(rootChild.getLocalName());
		  	switch (modelElementType) {
		  	// Processing resources
		  	case Regular_Processor:		
		  	case Packet_Based_Network_VCA_Simulator:
		  	case Packet_Based_Network:			  		
		  	case RTEP_Network:			  		
		  	case AFDX_Link:
		  		if (referenciableMap.get("Processing_Resource") == null)
		  			referenciableMap.put("Processing_Resource", new LinkedList<String>());
		  		referenciableMap.get("Processing_Resource").add(rootChildName);
		  		break;
		  	case Regular_Switch:
		  	case Regular_Router:
		  		if (referenciableMap.get("Processing_Resource") == null)
		  			referenciableMap.put("Processing_Resource", new LinkedList<String>());
		  		referenciableMap.get("Processing_Resource").add(rootChildName);
		  		if (referenciableMap.get("Network_Switch") == null)
		  			referenciableMap.put("Network_Switch", new LinkedList<String>());
		  		referenciableMap.get("Network_Switch").add(rootChildName);
		  		break;
		  	// Schedulers
		  	case Primary_Scheduler:
		  	case Secondary_Scheduler:
		  		if (referenciableMap.get("Scheduler") == null)
		  			referenciableMap.put("Scheduler", new LinkedList<String>());
		  		referenciableMap.get("Scheduler").add(rootChildName);
		  		break;
		  	// Schedulable resources
		  	case Thread:
		  	case Communication_Channel:
		  	case Virtual_Schedulable_Resource:
		  	case Virtual_Communication_Channel:
		  		if (referenciableMap.get("Schedulable_Resource") == null)
		  			referenciableMap.put("Schedulable_Resource", new LinkedList<String>());
		  		referenciableMap.get("Schedulable_Resource").add(rootChildName);
		  		break;
		  	// Mutual exclusion resources
		  	case Immediate_Ceiling_Mutex:
		  	case Priority_Inheritance_Mutex:
		  	case SRP_Mutex:
		  		if (referenciableMap.get("Mutual_Exclusion_Resource") == null)
		  			referenciableMap.put("Mutual_Exclusion_Resource", new LinkedList<String>());
		  		referenciableMap.get("Mutual_Exclusion_Resource").add(rootChildName);
		  		break;
		  	// Operations
		  	case Simple_Operation:
		  		if (referenciableMap.get("Simple_Operation") == null)
		  			referenciableMap.put("Simple_Operation", new LinkedList<String>());
		  		referenciableMap.get("Simple_Operation").add(rootChildName);
		  		if (referenciableMap.get("Operation") == null)
		  			referenciableMap.put("Operation", new LinkedList<String>());
		  		referenciableMap.get("Operation").add(rootChildName);
		  		break;
		  	case Message:
		  	case Composite_Operation:
		  	case Enclosing_Operation:
		  		if (referenciableMap.get("Operation") == null)
		  			referenciableMap.put("Operation", new LinkedList<String>());
		  		referenciableMap.get("Operation").add(rootChildName);
		  		break;
		  	// End to end flows
		  	case Regular_End_To_End_Flow:
		  		List<Element> e2eFlowChildren = XMLparser.getChildElem(rootChild);
		  		for (Element e2eFlowChild : e2eFlowChildren) {
					String e2eFlowChildName = e2eFlowChild.getAttribute("Name");
					E2eFlowChildType e2eFlowChildType = E2eFlowChildType.valueOf(e2eFlowChild.getLocalName());
					switch (e2eFlowChildType) {
					case Periodic_Event:
					case Sporadic_Event:
					case Unbounded_Event:
					case Bursty_Event:
					case Singular_Event:
						if (referenciableMap.get("Workload_Event") == null)
				  			referenciableMap.put("Workload_Event", new LinkedList<String>());
				  		referenciableMap.get("Workload_Event").add(rootChildName + "." + e2eFlowChildName);
				  		if (referenciableMap.get("Event") == null)
				  			referenciableMap.put("Event", new LinkedList<String>());
				  		referenciableMap.get("Event").add(rootChildName + "." + e2eFlowChildName);
						break;
					case Internal_Event:
						if (referenciableMap.get("Event") == null)
				  			referenciableMap.put("Event", new LinkedList<String>());
				  		referenciableMap.get("Event").add(rootChildName + "." + e2eFlowChildName);
						break;
					default: // Step, Merge, Join, Branch, Query_Server, Fork, Rate_Divisor, Delay, Offset, Message_Fork, Message_Delivery, Message_Branch
						break;
					}
				}
		  		break;
		  	// Timers
		  	case Clock_Synchronization_Object:
		  		if (referenciableMap.get("Synchronization_Timer") == null)
		  			referenciableMap.put("Synchronization_Timer", new LinkedList<String>());
		  		referenciableMap.get("Synchronization_Timer").add(rootChildName);
		  		break;
		  	case Ticker:
		  	case Alarm_Clock:
		  		if (referenciableMap.get("System_Timer") == null)
		  			referenciableMap.put("System_Timer", new LinkedList<String>());
		  		referenciableMap.get("System_Timer").add(rootChildName);
		  		break;
		  	default:
		  		break;
		  	}	    	
	    }
	    
		// 2nd round
		for (Element rootChild : rootChildren) { 
    		String rootChildName = rootChild.getAttribute("Name");
    		ModelElementType modelElementType = ModelElementType.valueOf(rootChild.getLocalName());
		  	switch (modelElementType) {
		  	case Regular_Processor:	  	
		  	case Regular_Switch:
		  	case Regular_Router:
		  		// I check if it references a Synchronization_Source and, if so, if it is present in the model
		  		if (rootChild.hasAttribute("Synchronization_Source")) {
					String syncSourceRef = rootChild.getAttribute("Synchronization_Source");
					if (referenciableMap.get("Synchronization_Timer") == null || !referenciableMap.get("Synchronization_Timer").contains(syncSourceRef))
						processIncidence("\nSynchronization source \"" + syncSourceRef + "\" for processing resource \"" + rootChildName + "\" is not found");
				}
		  		break;
		  	case Packet_Based_Network:
		  	case Packet_Based_Network_VCA_Simulator:
		  		// I check if it references a Synchronization_Source and, if so, if it is present in the model
		  		if (rootChild.hasAttribute("Synchronization_Source")) {
					String syncSourceRef = rootChild.getAttribute("Synchronization_Source");
					if (referenciableMap.get("Synchronization_Timer") == null || !referenciableMap.get("Synchronization_Timer").contains(syncSourceRef))
						processIncidence("\nSynchronization source \"" + syncSourceRef + "\" for packet based network \"" + rootChildName + "\" is not found");
		  		}
		  		// Checking the possible drivers...
		  		Vector<Element> pbNetChildren = XMLparser.getChildElem(rootChild);
		  		for (Element driverElem : pbNetChildren) {									
					String driverLocalName = driverElem.getLocalName();
					if(driverLocalName.equals("Character_Packet_Driver")) {
						// I check if the Character_Send_Operation is referenced and if so, if it is present in the model
						if (driverElem.hasAttribute("Character_Send_Operation")) {
							String charSendOperRef = driverElem.getAttribute("Character_Send_Operation");
							if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(charSendOperRef))
								processIncidence("\nCharacter send operation \"" + charSendOperRef + "\" for character packet driver \"XX\" is not found");
						}
						// I check if the Character_Receive_Operation is referenced and if so, if it is present in the model
						if (driverElem.hasAttribute("Character_Receive_Operation")) {
							String charReceiveOperRef = driverElem.getAttribute("Character_Receive_Operation");
							if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(charReceiveOperRef))
								processIncidence("\nCharacter receive operation \"" + charReceiveOperRef + "\" for character packet driver \"XX\" is not found");
						}
						// I check if the Character_Server is referenced and if so, if it is present in the model
						if (driverElem.hasAttribute("Character_Server")) {
							String charServerRef = driverElem.getAttribute("Packet_Server");
							if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(charServerRef))
								processIncidence("\nCharacter server \"" + charServerRef + "\" for character packet driver \"XX\" is not found");
						}
					}
					// I check if the Packet_Send_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Send_Operation")) {
						String packetSendOperRef = driverElem.getAttribute("Packet_Send_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetSendOperRef))
							processIncidence("\nPacket send operation \"" + packetSendOperRef + "\" for packet driver \"XX\" is not found");
					}
					// I check if the Packet_Receive_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Receive_Operation")) {
						String packetReceiveOperRef = driverElem.getAttribute("Packet_Receive_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetReceiveOperRef))
							processIncidence("\nPacket receive operation \"" + packetReceiveOperRef + "\" for packet driver \"XX\" is not found");
					}
					// I check if the Packet_Server is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Server")) {
						String packetServerRef = driverElem.getAttribute("Packet_Server");
						if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(packetServerRef))
							processIncidence("\nPacket server \"" + packetServerRef + "\" for packet driver \"XX\" is not found");
					}
				}				
		  		break;
		  	case RTEP_Network:
		  		// I check if it references a Synchronization_Source and, if so, if it is present in the model
		  		if (rootChild.hasAttribute("Synchronization_Source")) {
					String syncSourceRef = rootChild.getAttribute("Synchronization_Source");
					if (referenciableMap.get("Synchronization_Timer") == null || !referenciableMap.get("Synchronization_Timer").contains(syncSourceRef))
						processIncidence("\nSynchronization source \"" + syncSourceRef + "\" for RTEP network \"" + rootChildName + "\" is not found");
				}
		  		// Checking the possible drivers...
		  		Vector<Element> rtepNetChildren = XMLparser.getChildElem(rootChild);
		  		for (Element driverElem : rtepNetChildren) { // RTEP_Packet_Driver												
					// I check if the Packet_Send_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Send_Operation")) {
						String packetSendOperRef = driverElem.getAttribute("Packet_Send_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetSendOperRef))
							processIncidence("\nPacket send operation \"" + packetSendOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_Receive_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Receive_Operation")) {
						String packetReceiveOperRef = driverElem.getAttribute("Packet_Receive_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetReceiveOperRef))
							processIncidence("\nPacket receive operation \"" + packetReceiveOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_Server is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Server")) {
						String packetServerRef = driverElem.getAttribute("Packet_Server");
						if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(packetServerRef))
							processIncidence("\nPacket server \"" + packetServerRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_Interrupt_Server is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Interrupt_Server")) {
						String packetInterruptServerRef = driverElem.getAttribute("Packet_Interrupt_Server");
						if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(packetInterruptServerRef))
							processIncidence("\nPacket interrupt server \"" + packetInterruptServerRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_ISR_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_ISR_Operation")) {
						String packetIsrOperRef = driverElem.getAttribute("Packet_ISR_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetIsrOperRef))
							processIncidence("\nPacket ISR operation \"" + packetIsrOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Token_Check_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Token_Check_Operation")) {
						String tokenCheckOperRef = driverElem.getAttribute("Token_Check_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(tokenCheckOperRef))
							processIncidence("\nToken check operation \"" + tokenCheckOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Token_Manage_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Token_Manage_Operation")) {
						String tokenManageOperRef = driverElem.getAttribute("Token_Manage_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(tokenManageOperRef))
							processIncidence("\nToken manage operation \"" + tokenManageOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_Discard_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Discard_Operation")) {
						String packetDiscardOperRef = driverElem.getAttribute("Packet_Discard_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetDiscardOperRef))
							processIncidence("\nPacket discard operation \"" + packetDiscardOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Token_Retransmission_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Token_Retransmission_Operation")) {
						String tokenRetransmissionOperRef = driverElem.getAttribute("Token_Retransmission_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(tokenRetransmissionOperRef))
							processIncidence("\nToken retransmission operation \"" + tokenRetransmissionOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
					// I check if the Packet_Retransmission_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Retransmission_Operation")) {
						String packetRetransmissionOperRef = driverElem.getAttribute("Packet_Retransmission_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetRetransmissionOperRef))
							processIncidence("\nPacket retransmission operation \"" + packetRetransmissionOperRef + "\" for RTEP packet driver \"XX\" is not found");
					}
				}
				break;
		  	case AFDX_Link:
		  		// I check if it references a Synchronization_Source and, if so, if it is present in the model
		  		if (rootChild.hasAttribute("Synchronization_Source")) {
					String syncSourceRef = rootChild.getAttribute("Synchronization_Source");
					if (referenciableMap.get("Synchronization_Timer") == null || !referenciableMap.get("Synchronization_Timer").contains(syncSourceRef))
						processIncidence("\nSynchronization source \"" + syncSourceRef + "\" for AFDX link \"" + rootChildName + "\" is not found");
				}
		  		Vector<Element> afdxLinkChildren = XMLparser.getChildElem(rootChild);
		  		for (Element driverElem : afdxLinkChildren) { // Packet_Driver
					// I check if the Packet_Send_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Send_Operation")) {
						String packetSendOperRef = driverElem.getAttribute("Packet_Send_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetSendOperRef))
							processIncidence("\nPacket send operation \"" + packetSendOperRef + "\" for packet driver \"XX\" is not found");
					}
					// I check if the Packet_Receive_Operation is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Receive_Operation")) {
						String packetReceiveOperRef = driverElem.getAttribute("Packet_Receive_Operation");
						if (referenciableMap.get("Simple_Operation") == null || !referenciableMap.get("Simple_Operation").contains(packetReceiveOperRef))
							processIncidence("\nPacket receive operation \"" + packetReceiveOperRef + "\" for packet driver \"XX\" is not found");
					}
					// I check if the Packet_Server is referenced and if so, if it is present in the model
					if (driverElem.hasAttribute("Packet_Server")) {
						String packetServerRef = driverElem.getAttribute("Packet_Server");
						if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(packetServerRef)) {
							processIncidence("\nPacket server \"" + packetServerRef + "\" for packet driver \"XX\" is not found");
						}
					}
				}
				break;
		  	case Primary_Scheduler:
		  		// I check if its referenced host is present in the model
		  		String primarySchedulerHostRef = rootChild.getAttribute("Host");
		  		if (referenciableMap.get("Processing_Resource") == null || !referenciableMap.get("Processing_Resource").contains(primarySchedulerHostRef))
		  			processIncidence("\nHost \"" + primarySchedulerHostRef + "\" for primary scheduler \"" + rootChildName + "\" is not found");
		  		break;
		  	case Secondary_Scheduler:
		  		// I check if its referenced host is present in the model
		  		String secondarySchedulerHostRef = rootChild.getAttribute("Host");
		  		if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(secondarySchedulerHostRef))
		  			processIncidence("\nHost \"" + secondarySchedulerHostRef + "\" for secondary scheduler \"" + rootChildName + "\" is not found");
		  		break;
		  	case Thread:
		  	case Communication_Channel:
		  	case Virtual_Schedulable_Resource:			  	
		  	case Virtual_Communication_Channel:
		  		// I check if its referenced scheduler is present in the model
		  		String schedulerRef = rootChild.getAttribute("Scheduler");
				if (referenciableMap.get("Scheduler") == null || !referenciableMap.get("Scheduler").contains(schedulerRef))
					processIncidence("\nScheduler \"" + schedulerRef + "\" for schedulable resource \"" + rootChildName + "\" is not found");			
		  		break;			  	
		  	case Simple_Operation:
		  		// I check if it references any mutexes and, if so, if they are present in the model
		  		List<String> simpleOperMutexRefList = null;
		  		Vector<Element> simpleOperChildren = XMLparser.getChildElem(rootChild);
		  		for (Element simpleOperChild : simpleOperChildren) {
					String simpleOperChildLocalName = simpleOperChild.getLocalName();
					if(simpleOperChildLocalName.equals("Mutex_List")) {
						simpleOperMutexRefList = Tools.parses(simpleOperChild.getTextContent());								
						break;
					} else if(simpleOperChildLocalName.equals("Mutex_To_Lock_List") || simpleOperChildLocalName.equals("Mutex_To_Unlock_List"))
						simpleOperMutexRefList = Tools.parses(simpleOperChild.getTextContent());
				}
				if (simpleOperMutexRefList != null) {
					for (String mutexRef : simpleOperMutexRefList) {
						if (referenciableMap.get("Mutual_Exclusion_Resource") == null || !referenciableMap.get("Mutual_Exclusion_Resource").contains(mutexRef))
							processIncidence("\nMutex \"" + mutexRef + "\" for simple operation \"" + rootChildName + "\" is not found");							
					}
				}
		  		break;			  
		  	case Composite_Operation:
		  	case Enclosing_Operation:
		  		// I check if it references any mutexes and, if so, if they are present in the model
		  		List<String> compositeOperMutexRefList = null;
		  		List<String> compositeOperRefList = null;
		  		
		  		Vector<Element> compositeOperChildren = XMLparser.getChildElem(rootChild);
		  		for (Element compositeOperChild : compositeOperChildren) {
					String compositeOperChildLocalName = compositeOperChild.getLocalName();
					if(compositeOperChildLocalName.equals("Mutex_List"))
						compositeOperMutexRefList = Tools.parses(compositeOperChild.getTextContent());																
					else if(compositeOperChildLocalName.equals("Mutex_To_Lock_List") || compositeOperChildLocalName.equals("Mutex_To_Unlock_List"))
						compositeOperMutexRefList = Tools.parses(compositeOperChild.getTextContent());
					else if(compositeOperChildLocalName.equals("Operation_List"))
						compositeOperRefList = Tools.parses(compositeOperChild.getTextContent());
				}
				if (compositeOperMutexRefList != null) {
					for (String mutexRef : compositeOperMutexRefList) {
						if (referenciableMap.get("Mutual_Exclusion_Resource") == null || !referenciableMap.get("Mutual_Exclusion_Resource").contains(mutexRef))
							processIncidence("\nMutex \"" + mutexRef + "\" for composite operation \"" + rootChildName + "\" is not found");							
					}
				}
				// I also check if its referenced operations are present in the model
				if (compositeOperRefList != null) {
					for (String operRef : compositeOperRefList) {
						if (referenciableMap.get("Operation") == null || !referenciableMap.get("Operation").contains(operRef))
							processIncidence("\nOperation \"" + operRef + "\" for composite operation \"" + rootChildName + "\" is not found");							
					}
				}
		  		break;
		  	case Regular_End_To_End_Flow:
		  		Vector<Element> e2eFlowChildren = XMLparser.getChildElem(rootChild);
		  		for (Element e2eFlowChild : e2eFlowChildren) {
					String e2eFlowChildName = "XX";
					if (e2eFlowChild.hasAttribute("Name"))
						e2eFlowChildName = e2eFlowChild.getAttribute("Name");
					E2eFlowChildType e2eFlowChildType = E2eFlowChildType.valueOf(e2eFlowChild.getLocalName());
					switch (e2eFlowChildType) {
					case Periodic_Event:
					case Singular_Event:
						// I check if it references a System_Timer and, if so, if it is present in the model
				  		if (e2eFlowChild.hasAttribute("Timer")) {
							String timerRef = e2eFlowChild.getAttribute("Timer");
							if (referenciableMap.get("System_Timer") == null || !referenciableMap.get("System_Timer").contains(timerRef))
								processIncidence("\nSystem timer \"" + timerRef + "\" for workload event \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");									
						}
						break;							
					case Internal_Event:
						Vector<Element> regEventChildren = XMLparser.getChildElem(e2eFlowChild);
						for (Element regEventChild : regEventChildren) {
							String regEventChildLocalName = regEventChild.getLocalName();
							String regEventChildName = "XX";
							RegularEventChildType regEventChildType = RegularEventChildType.valueOf(regEventChildLocalName);
							switch (regEventChildType) {
							case Max_Output_Jitter_Req:
							case Hard_Global_Deadline:
							case Soft_Global_Deadline:
							case Global_Max_Miss_Ratio:
								// I check if its referenced workload event is present in the model
								String refEvent = regEventChild.getAttribute("Referenced_Event");
								if (referenciableMap.get("Workload_Event") == null || !referenciableMap.get("Workload_Event").contains(rootChildName + "." + refEvent))
									processIncidence("\nWorkload event \"" + refEvent + "\" for observer \"" + rootChildName + "." + e2eFlowChildName + "." + regEventChildName + "\" is not found");											
								break;										
							default: // Hard_Local_Deadline, Soft_Local_Deadline, Local_Max_Miss_Ratio, Max_Queue_Size_Req, Composite_Observer
								break;
							}
						}
						break;
					case Step:
						// I check if its referenced i/o events are present in the model
						String stepInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + stepInputEventRef))
							processIncidence("\nInput event \"" + stepInputEventRef + "\" for step \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						String stepOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (!referenciableMap.get("Event").contains(rootChildName + "." + stepOutputEventRef))
							processIncidence("\nOutput event \"" + stepOutputEventRef + "\" for step \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");								

						// I check if its referenced Step_Operation is present in the model							
						String stepOperRef = e2eFlowChild.getAttribute("Step_Operation");
						if (referenciableMap.get("Operation") == null || !referenciableMap.get("Operation").contains(stepOperRef))
							processIncidence("\nOperation \"" + stepOperRef + "\" for step \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						// I check if its referenced Step_Operation is present in the model							
						String stepSchedulableResourceRef = e2eFlowChild.getAttribute("Step_Schedulable_Resource");
						if (referenciableMap.get("Schedulable_Resource") == null || !referenciableMap.get("Schedulable_Resource").contains(stepSchedulableResourceRef))
							processIncidence("\nSchedulable resource \"" + stepSchedulableResourceRef + "\" for step \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");								
						break;
					case Merge:
					case Join:
						// I check if its referenced i/o events are present in the model
						String joinOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + joinOutputEventRef))
							processIncidence("\nOutput event \"" + joinOutputEventRef + "\" for flow control event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						List<String> joinInputEventRefList = Tools.parses(e2eFlowChild.getAttribute("Input_Events_List"));
						for (String joinInputEventRef : joinInputEventRefList) {
							if (!referenciableMap.get("Event").contains(rootChildName + "." + joinInputEventRef))
								processIncidence("\nInput event \"" + joinInputEventRef + "\" for flow control event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");																		
						}								
						break;
					case Branch:
					case Query_Server:
					case Fork:
						// I check if its referenced i/o events are present in the model
						String forkInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + forkInputEventRef))
							processIncidence("\nInput event \"" + forkInputEventRef + "\" for flow control event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						List<String> forkOutputEventRefList = Tools.parses(e2eFlowChild.getAttribute("Output_Events_List"));
						for (String forkOutputEventRef : forkOutputEventRefList) {
							if (!referenciableMap.get("Event").contains(rootChildName + "." + forkOutputEventRef))
								processIncidence("\nOutput event \"" + forkOutputEventRef + "\" for flow control event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");									
						}								
						break;
					case Rate_Divisor:
						// I check if its referenced i/o events are present in the model
						String rateDivisorInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + rateDivisorInputEventRef))
							processIncidence("\nInput event \"" + rateDivisorInputEventRef + "\" for rate divisor \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");								
						
						String rateDivisorOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (!referenciableMap.get("Event").contains(rootChildName + "." + rateDivisorOutputEventRef))
							processIncidence("\nOutput event \"" + rateDivisorOutputEventRef + "\" for rate divisor \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						break;
					case Delay:
						// I check if it references a System_Timer and, if so, if it is present in the model
				  		if (e2eFlowChild.hasAttribute("Timer")) {
							String timerRef = e2eFlowChild.getAttribute("Timer");
							if (referenciableMap.get("System_Timer") == null || !referenciableMap.get("System_Timer").contains(timerRef))
								processIncidence("\nSystem timer \"" + timerRef + "\" for delay \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");									
						}
				  		// I check if its referenced i/o events are present in the model
						String delayInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + delayInputEventRef))
							processIncidence("\nInput event \"" + delayInputEventRef + "\" for delay \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						String delayOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (!referenciableMap.get("Event").contains(rootChildName + "." + delayOutputEventRef))
							processIncidence("\nOutput event \"" + delayOutputEventRef + "\" for delay \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						break;
					case Offset:
						// I check if it references a System_Timer and, if so, if it is present in the model
				  		if (e2eFlowChild.hasAttribute("Timer")) {
							String timerRef = e2eFlowChild.getAttribute("Timer");
							if (referenciableMap.get("System_Timer") == null || !referenciableMap.get("System_Timer").contains(timerRef))
								processIncidence("\nSystem timer \"" + timerRef + "\" for offset \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");									
						}
				  		// I check if its referenced i/o events are present in the model
						String offsetInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + offsetInputEventRef))
							processIncidence("\nInput event \"" + offsetInputEventRef + "\" for offset \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						String offsetOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (!referenciableMap.get("Event").contains(rootChildName + "." + offsetOutputEventRef))
							processIncidence("\nOutput event \"" + offsetOutputEventRef + "\" for offset \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
				  		// I check if its referenced workload event is present in the model
				  		String refEvent = e2eFlowChild.getAttribute("Referenced_Event");
				  		if (referenciableMap.get("Workload_Event") == null || !referenciableMap.get("Workload_Event").contains(rootChildName + "." + refEvent))
				  			processIncidence("\nWorkload event \"" + refEvent + "\" for offset \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						break;
					case Message_Fork:
					case Message_Branch:
						// I check if its referenced i/o events are present in the model
						String mforkInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + mforkInputEventRef))
							processIncidence("\nInput event \"" + mforkInputEventRef + "\" for message event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						List<String> mforkOutputEventRefList = Tools.parses(e2eFlowChild.getAttribute("Output_Events_List"));
						for (String mforkOutputEventRef : mforkOutputEventRefList) {
							if (!referenciableMap.get("Event").contains(rootChildName + "." + mforkOutputEventRef))
								processIncidence("\nOutput event \"" + mforkOutputEventRef + "\" for message event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");									
						}							
						// I check if its referenced network switch is present in the model
						String switchRef = e2eFlowChild.getAttribute("Switch");
						if (referenciableMap.get("Network_Switch") == null || !referenciableMap.get("Network_Switch").contains(switchRef))
							processIncidence("\nNetwork switch \"" + switchRef + "\" for message event handler \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						break;
					case Message_Delivery:
						// I check if its referenced i/o events are present in the model
						String messageDeliveryInputEventRef = e2eFlowChild.getAttribute("Input_Event");
						if (referenciableMap.get("Event") == null || !referenciableMap.get("Event").contains(rootChildName + "." + messageDeliveryInputEventRef))
							processIncidence("\nInput event \"" + messageDeliveryInputEventRef + "\" for message delivery \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						String messageDeliveryOutputEventRef = e2eFlowChild.getAttribute("Output_Event");
						if (!referenciableMap.get("Event").contains(rootChildName + "." + messageDeliveryOutputEventRef))
							processIncidence("\nOutput event \"" + messageDeliveryOutputEventRef + "\" for message delivery \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
						// I check if its referenced network switch is present in the model
						String messageDeliverySwitchRef = e2eFlowChild.getAttribute("Switch");
						if (referenciableMap.get("Network_Switch") == null || !referenciableMap.get("Network_Switch").contains(messageDeliverySwitchRef))
							processIncidence("\nNetwork switch \"" + messageDeliverySwitchRef + "\" for message delivery \"" + rootChildName + "." + e2eFlowChildName + "\" is not found");
						
					default: // Unbounded_Event, Bursty_Event, Sporadic_Event
						break;
					}
				}
		  		break;
		  	default: // Synchronization_Timer, Ticker, Alarm_Clock
		  		break;
		  	}
    	}
		
	}
	
	private static void processIncidence(String mssg){
		//gui.addMessage(mssg);
		incidenceList.add(mssg);
	}
	
	public static int numFoundIncidences(){return incidenceList.size();}
	
	public static void report(){
		for (String incidence : incidenceList) {
			gui.addMessage(incidence);
		}
	}
	
	/*private void printCode() throws CesarException {
		FilesManager.fillTextFile(outputString, "./outputFiles/" + outputFileName + "_errorLog.txt");
	}*/
}

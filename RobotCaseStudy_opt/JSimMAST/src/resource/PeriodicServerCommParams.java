/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PeriodicServerParams
 * Description: Represents a task scheduled under the sporadic server
 *       scheduling algorithm.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/
package resource;

import module.Tools;

import org.w3c.dom.*;

public class PeriodicServerCommParams extends PriorityBasedParams {

	/* FIELDS */
	
	// Its the initial value of the execution capacity.
	protected long initialCapacity;
		public long initialCapacity(){return initialCapacity;} //JMD
		public void setInitialCapacity(int initialCapacity){this.initialCapacity = initialCapacity;}
  
	// It is the period after which a portion of consumed execution capacity is replenished.
	protected long replenishmentPeriod = 0;
		public long replenishmentPeriod() {return replenishmentPeriod;}
		public void setReplenishmentPeriod(long replenishmentPeriod) {this.replenishmentPeriod = replenishmentPeriod;}
  
	// It is the current priority of the periodic server. It changes in accordance with its funtionality
	protected int currentPriority;
		public int currentPriority(){return currentPriority;}
		public void setCurrentPriority(int priority){currentPriority = priority;}
  
	// It is the current capacity of the periodic server. It changes in accordance with its activity
	protected long currentCapacity = 0;
		public long currentCapacity() {return currentCapacity;}
		public void setCurrentCapacity(long capacity) {currentCapacity = capacity;}
  

	/* METHODS */

	// Constructor
	public PeriodicServerCommParams(Node node) {
		
		super(node);
		
		Element elem = (Element)node;
	  		
	  	if (elem.hasAttribute("Initial_Capacity"))
	  		initialCapacity = Tools.readLong(elem.getAttribute("Initial_Capacity"));
	  	
	  	if (elem.hasAttribute("Replenishment_Period"))
	  		replenishmentPeriod = Tools.readLong(elem.getAttribute("Replenishment_Period"));
	  	
	  	currentPriority = priority;
	  	currentCapacity = initialCapacity;
	}
	
	
	
	@Override
	public String toString(){
	
		String string = "\nInitial capacity = " + initialCapacity + 
		                "\nReplenishment period = " + replenishmentPeriod;
	  	
		return "\nPeriodic server comm params:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}


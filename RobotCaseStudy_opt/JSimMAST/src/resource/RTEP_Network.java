package resource;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class RTEP_Network extends PacketBasedNetwork {
	
	/* FIELDS */
	
	private int numStations;
	private long tokenDelay;
	private long failureTimeout;
	private int tokenTransmissionRetries;
	private int packetTransmissionRetries;
	private int exploringTokenSize;
	private int transmitTokenSize;
	
	
	/* METHODS */
	
	// Constructor
	public RTEP_Network(Node node){
		
		super(node);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Number_Of_Stations")) 
			numStations = Integer.parseInt(elem.getAttribute("Number_Of_Stations"));
		if (elem.hasAttribute("Token_Delay")) 
			tokenDelay = Tools.readLong(elem.getAttribute("Token_Delay"));
		if (elem.hasAttribute("Failure_Timeout"))
			failureTimeout = Tools.readLong(elem.getAttribute("Failure_Timeout"));
		if (elem.hasAttribute("Token_Transmission_Retries"))
			tokenTransmissionRetries = Integer.parseInt(elem.getAttribute("Token_Transmission_Retries"));
		if (elem.hasAttribute("Packet_Transmission_Retries"))
			packetTransmissionRetries = Integer.parseInt(elem.getAttribute("Packet_Transmission_Retries"));
		if (elem.hasAttribute("Arbitration_Token_Size"))
			exploringTokenSize = Integer.parseInt(elem.getAttribute("Arbitration_Token_Size"));
		if (elem.hasAttribute("Transmitt_Token_Size"))
			transmitTokenSize = Integer.parseInt(elem.getAttribute("Transmitt_Token_Size"));
	}



	@Override
	public String toString(){

		String string = "\nNumber of stations = " + numStations + 
		                "\nToken delay = " + tokenDelay + 
		                "\nFailure timeout = " + failureTimeout + 
		                "\nToken transmission retries = " + tokenTransmissionRetries + 
		                "\nPacket transmission retries = " + packetTransmissionRetries + 
		                "\nExploring token size = " + exploringTokenSize + 
		                "\nTransmitt token size = " + transmitTokenSize;
		
		return "\n\nRTEP network:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: mmediateCeilingResource
 * Description: Uses the Stack Resource Protocol (SRP). This is similar
 *      to the immediate ceiling protocol but works for non-priority-based
 *      policies.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.*;

import org.w3c.dom.*;

public class SRP_Mutex extends MutualExclusionResource {

	/* FIELDS */
	
	// Priority ceiling used for the resource. May be computed automatically by the tool, upon request.
	private int preemptionLevel;
	
	
	/* METHODS */
	
	// Constructor
	public SRP_Mutex(Node node){
		super(node);
		waitForLockList = new PriorityQueue<SimThread>(5, new SRPComparator());
		preemptionLevel = Integer.parseInt(((Element)node).getAttribute("Preemption_Level"));
	}

	@Override
	public String results(String offset) {
		String string = "";
		String sup = super.results(offset);
		if (!sup.equals(""))
			string = offset + "<mast_res:SRP_Mutex_Results Name=\"" + computingResourceName + "/" + name + "\">\n" + 
		    sup + 
		    offset + "</mast_res:SRP_Mutex_Results>\n\n";
		return string;
	}
	
	public void decide() {
		
		// I get the 1st server from the awaiting queue, removing it
		SimThread server = waitForLockList.remove();
		
		setOwner(server);
		setCurrentState(ASSIGNED);
		server.assignResource();
		
		// I update the current flow level of the server
		server.executingSeg().currentEvent().setFlowLevel(preemptionLevel);
		
		// I block the servers waiting to lock the mutex
		Iterator<SimThread> iter = waitForLockList.iterator();
		while(iter.hasNext()){
			iter.next().block();
		}

	}

	
	
	@Override
	public String toString(){
		String string = "\nPreemption level = " + preemptionLevel;
		return "\n\nSRP mutex:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
	
	
	
	
	
	/***** SRPResource *****/
	private class SRPComparator implements Comparator<SimThread>{
		public int compare(SimThread server1,SimThread server2){
			if ((server1.executingSeg()==null)|(server1.executingSeg().currentEvent()==null)) return -1;
			if ((server2.executingSeg()==null)|(server2.executingSeg().currentEvent()==null)) return +1;
				return (server1.executingSeg().currentEvent().flowLevel()-server2.executingSeg().currentEvent().flowLevel());
			}
			public boolean equals(Object obj){return false;}
		}
	}

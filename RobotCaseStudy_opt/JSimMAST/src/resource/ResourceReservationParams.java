package resource;

import org.w3c.dom.Node;

public abstract class ResourceReservationParams extends SchedulingParams {
		
	/* ENUM */
	private enum ResourceReservationParamsType {Virtual_Periodic_Server, 
		                                        Virtual_Deferrable_Server,
		                                        Virtual_Sporadic_Server,
		                                        Virtual_Periodic_Comm_Channel,
		                                        Virtual_Deferrable_Comm_Channel,
		                                        Virtual_Sporadic_Comm_Channel,
		                                        Virtual_Token_Bucket_Comm_Channel}
	
	
	/* METHODS */
	
	// Constructor
	public ResourceReservationParams(Node node){}
	
	public static ResourceReservationParams newResourceReservationParams(Node node){
		ResourceReservationParamsType type = ResourceReservationParamsType.valueOf(node.getLocalName());
		switch (type) {
		case Virtual_Periodic_Server:
			return new VirtualPeriodicServer(node);
		case Virtual_Deferrable_Server:
			return new VirtualDeferrableServer(node);
		case Virtual_Sporadic_Server:
			return new VirtualSporadicServer(node);
		case Virtual_Periodic_Comm_Channel:
			return new VirtualPeriodicCommChannel(node);
		case Virtual_Deferrable_Comm_Channel:
			return new VirtualDeferrableCommChannel(node);
		case Virtual_Sporadic_Comm_Channel:
			return new VirtualSporadicCommChannel(node);
		case Virtual_Token_Bucket_Comm_Channel:
			return new VirtualTokenBucketCommChannelParams(node);
		default:
			return null;
		}
	}
	
	public void denormalize(double factor) {}

}

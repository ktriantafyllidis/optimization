/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: SRPData
 * Description: Pramater for SRP protocol
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import org.w3c.dom.*;

public class SRP_Params  {

    /* ATTRIBUTES */
	
    private int preemptionLevel;
    	public int preemptionLevel(){return preemptionLevel;}
    	public void setPreemptionLevel(int preemptionLevel){this.preemptionLevel = preemptionLevel;}
    
 
    /* METHODS */
    
    // Constructor
    public SRP_Params(Node node){
    	Element elem = (Element)node;
    	preemptionLevel = Integer.parseInt(elem.getAttribute("Preemption_Level"));
    }
    
}

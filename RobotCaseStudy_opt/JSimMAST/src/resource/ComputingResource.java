/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: ComputingResource
 * Description: Abstract element root of all class of processor. 
 *      A processor is a processing resource that execute code.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import org.w3c.dom.Node;

import core.Controller;
import core.Logger;
import core.Controller.ProfileType;

public abstract class ComputingResource extends ProcessingResource {

	/* METHODS */
	
	// Constructor
	protected ComputingResource(Node node){
		
		super(node);
		
		// I register a source in the Logger
		if (Controller.profile() == ProfileType.TRACES)
			sID = Logger.registerNewSource(name, Logger.SrcType.ComputingResource);
	}
	
	public void denormalize() {}	
}
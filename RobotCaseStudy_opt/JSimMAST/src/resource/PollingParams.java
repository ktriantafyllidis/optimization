/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PollingParams
 * Description: Represents the scheduling mechanism by which there is
 *      a periodic task that polls for the arrival of its input event.
 *      Thus, execution of the event may be delayed until the next
 *      period.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import module.*;
import org.w3c.dom.*;

public class PollingParams extends PriorityBasedParams {

	
	/* FIELDS */
	
	// Period of the polling task.
	private long period;
		public long period() {return period;}
		public void setPeriod(long period) {this.period = period;}
  
	// Overhead of the polling task.
	private ETG overhead;
		public ETG overhead(){return overhead;}
		public void setPollingOverhead(ETG pollingOverhead) {this.overhead = pollingOverhead;}
  

	/* METHODS */

	// Constructor  
	public PollingParams(Node node) {
		
		super(node);
		
		Element elem = (Element)node;
		
	  	if (elem.hasAttribute("Polling_Period"))
	  		period = Tools.readLong(elem.getAttribute("Polling_Period"));
	  	
	  	long worst = 0;
	  	if (elem.hasAttribute("Polling_Worst_Overhead"))
	  		worst = Tools.readLong(elem.getAttribute("Polling_Worst_Overhead"));
	  	
	  	long avg = 0;
	  	if (elem.hasAttribute("Polling_Avg_Overhead"))
	  		avg = Tools.readLong(elem.getAttribute("Polling_Avg_Overhead"));
	  	
	  	long best = 0;
	  	if (elem.hasAttribute("Polling_Best_Overhead"))
	  		best = Tools.readLong(elem.getAttribute("Polling_Best_Overhead"));
	  	
		overhead = new WABETG(worst, avg, best);
	}
  
	@Override
  	public void denormalize(double factor){
  		overhead.denormalize(factor);
  	}
	
	public int currentPriority() {
    	return priority;
    }


	@Override
	public String toString(){
		
		String string = "\nPeriod = " + period + 
		                "\nOverhead: " + overhead.toString();
	  	
		return "\nPolling params:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

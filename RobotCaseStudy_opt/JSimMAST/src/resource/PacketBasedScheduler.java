/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PriorityBasedScheduler
 * Description: It represents a scheduler that is able to handle the
 * 		 processing capacity of a proccesing suplier: Processing 
 *       Resource (PrimaryScheduler) or Scheduling Server 
 *       (SecondaryScheduler). It represents a fixed-priority policy.
 *      
 * 
 * Author: J.M. Drake
 * Date: 14-04-2011
 ********************************************************************/

package resource;

import java.util.PriorityQueue;
import org.w3c.dom.*;

import module.*;
import core.*;

public class PacketBasedScheduler extends Scheduler {

	/* METHODS */
	
	// Constructor
	public PacketBasedScheduler(Element policyElem){ 
		
		super(policyElem.getParentNode());
		
		setAwaitingServers(new PriorityQueue<SimThread>(5));

		long max = 0;
		if (policyElem.hasAttribute("Packet_Overhead_Max_Size"))
			max = Tools.readLong(policyElem.getAttribute("Packet_Overhead_Max_Size"));
			
		long avg = 0;
		if (policyElem.hasAttribute("Packet_Overhead_Avg_Size"))
			avg = Tools.readLong(policyElem.getAttribute("Packet_Overhead_Avg_Size"));
			
		long min = 0;
		if (policyElem.hasAttribute("Packet_Overhead_Min_Size"))
			min = Tools.readLong(policyElem.getAttribute("Packet_Overhead_Min_Size"));
		
		if (max != 0)
			contextSwitch = new WABETG(max, avg, min);
	    else 
	    	contextSwitch = null;
		
		// not used policyNode's attrs
		/*
		<xs:attribute name="Max_Priority" type="mast_mdl:Priority" use="optional"/>
		<xs:attribute name="Min_Priority" type="mast_mdl:Priority" use="optional"/>
	    */
	}
		
	@Override
	public void denormalize(){
//		if (contextSwitch != null)
//			contextSwitch.denormalize(host.speedFactor()*((PacketBasedNetwork)host).throughput());
	}
	
	public void decide() {
		// There is not independent context switch message, and only PRE_DECIDING state is used.
		if ((currentServer()==null) && awaitingServers.isEmpty()){
			setCurrentState(FREE);
			setCurrentServer(null);
		}else {
			   setCurrentServer(awaitingServers.poll());
			if(currentServer.schedParams() instanceof NonPreemptible_FP_Params)
				setCurrentState(NON_PREEMTIBLE_EXECUTION);
			else
				setCurrentState(PREEMTIBLE_EXECUTION);
			host.require(currentServer);

		}
	}
	
	public void require(SimThread server){
		
        if (server!=currentServer){
        	if (!awaitingServers.contains(server)) awaitingServers.add(server);
        }
		if (currentState==FREE){
			setCurrentState(PRE_DECIDING);
			Controller.addDecisor(this);
		}

	}

	/**
	 * This method is conceived to allow a processing resource to report to 
	 * the current scheduler that the execution scheduled by it has finished.
	 */ 
	public void finish() {
		switch (currentState){
		case NON_PREEMTIBLE_EXECUTION:
			currentServer.finish();
			if(currentServer.executingSeg().isLast()){ // reached end of the segment
				SimThread tcs=currentServer;
				currentServer = null; // scheduler released
				tcs.finish();
				setCurrentState(PRE_DECIDING);
				Controller.addDecisor(this);
			}else{
				host.require(currentServer);
			}
			break;
		case PREEMTIBLE_EXECUTION:
			SimThread tcs=currentServer;
			currentServer = null;
			tcs.finish();
			setCurrentState(PRE_DECIDING);
			Controller.addDecisor(this);
			break;
		default:
		}
	}	
	
	@Override
	public String toString(){
		return "\n\nScheduler" + (super.toString() + "\nPolicy: FP packet based").replaceAll("\n", "\n\t");
	}
}
/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Scheduler
 * Description: It models the scheduling of the processing capacity of
 *     a processing resource (primary scheduler)  or scheduling server
 *     (secondary scheduler) 
 * 
 * Author: J.M. Drake
 * Date: 20-1-10
 ********************************************************************/

package resource;

import core.*;
import module.*;
import org.w3c.dom.*;

import utilityClasses.XMLparser;

import java.util.*;

public abstract class Scheduler extends Resource {

	/* ENUMS */
	
	private enum PolicyType {Fixed_Priority_Policy, 
		                     EDF_Policy, 
		                     FP_Packet_Based_Policy, 
		                     Timetable_Driven_Policy, 
		                     Timetable_Driven_Packet_Based_Policy,
		                     AFDX_Policy}
	
	
	/* CONSTANTS */
	
	// States declaration
	protected static final int FREE = 0;
	protected static final int NON_PREEMTIBLE_EXECUTION = 1;
	protected static final int PREEMTIBLE_EXECUTION = 2;
	protected static final int PRE_DECIDING = 3;
	protected static final int CONTEXT_SWITCH = 4;
	protected static final int POST_DECIDING = 5;

	
	/* FIELDS */
	
	private static List<String> stateNames;
	
	// True if it is a primary scheduler, False is secondary scheduler
	private boolean isPrimary = true;

	// The processing suplier which processing capacity is schedule. 
	protected ProcessingSupplier host;
		public ProcessingSupplier host(){return host;}
		public void setHost(ProcessingSupplier host){this.host = host;}
	
	// Reference the scheduling server which is being scheduled currently.
	protected SimThread currentServer;
		public SimThread currentServer(){return currentServer;}
		public void setCurrentServer(SimThread server){currentServer = server;}
		
	// Reference the last scheduling server which has been scheduled.
	private SimThread previousServer = null;
		public SimThread previousServer(){return previousServer;}
		public void setPreviousServer(SimThread server){previousServer = server;}	
	
	// List of scheduling server awaiting to be scheduled. 
	// It do not include the scheduling server that is been schedule.
	protected PriorityQueue<SimThread> awaitingServers; //Se inicializa en las clase privadas
		public void setAwaitingServers(PriorityQueue<SimThread> awaitingServer){this.awaitingServers = awaitingServer;}
		
	// Define the time of context switch process
	protected ETG contextSwitch;
		public ETG contextSwitch() {return contextSwitch;}
		public void setContextSwitch(ETG contextSwitch) {this.contextSwitch = contextSwitch;}

	//
	protected boolean thereIsContextSwitchTime = false;
		public void setContextSwitch() {thereIsContextSwitchTime = true;}
	
	
	/* METHODS */
	
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(FREE, "FREE");
		stateNames.add(NON_PREEMTIBLE_EXECUTION, "NON_PREEMTIBLE_EXECUTION");
		stateNames.add(PREEMTIBLE_EXECUTION, "PREEMTIBLE_EXECUTION");
		stateNames.add(PRE_DECIDING, "PRE_DECIDING");
		stateNames.add(CONTEXT_SWITCH, "CONTEXT_SWITCH");
		stateNames.add(POST_DECIDING, "POST_DECIDING");
	}
	
	// Constructor
	protected Scheduler(Node node) {
		super(node);
		if (node != null) { // Caution because Interrupt_Scheduler
			if (node.getLocalName().equals("Primary_Scheduler"))
				isPrimary = true;
			else // Secondary_Scheduler
				isPrimary = false;
		}
	}
	
	public static Scheduler newScheduler(Node node){
		List<Element> policyElemList = XMLparser.getChildElem(node);
		for (Element policyElem : policyElemList) {
			PolicyType type = PolicyType.valueOf(policyElem.getLocalName());
			switch (type) {
			case Fixed_Priority_Policy:
				return new PriorityBasedScheduler(policyElem);					
			case EDF_Policy:
				return new EDF_Scheduler(policyElem);					
			case FP_Packet_Based_Policy:
				return new PacketBasedScheduler(policyElem);					
			case Timetable_Driven_Policy:
			case Timetable_Driven_Packet_Based_Policy:
			default: // case AFDX_Policy:
				break;
			}
		}
		return null;
	}
	
	public void complete(){
		if(this.getClass() != InterruptScheduler.class){
			String hostId = ((Element)node).getAttribute("Host");
			host = (ProcessingSupplier)Repository.getModelElem(hostId, "ProcessingResource");
			host.setScheduler(this);
		}
	}
	
	public void denormalize(){
		if (contextSwitch != null)
			contextSwitch.denormalize(host.speedFactor());
	}
	
	public String stateName(int state){return stateNames.get(state);}
	
	public String results(String offset){return null;}
	
	/**
  	 * This method is conceived to allow the <code>server</code> argument to require 
  	 * scheduling by the current scheduler.
  	 * @param server
  	 */
	public abstract void require(SimThread server);
	
	/**
	 * This method is conceived to allow a processing resource to report to 
	 * the current scheduler that the execution scheduled by it has finished.
	 */ 
	public void finish() {
		switch (currentState){
		case CONTEXT_SWITCH:
			setCurrentState(POST_DECIDING);
			Controller.addDecisor(this);
			break;
		case NON_PREEMTIBLE_EXECUTION:
			currentServer.finish();
			if(currentServer.executingSeg().isLast()){ // reached end of the segment
				previousServer = currentServer;
				currentServer = null; // scheduler released
				setCurrentState(PRE_DECIDING);
				Controller.addDecisor(this);
			}
			break;
		case PREEMTIBLE_EXECUTION:
			if(currentServer.executingSeg().isLast()){
				previousServer = currentServer;
				currentServer.finish();
				currentServer = null; 
			} else
				currentServer.finish();
			setCurrentState(PRE_DECIDING);
			Controller.addDecisor(this);
			break;
		default:
		}
	}
 
	/**
	 * This method is conceived to allow a processing resource to block the 
	 * current server of the current scheduler if any mutex required by that server is locked.
	 */
	public void blocked() {
		currentServer.block();
		currentServer = null;
		setCurrentState(PRE_DECIDING);
		Controller.addDecisor(this);
	}
		
	/** Returns true if the scheduling server is the current server	 */
	public boolean isCurrentServer(SimThread schedulingServer) {
		return schedulingServer == currentServer;
	}


	@Override
	public String toString() {
		
		String string = super.toString();
		
		if (isPrimary)
			string += "\nPRIMARY";
		else
			string += "\nSECONDARY";
			
		string += "\nHost: ";
		if (host != null)
			string += ((ModelElement)host).name();
		
		string += "\nContext switch:";
		if (contextSwitch != null)
			string += contextSwitch.toString().replaceAll("\n", "\n\t");
		
		return string;
	}
}
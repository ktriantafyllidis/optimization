/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Resource
 * Description: Abstract element root of all class of resources.
 * 
 * Author: D.Garc�a,J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import core.*;
import java.util.*;

import org.w3c.dom.*;
import statemonitor.*;

public abstract class Resource extends ModelElement implements Decisor {

	/* FIELDS */
	
	protected List<StateMonitor> stateMonitorList = new ArrayList<StateMonitor>();
	
	// Current state of the state machine
	protected int currentState;
		public int currentState() {return currentState;}
		public void setCurrentState(int newState){
			if (newState != currentState){
				previousState = currentState;
				currentState = newState;   
				// The new state has to be reported to all state monitors associated to the resource
				for (StateMonitor stateMonitor : stateMonitorList) {
					stateMonitor.state(newState);
				}
			}
		}
	
	// 
	private int previousState;
		public int previousState() {return previousState;}
	
	// ID for printing the current resource in the traces file
	protected int sID = -1; // initial value -1 means that the current resource does not have an ID assigned
		public int sID() {return sID;}
		public void setSID(int sID) {this.sID = sID;}
	

	/* METHODS */
	
	// Constructor
	public Resource(Node node){
		super(node);
	}

	/**
	 * @param state
	 * @return
	 */
	public abstract String stateName(int state);
	
}

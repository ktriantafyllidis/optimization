/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: PriorityBasedScheduler
 * Description: It represents a scheduler that is able to handle the
 * 		 processing capacity of a proccesing suplier: Processing 
 *       Resource (PrimaryScheduler) or Scheduling Server 
 *       (SecondaryScheduler). It represents a interrupt primary-
 *       scheduler with a nonpreemptible policy.
 *      
 * Author: J.M. Drake y D. Garc�a
 * Date: 1-1-09
 ********************************************************************/
		
package resource;

import java.util.PriorityQueue;
import module.*;
import core.*;
		
public class InterruptScheduler extends Scheduler {
	
	/* METHODS */
	
	// Constructor
	public InterruptScheduler(ETG etg, ProcessingResource proc){
		super(null);
		setAwaitingServers(new PriorityQueue<SimThread>(5));
		name = proc.name() + ".InterruptScheduler";
		host = proc;
		contextSwitch = etg;
	}	

	public void require(SimThread server){
		switch (currentState){
			case FREE :
				awaitingServers.add(server);
				setCurrentState(PRE_DECIDING);
				Controller.addDecisor(this);
				break;
			default:
				if (server != currentServer)
					awaitingServers.add(server);
				break;
		}				
	}
	
	public void decide() {
		//If state is "pre_deciding"
		if(currentState()==PRE_DECIDING) {
			//If there is a current server in execution
			if(currentServer()!=null){
				((RegularProcessor)host).interruptRequire(currentServer);
			//If there is not a current server in execution	
			}else{	
				//If there is awaiting server
				if(!awaitingServers.isEmpty()){
					setCurrentServer(awaitingServers.poll());
					//Changes state to "context_switch"
					setCurrentState(CONTEXT_SWITCH);
					((RegularProcessor)host).interruptRequire(null);
				//Else	
				}else{
					//Changes state to "free"
					setCurrentState(FREE);
				}
			}
		}else if(currentState()==POST_DECIDING){	
			//setCurrent(awaitingServer().poll());
			((RegularProcessor)host).interruptRequire(currentServer);	
			setCurrentState(NON_PREEMTIBLE_EXECUTION);	
		}
	}
	
	
	

	@Override
	public String toString(){
		return "\nInterrupt scheduler" + super.toString().replaceAll("\n", "\n\t");
	}
	
}
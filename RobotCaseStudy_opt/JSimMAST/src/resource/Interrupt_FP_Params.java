/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Interrupt_FP_Params
 * Description: Represents an interrupt service routine. No additional
 *    attributes.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import org.w3c.dom.*;

public class Interrupt_FP_Params extends SchedulingParams {

	
    /* ATTRIBUTES */
    
	private int priority;
		public int priority() {return priority;}
		public void setPriotity(int priority) {this.priority = priority;}
    
    
    /* METHODS */
	
	// Constructor
	public Interrupt_FP_Params(Node node) {
		priority = Integer.parseInt(((Element)node).getAttribute("Priority"));
	}

	public void denormalize(double factor){};




	@Override
	public String toString(){
		return "\nInterrupt FP params:" + "\n\tPriority = " + priority;
	}
}

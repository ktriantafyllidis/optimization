package resource;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class NetworkRouter extends NetworkSwitch {

	/* FIELDS */
	
	private long maxFixedBranchLatency;
	private long avgFixedBranchLatency;
	private long minFixedBranchLatency;
	
	private long maxVariableBranchLatency;
	private long avgVariableBranchLatency;
	private long minVariableBranchLatency;
	
	
	/* METHODS */
	
	// Constructor
	protected NetworkRouter(Node node){
		
		super(node);
		
		Element elem = (Element)node;
		
		if (elem.hasAttribute("Max_Fixed_Branch_Latency"))
			maxFixedBranchLatency = Tools.readLong(elem.getAttribute("Max_Fixed_Branch_Latency"));
	    if (elem.hasAttribute("Avg_Fixed_Branch_Latency"))
	    	avgFixedBranchLatency = Tools.readLong(elem.getAttribute("Avg_Fixed_Branch_Latency"));
		if (elem.hasAttribute("Min_Fixed_Branch_Latency"))
			minFixedBranchLatency = Tools.readLong(elem.getAttribute("Min_Fixed_Branch_Latency"));
			
		if (elem.hasAttribute("Max_Variable_Branch_Latency"))
			maxVariableBranchLatency = Tools.readLong(elem.getAttribute("Max_Variable_Branch_Latency"));
	    if (elem.hasAttribute("Avg_Variable_Branch_Latency"))
	    	avgVariableBranchLatency = Tools.readLong(elem.getAttribute("Avg_Variable_Branch_Latency"));
		if (elem.hasAttribute("Min_Variable_Branch_Latency"))
			minVariableBranchLatency = Tools.readLong(elem.getAttribute("Min_Variable_Branch_Latency"));
	}
	
	@Override
	public void denormalize() {
		
		maxFixedBranchLatency =(long)((double)maxFixedBranchLatency / speedFactor);
		avgFixedBranchLatency =(long)((double)avgFixedBranchLatency / speedFactor);
		minFixedBranchLatency =(long)((double)minFixedBranchLatency / speedFactor);
		
		maxVariableBranchLatency =(long)((double)maxVariableBranchLatency / speedFactor);
		avgVariableBranchLatency =(long)((double)avgVariableBranchLatency / speedFactor);
		minVariableBranchLatency =(long)((double)minVariableBranchLatency / speedFactor);
	}

	public ETG getBranchLatency(int outputNum){
		return new WABETG(maxFixedBranchLatency + outputNum * maxVariableBranchLatency, 
				          avgFixedBranchLatency + outputNum * avgVariableBranchLatency, 
				          minFixedBranchLatency + outputNum * minVariableBranchLatency);
	}



	@Override
	public String toString(){
		return super.toString() + 
	           "\nBranch latency:";
	}
}

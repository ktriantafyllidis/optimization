/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Network
 * Description: It represents a network that uses some kind of real
 *    time protocol based on non-preemptible packets for sending
 *    messages. There are networks that support priorities in their
 *    standard protocols (i.e., the CAN bus), and other networks that
 *    need an additional protocol that works on top of the standard
 *    ones (i.e., serial lines, ethernet).
 * 
 * Author: J.M. Drake
 * Date: 10-4-11
 ********************************************************************/

package resource;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;
import statemonitor.PercentOnState;
import statemonitor.StateMonitor;
import core.Clock;
import core.Controller;
import core.Logger;
import core.Timed;
import flow.FlowEndPoint;

public class PacketBasedNetwork extends Network implements Timed {
	
	/* CONSTANTS */

	// States
	private static final int FREE = 0;
	private static final int TRANSMITING_PACKET = 1;
	private static final int DECIDING = 2;
	

	/* ATTRIBUTES */
	
	private static List<String> stateNames;
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(FREE, "FREE");
		stateNames.add(TRANSMITING_PACKET, "TRANSMITING_PACKET");
		stateNames.add(DECIDING, "DECIDING");
	}
	public String stateName(int state){return stateNames.get(state);}	
	
	/* Description of the amount of data included in a packet, excluding any
	 * protocol information. 
	 * The maximum size is used in the calculation
	 * of the number of packets into which a large message is split, calculated
	 * as the ceiling of the message size divided by the maximum packet size. */
	private long maxPacketTransmissionTime = Long.MAX_VALUE; // ns
	private long minPacketTransmissionTime; // ns
		public long maxPacketTransmissionTime() {return maxPacketTransmissionTime;}
		public long minPacketTransmissionTime() {return minPacketTransmissionTime;}

	// FlowEndPoints which will receive a flow event generated before/after a packet is transmitted.
	private ArrayList<FlowEndPoint> preEventTargetList = new ArrayList<FlowEndPoint>();
	private ArrayList<FlowEndPoint> postEventTargetList = new ArrayList<FlowEndPoint>();
		public void addPreEventTarget(FlowEndPoint target) {preEventTargetList.add(target);}
		public void addPostEventTarget(FlowEndPoint target) {postEventTargetList.add(target);}

	
	/* METHODS */
		

	// Constructor 
	public PacketBasedNetwork(Node node){
		
		super(node);
		
		Element elem = (Element)node;

		/*
		<xs:attribute name="Transmission_Kind" type="mast_mdl:Transmission_Kind" use="optional"/>
		<xs:attribute name="Max_Blocking" type="mast_mdl:Normalized_Execution_Time" use="optional"/>
		*/
		
		if (elem.hasAttribute("Max_Packet_Size")) {
			int maxPacketSize = Integer.parseInt(elem.getAttribute("Max_Packet_Size"));
			// I convert size to denormalized time in nanoseconds 
			maxPacketTransmissionTime = (long)(((double)maxPacketSize / throughput / speedFactor) * 1.0E9);
	    }
		if (elem.hasAttribute("Min_Packet_Size")) {
			int minPacketSize = Integer.parseInt(elem.getAttribute("Min_Packet_Size"));
			minPacketTransmissionTime = (long)(((double)minPacketSize / throughput / speedFactor) * 1.0E9);
	    }
	}
	
	@Override
	public void complete(){
		super.complete();
		addMonitors();
	}
	@Override

	public void require(SimThread schedulingServer) {
		
//	//	setIsThereAwaitingServer(true);
		setExecutingServer(schedulingServer);
		schedulingServer.resume();
		//en este caso remaining time es todo el tiempo del paquete pues no es expulsable
		long nextExcecTime=executingServer().executingSeg().currentStep().etg().next();
		Clock.addTimedEvent(nextExcecTime, this); 
		setCurrentState(TRANSMITING_PACKET);
		// I notify all the pre end points that a packet is gonna be sent
		for(FlowEndPoint flowEndPoint : preEventTargetList){
			flowEndPoint.addFlowEvent(executingServer().executingSeg.currentEvent());
		}

	//	setCurrentState(DECIDING);
	}

	public void decide() {}
	

	
	@Override
	public void update(){
		// I notify all the post end points that a packet has been sent
		for(FlowEndPoint flowEndPoint : postEventTargetList){
			flowEndPoint.addFlowEvent(executingServer().executingSeg.currentEvent());
		}
		//if(executingServer!=null)executingServer.suspend();
		executingServer.suspend();
		setExecutingServer(null);
//		setIsThereAwaitingServer(false);
		setCurrentState(FREE);
		scheduler().finish();
	}
	

	public String results(String offset){
		
		String string = "";
		
		/*
		 *  I write the detailed utilization results (<mast_res:Detailed_Utilization_Results>)
		 *  The generation source of detailed utilization results is 
		 *  the PercentOnState aggregated to the current Network
		 *  under PERFORMANCE simulation profile.
		 *  I look for that PercentOnState.
		 */
		PercentOnState monitor = null;
		for (StateMonitor stateMonitor : stateMonitorList) {
			if (stateMonitor instanceof PercentOnState) {
				monitor = (PercentOnState)stateMonitor;
				break;
			}
		}
		if (monitor != null)
			string = offset + "<mast_res:Network_Results Name=\"" + name + "\">\n" +  
					 offset + "\t<mast_res:Detailed_Utilization_Results\n" + 
			         offset + "\t\tTotal=\"" + (100 - monitor.utilizationResults(FREE)) + "\"\n" + 
			         offset + "\t\tApplication=\"" + monitor.utilizationResults(TRANSMITING_PACKET) + "\"\n" + 
			         offset + "\t\tContext_Switch=\"0.0\"/>\n" + 
			         offset + "</mast_res:Network_Results>\n\n";
		
		return string;
	}	
	private void addMonitors(){
		switch (Controller.profile()) {
		case VERBOSE:
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
			break;
		case TRACES:
			stateMonitorList.add(new LogOnState(this, FREE, Logger.PACKET_BASED_NETWORK_FREE));
			stateMonitorList.add(new LogOnState(this, TRANSMITING_PACKET, Logger.PACKET_BASED_NETWORK_TRANSMITING_PACKET));
			break;
		case PERFORMANCE:
			stateMonitorList.add(new PercentOnState(this));
			break;
		default: // SCHEDULABILITY
			break;
		}
	}
	
	@Override
	public String toString(){

		String string = "\nMax packet transmission time = " + maxPacketTransmissionTime + 
		                "\nMin packet transmission time = " + minPacketTransmissionTime;
		
		return "\n\nPacket based network:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

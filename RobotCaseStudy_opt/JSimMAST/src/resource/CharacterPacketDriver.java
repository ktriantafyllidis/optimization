package resource;

import module.Tools;
import operation.SimpleOperation;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import utilityClasses.XMLparser;
import core.Repository;

public class CharacterPacketDriver extends PacketDriver {

	/* FIELDS */
	
	private String charServerRef;
	private SimThread charServer;
	
	private String charSendOperRef;
	private SimpleOperation charSendOper;
	
	private String charReceiveOperRef;
	private SimpleOperation charReceiveOper;
	
	private long charTransmissionTime;
	
	
	/* METHODS */
	
	// Constructor
	public CharacterPacketDriver(Node node, Network network) {
		
		super(node, network);
		
		Element elem = (Element)node;
		if (elem.hasAttribute("Character_Server"))
			charServerRef = elem.getAttribute("Character_Server");
		else {
			Element charServerElem = XMLparser.getChildElem(node, "Character_Server").firstElement();
			charServer = SimThread.newSchedulableResource(charServerElem);
		}
		if (elem.hasAttribute("Character_Send_Operation"))
			charSendOperRef = elem.getAttribute("Character_Send_Operation");
		else {
			Element charSendOperElem = XMLparser.getChildElem(node, "Character_Send_Operation").firstElement();
			charSendOper = new SimpleOperation(charSendOperElem);
		}
		if (elem.hasAttribute("Character_Receive_Operation"))
			charReceiveOperRef = elem.getAttribute("Character_Receive_Operation");
		else {
			Element charReceiveOperElem = XMLparser.getChildElem(node, "Character_Receive_Operation").firstElement();
			charReceiveOper = new SimpleOperation(charReceiveOperElem);
		}
		if (elem.hasAttribute("Character_Transmission_Time"))
			charTransmissionTime = Tools.readLong(elem.getAttribute("Character_Transmission_Time"));
	}
	
	@Override
	public void complete(){
		
		super.complete();
		
		// TODO revisar
		if (charServer != null)
			charServer.complete();
		else// if (charServerRef != null)
			charServer = (SimThread)Repository.getModelElem(charServerRef, "SchedulableResource");
  		
		if (charSendOperRef != null)
  			charSendOper = (SimpleOperation)Repository.getModelElem(charSendOperRef, "Operation");
  		if (charReceiveOperRef != null)
			charReceiveOper = (SimpleOperation)Repository.getModelElem(charReceiveOperRef, "Operation");
  	}
	
	
	
	@Override
	public String toString(){
		
		String string = "\nCharacter server: ";
		if (charServerRef != null)
			string += charServer.name();
		else
			string += charServer.toString();
		
		string += "\nCharacter send operation: "; 
		if (charSendOperRef != null)
			string += charSendOper.name();
		else
			string += charSendOper.toString();
		
		string += "\nCharacter receive operation: ";
		if (charReceiveOperRef != null)
			string += charReceiveOper.name();
		else
			string += charReceiveOper.toString();
		
		string += "\nCharacter transmission time = " + charTransmissionTime;
				
		return "\nCharacter packet driver:" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

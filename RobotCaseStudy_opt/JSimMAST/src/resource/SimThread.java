/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: Thread
 * Description: Models a scheduling entity. Process, task or 
 * 		communication stream 
 * 
 * Author: J.M. Drake, D. Garc�a
 * Date: 1-1-09
 ********************************************************************/

package resource;

import flow.*;
import core.*;

import org.w3c.dom.*;

import utilityClasses.XMLparser;

import java.util.*;

public abstract class SimThread extends Resource implements ProcessingSupplier, Comparable {

	/* ENUMS */ 
	
	protected enum SchedulableResourceType {Thread,
	  									    Communication_Channel
	  									    //Virtual_Schedulable_Resource,
	  									    //Virtual_Communication_Channel,
	  									    //Packet_Server
	  									    }
	
	private enum ParamsType {Interrupt_FP_Params, 
		                     Non_Preemptible_FP_Params, 
		                     Fixed_Priority_Params, 
		                     Polling_Params, 
		                     Periodic_Server_Params, 
		                     Sporadic_Server_Params,
		                     Periodic_Server_Comm_Params,
		                     Sporadic_Server_Comm_Params,
		                     EDF_Params, 
		                     //Time_Table_Driven_Params,
		                     //SRP_Parameters,
		                     //Virtual_Periodic_Server,
		                     //Virtual_Deferrable_Server,
		                     //Virtual_Sporadic_Server,
		                     //Virtual_Periodic_Comm_Channel,
		                     //Virtual_Deferrable_Comm_Channel,
		                     //Virtual_Sporadic_Comm_Channel,
		                     //Virtual_Token_Bucket_Comm_Channel
		                    }
	
	
	/* FIELDS */
		
	// Scheduling data associated to the scheduling server.
	protected SchedulingParams schedParams;
		public SchedulingParams schedParams() {return schedParams;}
  	
  	// SRP data associated to the server.
  	protected SRP_Params srpParams;
  		
  	//
  	protected ResourceReservationParams resourceReservationParams;

	// Segments that have required execution in the scheduling server. 
  	protected LinkedList<Segment> awaitingSegmentList = new LinkedList<Segment>();

	// Current segment that is being executed
  	protected Segment executingSeg;
		public Segment executingSeg() {return executingSeg;}
		public void setExecutingSeg(Segment executingSeg){
			this.executingSeg = executingSeg;
			if (executingSeg != null)
				sID = executingSeg.sid();
		}
	
	// Host of the current server
	protected Scheduler scheduler;
  		public Scheduler scheduler() {return scheduler;}
  		public void setScheduler(Scheduler scheduler) {this.scheduler = scheduler;}
  	
  	// Speed factor of the corresponding final processing resource. 
  	protected double speedFactor = 1.0;
  		public double speedFactor() {return speedFactor;}
  	
  	//
  	private double throughput;
  		public double throughput(){return throughput;}

  	// Next resource that must be locked
  	protected int nextRequiredResourceIndex = 0;
  	protected MutualExclusionResource nextRequiredResource;
  		public MutualExclusionResource nextRequiredResource() {return nextRequiredResource;}
  	  		
  	
  	/* METHODS */
  	
  	// Constructor
  	protected SimThread(Node node){
  		super(node);
  		List<Element> paramsElemList = XMLparser.getChildElem(node); // scheduling params or sync params
  		for (Element paramsElem : paramsElemList) {
			ParamsType type = ParamsType.valueOf(paramsElem.getLocalName());
			switch (type) {
		    //case Virtual_Periodic_Server:
			//case Virtual_Deferrable_Server:
			//case Virtual_Sporadic_Server:
			//case Virtual_Periodic_Comm_Channel:
			//case Virtual_Deferrable_Comm_Channel:
			//case Virtual_Sporadic_Comm_Channel:
			//case Virtual_Token_Bucket_Comm_Channel:
			//	resourceReservationParams = ResourceReservationParams.newResourceReservationParams(paramsElem);
			//	break;
			//case SRP_Parameters:
			//	srpParams = new SRP_Params(paramsElem);
			//	break;
			default:
		  /*case Interrupt_FP_Params:
			case Non_Preemptible_FP_Params:
			case Fixed_Priority_Params:
			case EDF_Params:
			case Polling_Params:
			case Periodic_Server_Params:						
			case Sporadic_Server_Params:
			case Periodic_Server_Comm_Params:						
			case Sporadic_Server_Comm_Params:				
			case Time_Table_Driven_Params:*/
				break;
			}
  		}
  	}
  	
  	public static SimThread newSchedulableResource(Node node) {
  		SimThread server = null;
  		List<Element> paramsElemList = XMLparser.getChildElem(node); // scheduling params or sync params
  		for (Element paramsElem : paramsElemList) {
			ParamsType type = ParamsType.valueOf(paramsElem.getLocalName());
			switch (type) {
			case Interrupt_FP_Params:
			case Non_Preemptible_FP_Params:
			case Fixed_Priority_Params:
			case EDF_Params:
				server = new BasicSchedulableResource(paramsElem);
				break;
			case Polling_Params:
				server = new PollingSchedulableResource(paramsElem);
				//Clock.addTimedEvent(((PollingPolicy)server.data).period(), (PollingSchedulingServer)server);
				break;
			case Periodic_Server_Params:						
			case Sporadic_Server_Params:
			case Periodic_Server_Comm_Params:						
			case Sporadic_Server_Comm_Params:
				server = new SporadicSchedulableResource(paramsElem); // TODO tocar el constructor para contemplar ambos casos
				break;
			//case Timetable_Driven_Params:
				// TODO manage "Time_Table_Driven_Params" case
			//	break;
			default:
		  /*case Virtual_Periodic_Server:
			case Virtual_Deferrable_Server:
			case Virtual_Sporadic_Server:
			case Virtual_Periodic_Comm_Channel:
			case Virtual_Deferrable_Comm_Channel:
			case Virtual_Sporadic_Comm_Channel:
			case Virtual_Token_Bucket_Comm_Channel:
			case SRP_Parameters:*/
				break;
			}
  		}
  		return server;
  	}
  	
  	//@Override
/*  	public int sID() {
  		if (executingSeg != null)
			return executingSegment.sid();  			
  		else
  			return -1;
  	} */
  	
  	public void complete(){
  		
  		String schedulerName = ((Element)node()).getAttribute("Scheduler");
  		scheduler = (Scheduler)Repository.getModelElem(schedulerName, "Scheduler");
  		
  		speedFactor = getProcessingResource().speedFactor();
  		
  		if (getProcessingResource() instanceof Network)
			throughput = ((Network)getProcessingResource()).throughput();
  	}
  	
  	public void denormalize(){
  		/*ProcessingSupplier suplier = scheduler.host();
		while(!ProcessingResource.class.isAssignableFrom(suplier.getClass())){
			suplier=((SchedulableResource)suplier).scheduler().host();
		}
		speedFactor=((ProcessingResource)suplier).speedFactor();*/
  		speedFactor = getProcessingResource().speedFactor();
		schedParams.denormalize(speedFactor);
  	}
  	
  	public void require(SimThread server) {}
  	
	public int compareTo(Object arg0) {
  		// Used in Priority Queue of SchedulingServer. The order criterio is the current priority.
		SimThread obj = (SimThread)arg0;
		if ((executingSeg == null) | (executingSeg.currentEvent() == null)) 
			return -1;
		if ((obj.executingSeg == null) | obj.executingSeg.currentEvent() == null)
			return +1;
		return -(executingSeg.currentEvent().flowPriority()-obj.executingSeg.currentEvent().flowPriority());
	}

  	/**
  	 * This method locks the next shared resource.  
  	 */
  	public void lockNextResource(){
  		nextRequiredResource.lock(this);
  	}
  	
  	/**
  	 * The current activity shared resources are all unlocked
  	 */
  	public void unLockResources(){
  		Iterator<MutualExclusionResource> iter = executingSeg.currentStep().getUnlockList().iterator();
  		while(iter.hasNext()) {
  			iter.next().unlock();
  		}
  	}
  	
  	/**
  	 * This method allows a shared resource to report that server�s priority has changed. 
  	 */
  	public void changeSchedulingData() {
  		// Because the priority changes, we must require a new execution to the scheduler
		scheduler.require(this);
  	}
  	
  	/**
  	 * This method is invoked after a shared resource list is unlocked.
  	 * The current priority is updated 
  	 */
  	public void updatePriority() {
		if(schedParams instanceof PriorityBasedParams) {
			// Flow priority will be the same of the activity priority if activity has a define priority, 
			// else, it uses server's priority
			if(executingSeg.currentStep().operation().overriddenPriority() >= 0)
				executingSeg().currentEvent().setFlowPriority(executingSeg().currentStep().operation().overriddenPriority());
			else		
				executingSeg().currentEvent().setFlowPriority(((PriorityBasedParams)schedParams).priority());
		}
  	}
  	
  	/**
  	 * This method is conceived to allow the <code>segment</code> argument to 
  	 * require to the current server the execution of a step.
  	 * @param segment
  	 */
  	public abstract void require(Segment segment);

  	/**
  	 * This method is conceived to allow a scheduler to report to 
  	 * the current server that the required step execution has finished. 
  	 */
  	public abstract void finish();
    
  	/**
  	 * This method is conceived to allow a mutex to report to
  	 * the current server that it (the mutex) has been assigned to it (the server) 
  	 */
  	public abstract void assignResource();

  	/**
	 * This method is conceived to block the current server. 
  	 */
  	public abstract void block();

  	/**
  	 * This method is conceived to allow a processing resource to 
  	 * resume the execution of the current server.
  	 */
  	public abstract void resume();

  	/**
  	 * This method is conceived to allow a processing resource to 
  	 * suspend the execution of the current server.
  	 */
  	public abstract void suspend();
    
  	/**
  	 * @return
  	 */
  	public ProcessingResource getProcessingResource(){
  		ProcessingSupplier suplier = scheduler.host();
		while(!ProcessingResource.class.isAssignableFrom(suplier.getClass())){
			suplier=((SimThread)suplier).scheduler().host();
		}
		return (ProcessingResource)suplier;
  	}
  	
  	
  	@Override
  	public String toString(){
  		
  		String string = super.toString() + 
  		                "\nScheduler: " + scheduler.name() + 
  		                "\nSpeed factor: " + speedFactor + 
  		                "\nThroughput: " + throughput;
  		                
  		if (schedParams != null)
  			string += schedParams.toString();
  		
  		if (srpParams != null)
			string += "\nSRP params:" + srpParams.toString();
  		
  		if (resourceReservationParams != null)
			string += "\nResource reservation params:" + resourceReservationParams.toString();
  		
  		SchedulableResourceType type = SchedulableResourceType.valueOf(node.getLocalName());
  		switch (type) {
		case Thread:
			return "\n\nRegular schedulable resource" + string.replaceAll("\n", "\n\t");
		//case Virtual_Schedulable_Resource:			
		//	return "\n\nVirtual schedulable resource" + string.replaceAll("\n", "\n\t");
		case Communication_Channel:
			return "\n\nCommunication channel" + string.replaceAll("\n", "\n\t");
		//case Virtual_Communication_Channel:
		//	return "\n\nVirtual communication channel" + string.replaceAll("\n", "\n\t");
		default: //case Packet_Server:
			return "\n\nPacket server" + string.replaceAll("\n", "\n\t");  
		}
  	}
  	
  	
  	
  	
	private class EDFComparator implements Comparator<SimThread>{
		
		public int compare(SimThread server1, SimThread server2){

			long diff = ((EDF_Params)(server2.schedParams())).deadline() - ((EDF_Params)(server1.schedParams())).deadline();
			if (diff > 0)
				return +1;
			else if (diff < 0)
				return -1;
			else 
				return 0;
		}
	}
}
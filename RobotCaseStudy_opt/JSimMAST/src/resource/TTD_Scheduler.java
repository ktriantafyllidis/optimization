package resource;

import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TTD_Scheduler extends TTD_AbstractScheduler {
	
	/* METHODS */
	
	// Constructor
	public TTD_Scheduler(Node policyNode) {
		
		super(policyNode);
		
		Element policyElem = (Element)node;
		
		long worst= 0;
		if (policyElem.hasAttribute("Worst_Context_Switch"))
			worst = Tools.readLong(policyElem.getAttribute("Worst_Context_Switch"));
		if (worst != 0)
			setContextSwitch();
	    
	    long avg = 0;
	    if (policyElem.hasAttribute("Avg_Context_Switch"))
	    	avg = Tools.readLong(policyElem.getAttribute("Avg_Context_Switch"));
	    
		long best = 0;
		if (policyElem.hasAttribute("Best_Context_Switch"))
			best = Tools.readLong(policyElem.getAttribute("Best_Context_Switch"));
		
		if (worst != 0)
			contextSwitch = new WABETG(worst, avg, best);
	    else 
	    	contextSwitch = null;
	}
		
}

/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: RegularProcessor
 * Description: It represents a physical processor with the basic
 *    overheads associated with its hardware interrupt services.
 * 
 * Author: J.M. Drake
 * Date: 1-1-09
 ********************************************************************/

package resource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import statemonitor.DisplayChangeStateMonitor;
import statemonitor.LogOnState;
import statemonitor.PercentOnState;
import statemonitor.StateMonitor;
import timers.Timer;
import timers.Ticker;
import utilityClasses.XMLparser;
import core.Clock;
import core.Controller;
import core.Logger;
import core.Repository;
import core.Timed;

public class RegularProcessor extends ComputingResource implements Timed {
	
	/* CONSTANTS */
	
	// States
	private static final int FREE = 0;
	private static final int ATTENDING_TIMER = 1;
	private static final int ATTENDING_INTERRUPT = 2;
	private static final int ATTENDING_SCHEDULER = 3;
	private static final int ATTENDING_CONTEXTSWITCH = 4;
	private static final int ATTENDING_INTERRUPT_CONTEXTSWITCH = 5;
	private static final int LOCKING_RESOURCE = 6;
	private static final int LOCKING_INTERRUPT_RESOURCE = 7;
	private static final int DECIDING = 8;
	
	/* FIELDS */

	//
	private static List<String> stateNames;
	
	// It is true is there are a timer requirement pending.
	//private boolean isThereAwaitingTimer = false;

	// True if a interrupt execution is pending.
	private boolean isThereAwaitingInterruptServer;
	
	// A reference to the hardware system timer used (see below), that
	// influences the overhead of the System Timed Activities and specifies
	// the processor overhead implicitly introduced by the scheduler or the
	// operating system to implement its own time management services.
    private List<Timer> timerList;
	
	// System timer currently being attended
	private Timer attendedSysTimer;
	
	private List<Timer> awaitingSysTimerList; 
		
	private InterruptScheduler interruptScheduler;
	   
	private SimThread executingInterruptServer;
	
	
	
	/* METHODS */
	
	// Static constructor
	static{
		stateNames = new ArrayList<String>();
		stateNames.add(FREE,"FREE");
		stateNames.add(ATTENDING_TIMER,"ATTENDING_TIMER");
		stateNames.add(ATTENDING_INTERRUPT,"ATTENDING_INTERRUPT");
		stateNames.add(ATTENDING_SCHEDULER,"ATTENDING_SCHEDULER");
		stateNames.add(ATTENDING_CONTEXTSWITCH,"ATTENDING_CONTEXTSWITCH");
		stateNames.add(ATTENDING_INTERRUPT_CONTEXTSWITCH,"ATTENDING_INTERRUPT_CONTEXTSWITCH");
		stateNames.add(LOCKING_RESOURCE,"LOCKING_RESOURCE");
		stateNames.add(LOCKING_INTERRUPT_RESOURCE,"LOCKING_INTERRUPT_RESOURCE");
		stateNames.add(DECIDING,"DECIDING");
	}
	
	// Constructor
	public RegularProcessor(Node node){
		
		super(node);
		
		Element elem = (Element)node;
		
		/*
		<xs:attribute name="Max_Interrupt_Priority" type="mast_mdl:Interrupt_Priority" use="optional"/>
		<xs:attribute name="Min_Interrupt_Priority" type="mast_mdl:Interrupt_Priority" use="optional"/>
		 */
		
		long worst = 0;
		if (elem.hasAttribute("Worst_ISR_Switch")){
			worst = Tools.readLong(elem.getAttribute("Worst_ISR_Switch"));
			//worst = (long)((double)worst / speedFactor);
	    }
	    long avg = 0;
	    if (elem.hasAttribute("Avg_ISR_Switch")){
	    	avg = Tools.readLong(elem.getAttribute("Avg_ISR_Switch"));
	    	//avg = (long)((double)avg / speedFactor);
	    }
		long best = 0;
		if (elem.hasAttribute("Best_ISR_Switch")){
			best = Tools.readLong(elem.getAttribute("Best_ISR_Switch"));
			//best = (long)((double)best / speedFactor);
	    }
		
		// Interrupt scheduler
		ETG isrSwitch = new WABETG(worst, avg, best);
		isrSwitch.denormalize(speedFactor); // instead of denormalize each value
		interruptScheduler = new InterruptScheduler(isrSwitch, this);
		Repository.addModelElem(interruptScheduler, "Scheduler", name + ".InterruptScheduler");
	}
	
	@Override
	public void complete() {
		
		super.complete();
		
		List<Element> sysTimerElemList = XMLparser.getChildElem(node); // localName = "Timer" all of them
		String timerName;
		Timer timer;
		for (Element sysTimerElem : sysTimerElemList) {
			if (timerList == null) {
				timerList = new ArrayList<Timer>();
				awaitingSysTimerList = new LinkedList<Timer>();
			}
			timerName = sysTimerElem.getAttribute("Name");
			timer = (Timer)Repository.getModelElem(timerName, "Timer");
			timer.setOwner(this);
			timerList.add(timer);
		}
		
		addMonitors();
	}
		
	public String stateName(int state){return stateNames.get(state);}
	
	public String results(String offset){
		
		String string = "";
		
		/*
		 *  I write the detailed utilization results (<mast_res:Detailed_Utilization_Results>)
		 *  The generation source of detailed utilization results is 
		 *  the PercentOnState aggregated to the current ComputingResource
		 *  under PERFORMANCE simulation profile.
		 *  I look for that PercentOnState.
		 */
		PercentOnState monitor = null;
		for (StateMonitor stateMonitor : stateMonitorList) {
			if (stateMonitor instanceof PercentOnState) {
				monitor = (PercentOnState)stateMonitor;
				break;
			}
		}
		if (monitor != null) 
			string = offset + "<mast_res:Computing_Resource_Results Name=\"" + name + "\">\n" + 
					 offset + "\t<mast_res:Detailed_Utilization_Results\n" + 
					 offset + "\t\tTotal=\"" + (100 - monitor.utilizationResults(FREE)) + "\"\n" + 
					 offset + "\t\tApplication=\"" + monitor.utilizationResults(ATTENDING_SCHEDULER) + "\"\n" + 
					 offset + "\t\tContext_Switch=\"" + (monitor.utilizationResults(ATTENDING_CONTEXTSWITCH) + 
			      									  	 monitor.utilizationResults(ATTENDING_INTERRUPT_CONTEXTSWITCH)) + "\"\n" +
			      	 offset + "\t\tTimer=\"" + monitor.utilizationResults(ATTENDING_TIMER) + "\"\n" + 
			      	 offset + "\t\tInterrupt=\"" + monitor.utilizationResults(ATTENDING_INTERRUPT) + "\"/>\n" + 
			      	 offset + "</mast_res:Computing_Resource_Results>\n\n";
		
		return string;
	}
	
	public void decide(){
	  if (currentState()==DECIDING){	
		if(awaitingSysTimerList != null && !awaitingSysTimerList.isEmpty()){
			//isThereAwaitingTimer=false;
			attendedSysTimer = awaitingSysTimerList.remove(0);
			Clock.addTimedEvent(attendedSysTimer.overhead().next(),this);
			setCurrentState(ATTENDING_TIMER);
		}else if(isThereAwaitingInterruptServer){
			isThereAwaitingInterruptServer=false;
			if (executingInterruptServer==null){
				Clock.addTimedEvent(interruptScheduler.contextSwitch().next(),this);
				setCurrentState(ATTENDING_INTERRUPT_CONTEXTSWITCH);
			}else{
				if(executingInterruptServer.nextRequiredResource()!=null){
					executingInterruptServer.lockNextResource();
					setCurrentState(LOCKING_INTERRUPT_RESOURCE);
					Controller.addDecisor(this);
				}else{
					executingInterruptServer.resume();
					Clock.addTimedEvent(executingInterruptServer.executingSeg().currentEvent().remainingTime(), this);
					setCurrentState(ATTENDING_INTERRUPT);
				}
			}
		}else if(isThereAwaitingServer()){
			setIsThereAwaitingServer(false);
			if (executingServer == null){
				Clock.addTimedEvent(scheduler.contextSwitch().next(),this);
				setCurrentState(ATTENDING_CONTEXTSWITCH);
			}else{
				if(executingServer.nextRequiredResource() != null){
					executingServer.lockNextResource();
					setCurrentState(LOCKING_RESOURCE);
					Controller.addDecisor(this);
				}else{
					executingServer.resume();
					Clock.addTimedEvent(executingServer.executingSeg().currentEvent().remainingTime(), this);
					setCurrentState(ATTENDING_SCHEDULER);
				}
			}
		}else{
			setCurrentState(FREE);
			setExecutingServer(null);   //NEW
		}
	  }else if (currentState()==LOCKING_RESOURCE){
		if (executingServer().currentState()!=BasicSchedulableResource.BLOCKED){
			if(executingServer().nextRequiredResource()!=null){
				executingServer.lockNextResource();
				setCurrentState(LOCKING_RESOURCE);
				Controller.addDecisor(this);
			}else{
				executingServer.resume();
				Clock.addTimedEvent(executingServer().executingSeg().currentEvent().remainingTime(), this);
				setCurrentState(ATTENDING_SCHEDULER);
				
				//if (executingInterruptServer!=null) isThereAwaitingInterruptServer=true;
			}
		}else{
			scheduler.blocked();
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		}
	  }else if (currentState()==LOCKING_INTERRUPT_RESOURCE){
			if (executingInterruptServer.currentState()!=BasicSchedulableResource.BLOCKED){
				if(executingInterruptServer.nextRequiredResource()!=null){
					executingInterruptServer.lockNextResource();
					setCurrentState(LOCKING_INTERRUPT_RESOURCE);
					Controller.addDecisor(this);
				}else{
					executingInterruptServer.resume();
					Clock.addTimedEvent(executingInterruptServer.executingSeg().currentEvent().remainingTime(), this);
					setCurrentState(ATTENDING_INTERRUPT); //CAMBIO
					
				}
			}else{
				interruptScheduler.blocked();
				setCurrentState(DECIDING);
				Controller.addDecisor(this);
			}
	  }
	}
	
	public void require(SimThread server) {
		
		setIsThereAwaitingServer(true);
		
		if(currentState == ATTENDING_SCHEDULER){
			executingServer.suspend();
			Clock.cancelTimedEvent(this); //CAMBIO DEL DIA 17
		}
		
		executingServer = server;
		
		if(currentState == FREE || currentState == ATTENDING_SCHEDULER){
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		}
	}
	
	/**
  	 * This method is conceived to allow the execution of a timer attending routine
  	 * @param requirerTimer
  	 */
	public void timerRequire(Timer requirerTimer) {
		if (currentState == FREE || currentState == ATTENDING_INTERRUPT
				                 || currentState == ATTENDING_SCHEDULER ){
			attendedSysTimer = requirerTimer;
			if(currentState == ATTENDING_INTERRUPT){
				interruptScheduler.currentServer().suspend();
				isThereAwaitingInterruptServer = true;
			} else if (currentState == ATTENDING_SCHEDULER){	
				scheduler.currentServer().suspend();
				setIsThereAwaitingServer(true);
			}
			Clock.cancelTimedEvent(this);
			//isThereAwaitingTimer=false;
			Clock.addTimedEvent(requirerTimer.overhead().next(),this);
			setCurrentState(ATTENDING_TIMER);
		} else {
			//isThereAwaitingTimer=true;
			awaitingSysTimerList.add(requirerTimer);
		}
	}
	
	/**
  	 * This method is conceived to allow an interrupt scheduler to require the execution,
  	 * by the current regular processor, of the <code>server</code> argument, 
  	 * typically the current server of the interrupt scheduler.
  	 * @param server
  	 */
	public void interruptRequire(SimThread server) {
		executingInterruptServer = server;
		switch (currentState) {
		case ATTENDING_TIMER:
		case DECIDING:
		case ATTENDING_CONTEXTSWITCH:
		case ATTENDING_INTERRUPT_CONTEXTSWITCH:
			isThereAwaitingInterruptServer = true;
			break;
		case ATTENDING_INTERRUPT:
			isThereAwaitingInterruptServer = true;
			Clock.cancelTimedEvent(this);
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
			break;
		case ATTENDING_SCHEDULER:
			isThereAwaitingInterruptServer = true;
			scheduler.currentServer().suspend();
			Clock.cancelTimedEvent(this);
			setIsThereAwaitingServer(true);
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
			break;
		case FREE:
			isThereAwaitingInterruptServer = true;
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
			break;
		default:
	  /*case LOCKING_RESOURCE:
		case LOCKING_INTERRUPT_RESOURCE:*/
			break;
		}
		
		/*if((currentState == ATTENDING_TIMER) || currentState == DECIDING
				                             || currentState == ATTENDING_CONTEXTSWITCH
				                             || currentState == ATTENDING_INTERRUPT_CONTEXTSWITCH){			
			isThereAwaitingInterruptServer = true;
		} else if (currentState == ATTENDING_INTERRUPT){
			isThereAwaitingInterruptServer = true;
			Clock.cancelTimedEvent(this);
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		} else if (currentState == ATTENDING_SCHEDULER){
			isThereAwaitingInterruptServer = true;
			scheduler.currentServer().suspend();
			Clock.cancelTimedEvent(this);
			setIsThereAwaitingServer(true);
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		}else if(currentState == FREE){
			isThereAwaitingInterruptServer = true;
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		}*/
	}

	// It is invoked when the deadlime programmed is elapsed
	public void update(){
		
		if(currentState()==ATTENDING_SCHEDULER){
			executingServer.unLockResources();
			setExecutingServer(null);
			scheduler().finish();
			setCurrentState(DECIDING);
			Controller.addDecisor(this);}
		else if(currentState()==ATTENDING_CONTEXTSWITCH){
				scheduler().finish();
				setCurrentState(DECIDING);
				Controller.addDecisor(this);
		}else if(currentState()==ATTENDING_INTERRUPT){
			executingInterruptServer.unLockResources();	
			executingInterruptServer=null;
			interruptScheduler.finish();
			setCurrentState(DECIDING);
			Controller.addDecisor(this);}
		else if(currentState()==ATTENDING_INTERRUPT_CONTEXTSWITCH){
				interruptScheduler.finish();
				setCurrentState(DECIDING);
				Controller.addDecisor(this); //CAMBIO
		}else if(currentState()==ATTENDING_TIMER){ // attendedSystemTimer != null
			if (attendedSysTimer instanceof Ticker)
				((Ticker)attendedSysTimer).finish();
			attendedSysTimer = null;
			setCurrentState(DECIDING);
			Controller.addDecisor(this);
		}		
	}	

	private void addMonitors(){
		switch (Controller.profile()) {
		case VERBOSE:
			stateMonitorList.add(new DisplayChangeStateMonitor(this));
			break;
		case TRACES:
			stateMonitorList.add(new LogOnState(this, FREE, Logger.REGULAR_PROCESSOR_FREE));
			stateMonitorList.add(new LogOnState(this, ATTENDING_SCHEDULER, Logger.REGULAR_PROCESSOR_ATTENDING_SCHEDULER));
			stateMonitorList.add(new LogOnState(this, ATTENDING_CONTEXTSWITCH, Logger.REGULAR_PROCESSOR_ATTENDING_CONTEXTSWITCH));
			stateMonitorList.add(new LogOnState(this, ATTENDING_TIMER, Logger.REGULAR_PROCESSOR_ATTENDING_TIMER));
			stateMonitorList.add(new LogOnState(this, ATTENDING_INTERRUPT, Logger.REGULAR_PROCESSOR_ATTENDING_INTERRUPT));
			stateMonitorList.add(new LogOnState(this, ATTENDING_INTERRUPT_CONTEXTSWITCH, Logger.REGULAR_PROCESSOR_ATTENDING_INTERRUPT_CONTEXTSWITCH));
			stateMonitorList.add(new LogOnState(this, LOCKING_RESOURCE, Logger.REGULAR_PROCESSOR_LOCKING_RESOURCE));
			break;
		case PERFORMANCE:
			stateMonitorList.add(new PercentOnState(this));
			break;
		default: // SCHEDULABILITY
			break;
		}
	}

	@Override
	public String toString(){
		String string = interruptScheduler.toString() +  
		                "\nSystem timers:";
		if (timerList != null)			
			for (Timer sysTimer : timerList) {
				string += "\n\t" + sysTimer.name();
			}
		return "\n\nRegular processor" + (super.toString() + string).replaceAll("\n", "\n\t");
	}
}

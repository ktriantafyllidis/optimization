package resource;

import module.ETG;
import module.Tools;
import module.WABETG;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class NetworkSwitch extends ProcessingResource {

	/* FIELDS */
	
	private ETG deliveryLatency;
		public ETG getDeliveryLatency(){return deliveryLatency;}
	
	private long maxFixedForkLatency;
	private long avgFixedForkLatency;
	private long minFixedForkLatency;
	
	private long maxVariableForkLatency;
	private long avgVariableForkLatency;
	private long minVariableForkLatency;

	/*private ETG forkLatency;
		public ETG getForkLatency(){return forkLatency;}
	private int outputNum;
		public void setOutputNum(int outputNum){this.outputNum = outputNum;}*/
		
	
	/* METHODS */
	
	// Constructor
	protected NetworkSwitch(Node node){
		
		super(node);
		
		Element elem = (Element)node;
		
		long max = 0;
		if (elem.hasAttribute("Max_Delivery_Latency"))
			max = Tools.readLong(elem.getAttribute("Max_Delivery_Latency"));
		long avg = 0;
	    if (elem.hasAttribute("Avg_Delivery_Latency"))
	    	avg = Tools.readLong(elem.getAttribute("Avg_Delivery_Latency"));
	    long min = 0;
		if (elem.hasAttribute("Min_Delivery_Latency"))
			min = Tools.readLong(elem.getAttribute("Min_Delivery_Latency"));
		
		deliveryLatency = new WABETG(max, avg, min);
		
		if (elem.hasAttribute("Max_Fixed_Fork_Latency"))
			maxFixedForkLatency = Tools.readLong(elem.getAttribute("Max_Fixed_Fork_Latency"));
	    if (elem.hasAttribute("Avg_Fixed_Fork_Latency"))
	    	avgFixedForkLatency = Tools.readLong(elem.getAttribute("Avg_Fixed_Fork_Latency"));
		if (elem.hasAttribute("Min_Fixed_Fork_Latency"))
			minFixedForkLatency = Tools.readLong(elem.getAttribute("Min_Fixed_Fork_Latency"));
		
		if (elem.hasAttribute("Max_Variable_Fork_Latency"))
			maxVariableForkLatency = Tools.readLong(elem.getAttribute("Max_Variable_Fork_Latency"));
	    if (elem.hasAttribute("Avg_Variable_Fork_Latency"))
	    	avgVariableForkLatency = Tools.readLong(elem.getAttribute("Avg_Variable_Fork_Latency"));
		if (elem.hasAttribute("Min_Variable_Fork_Latency"))
			minVariableForkLatency = Tools.readLong(elem.getAttribute("Min_Variable_Fork_Latency"));
	}

	public void denormalize() {
		
		deliveryLatency.denormalize(speedFactor);
		
		maxFixedForkLatency = (long)((double)maxFixedForkLatency / speedFactor);
		avgFixedForkLatency = (long)((double)avgFixedForkLatency / speedFactor);
		minFixedForkLatency = (long)((double)minFixedForkLatency / speedFactor);
		
		maxVariableForkLatency = (long)((double)maxVariableForkLatency / speedFactor);
		avgVariableForkLatency = (long)((double)avgVariableForkLatency / speedFactor);
		minVariableForkLatency = (long)((double)minVariableForkLatency / speedFactor);
	}

	public String stateName(int state) {return null;}

	public String results(String offset){return "";}
	
	public void decide() {}

	public void require(SimThread server) {}
	
	public ETG calculateForkLatency(int outputNum){
		return new WABETG(maxFixedForkLatency + outputNum * maxVariableForkLatency, 
				          avgFixedForkLatency + outputNum * avgVariableForkLatency, 
				          minFixedForkLatency + outputNum * minVariableForkLatency);
	}
	
	
	@Override
	public String toString(){
		return super.toString() + 
		       "\nDelivery latency:" + getDeliveryLatency().toString() + 
		       "\nForkLatency";
	}
}

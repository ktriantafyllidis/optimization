/*********************************************************************
 *                           JSimMast_2
 *                          PROJECT MAST
 *        Computer and Real-Time Group. Universidad de Cantabria
 *                              
 * Class: NonPreemptible_FP_Params
 * Description: Activities scheduled with these parameters are
 *    non preemptible.
 * 
 * Author: J.M. Drake
 * Date: 28-1-2010
 ********************************************************************/

package resource;

import org.w3c.dom.*;

public class NonPreemptible_FP_Params extends PriorityBasedParams {
	
    /* METHODS */
		
	// Constructor
    public NonPreemptible_FP_Params(Node node) {
    	super(node);
    }
    
    public int currentPriority() {
    	return priority;
    }




    @Override
	public String toString(){
		return "\nNon preemptible FP params:" + super.toString().replaceAll("\n", "\n\t");
	}
}

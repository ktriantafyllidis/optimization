package resource;

import module.Tools;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public abstract class VirtualPeriodicReplenishmentServer extends VirtualResourceParams {

	/* FIELDS */
	
	private long budget;
	private long period; 
	private long deadline;
	
	
	/* METHODS */
	
	// Constructor
	protected VirtualPeriodicReplenishmentServer(Node node) {
		
		super(node);
		
		Element elem = (Element)node;

		if (elem.hasAttribute("Budget"))
			budget = Tools.readLong(elem.getAttribute("Budget"));

		if (elem.hasAttribute("Period"))
			period = Tools.readLong(elem.getAttribute("Period"));

		if (elem.hasAttribute("Deadline"))
			deadline = Tools.readLong(elem.getAttribute("Deadline"));
	}
	
	@Override
	public void denormalize(double factor){
		budget = (long)((double)budget / factor);
	}
	
}

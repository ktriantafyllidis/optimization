package resource;

import org.w3c.dom.Node;

public class RegularSwitch extends NetworkSwitch {

	/* METHODS */
	
	// Constructor
	public RegularSwitch(Node node){
		super(node);
	}



	@Override
	public String toString(){
		return "\n\nRegular switch: " + super.toString().replaceAll("\n", "\n\t");
	}
}
